package com.aait.mazaqelkhlij.listeners;

/**
 * Created by Suleiman on 16/11/16.
 */

public interface PaginationAdapterCallback {
    void retryPageLoad();
}
