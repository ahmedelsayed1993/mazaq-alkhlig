package com.aait.mazaqelkhlij.listeners;



/**
 * Created by Amrel on 14/06/2017.
 */


public interface OrderHasDelivery {

    void orderHasDelivery(boolean hasDelivery);
}

