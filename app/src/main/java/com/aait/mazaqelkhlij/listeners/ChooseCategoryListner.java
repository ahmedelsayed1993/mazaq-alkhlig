package com.aait.mazaqelkhlij.listeners;


import com.aait.mazaqelkhlij.models.CatModel;
import com.aait.mazaqelkhlij.models.CategoryModel;
import com.aait.mazaqelkhlij.models.ListModel;

import java.util.ArrayList;

/**
 * Created by Amrel on 14/06/2017.
 */


public interface ChooseCategoryListner {

    void ChooseCategories(ArrayList<CatModel> categoryModels);
}

