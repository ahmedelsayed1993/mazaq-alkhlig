package com.aait.mazaqelkhlij.listeners;

/**
 * Created by Ramzy on 1/18/2018.
 */

public interface SnackBarClickListener {
    void onSnackBarActionClickListener();
}
