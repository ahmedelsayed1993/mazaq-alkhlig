package com.aait.mazaqelkhlij.UI.adapters.Provider;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentRecyclerAdapter;
import com.aait.mazaqelkhlij.base.ParentRecyclerViewHolder;
import com.aait.mazaqelkhlij.models.CatModel;

import java.util.List;

import butterknife.BindView;

public class CategoryChooseAdapter2 extends ParentRecyclerAdapter<CatModel> {

    public CategoryChooseAdapter2(final Context context, final List<CatModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setClickableRootView(holder.layContainer);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        CatModel categoryModel = data.get(position);
        viewHolder.tvName.setText(categoryModel.getName());
        if (categoryModel.isCheckCategory()) {
            viewHolder.ivCheck.setVisibility(View.VISIBLE);
        } else {
            viewHolder.ivCheck.setVisibility(View.GONE);
        }
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.iv_check)
        ImageView ivCheck;

        @BindView(R.id.lay_container)
        RelativeLayout layContainer;

        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

