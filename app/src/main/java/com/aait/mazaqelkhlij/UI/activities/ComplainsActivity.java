package com.aait.mazaqelkhlij.UI.activities;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.ValidationUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.BaseResponse;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComplainsActivity extends ParentActivity {


    @BindView(R.id.til_complains_title)
    TextInputLayout tilComplainsTitle;

    @BindView(R.id.et_complains_title)
    TextInputEditText etComplainsTitle;

    @BindView(R.id.et_complains_description)
    EditText etComplainsDescription;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, ComplainsActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.complains));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_complaints_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    boolean setComplains() {
        if (!ValidationUtils.checkError(etComplainsTitle, tilComplainsTitle, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.emptyValidation(etComplainsDescription, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }

    @OnClick(R.id.btn_send_complains)
    void onBtnSendComplainsClick() {
      if (setComplains()){
          sendComplain();
      }
    }

    private void sendComplain(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).sendComplain(mSharedPrefManager.getUserData().getUser_id()+"",mLanguagePrefManager.getAppLanguage(),
                etComplainsTitle.getText().toString(),etComplainsDescription.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        etComplainsDescription.setText("");
                        etComplainsTitle.setText("");
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
