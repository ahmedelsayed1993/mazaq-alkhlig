package com.aait.mazaqelkhlij.UI.adapters.Client;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentRecyclerAdapter;
import com.aait.mazaqelkhlij.base.ParentRecyclerViewHolder;
import com.aait.mazaqelkhlij.models.ProductModel;
import com.bumptech.glide.Glide;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class OffersAdapters1 extends ParentRecyclerAdapter<ProductModel> {

    private String errorMsg;

    public OffersAdapters1(final Context context, final List<ProductModel> data) {
        super(context, data);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        switch (viewType) {
            case Constant.InfinitScroll.ITEM:
                View viewItem = inflater.inflate(R.layout.recycle_food_row_client2, parent, false);
                viewHolder = new OffersViewHolder(viewItem);
                break;
            case Constant.InfinitScroll.LOADING:
                View viewLoading = inflater.inflate(R.layout.recycle_item_progress_client, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case Constant.InfinitScroll.ITEM:
                final OffersViewHolder offersViewHolder = (OffersViewHolder) holder;
                ProductModel foodModel = data.get(position);
                Glide.with(mcontext).load(foodModel.getProduct_image()).asBitmap().placeholder(R.mipmap.splash)
                        .into(offersViewHolder.ivFoodImage);
                offersViewHolder.tvFoodName.setText(foodModel.getProduct_name());
                offersViewHolder.tvNormalPrice
                        .setText(foodModel.getProduct_price() + " " + mcontext.getResources().getString(R.string.SAR));
                if (foodModel.isProduct_have_offer()==true) {
                    CommonUtil.setStrokInText(offersViewHolder.tvNormalPrice);
                    offersViewHolder.tvDiscountPrice
                            .setText(foodModel.getProduct_offer() + " " + mcontext.getResources()
                                    .getString(R.string.SAR));
                } else {
                    offersViewHolder.tvDiscountPrice.setVisibility(View.GONE);
                    offersViewHolder.iv_offer.setVisibility(View.GONE);
                }
                offersViewHolder.tvDecription.setText(foodModel.getProduct_disc());

                offersViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        itemClickListener.onItemClick(view, position);
                    }
                });
                break;

            case Constant.InfinitScroll.LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    mcontext.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == data.size() - 1 && isLoadingAdded)
                ? Constant.InfinitScroll.LOADING
                : Constant.InfinitScroll.ITEM;
    }


    /**
     * Main list's content ViewHolder
     */

    protected class OffersViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.iv_food_image)
        ImageView ivFoodImage;

        @BindView(R.id.iv_offer)
        ImageView iv_offer;

        @BindView(R.id.tv_food_name)
        TextView tvFoodName;

        @BindView(R.id.tv_normal_price)
        TextView tvNormalPrice;

        @BindView(R.id.tv_discount_price)
        TextView tvDiscountPrice;

        @BindView(R.id.tv_decription)
        TextView tvDecription;


        public OffersViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }


    protected class LoadingVH extends ParentRecyclerViewHolder {


        @BindView(R.id.progress_wheel)
        ProgressWheel mProgressBar;

        @BindView(R.id.loadmore_retry)
        ImageButton mRetryBtn;

        @BindView(R.id.loadmore_errortxt)
        TextView mErrorTxt;

        @BindView(R.id.loadmore_errorlayout)
        LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            ProgressWheel mProgressBar = (ProgressWheel) findViewById(R.id.progress_wheel);
        }

        @OnClick(R.id.loadmore_retry)
        void onLoadMoreRetryClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }

        @OnClick(R.id.loadmore_errorlayout)
        void onLoadMoreErrorLayoutClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }
    }


    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(data.size() - 1);
        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }



}
