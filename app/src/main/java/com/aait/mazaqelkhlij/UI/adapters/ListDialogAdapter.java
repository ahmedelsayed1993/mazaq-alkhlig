package com.aait.mazaqelkhlij.UI.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentRecyclerAdapter;
import com.aait.mazaqelkhlij.base.ParentRecyclerViewHolder;
import com.aait.mazaqelkhlij.models.ListModel;

import java.util.List;

import butterknife.BindView;

public class ListDialogAdapter extends ParentRecyclerAdapter<ListModel> {

    public ListDialogAdapter(final Context context, final List<ListModel>  data) {
        super(context, data);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        View viewItem = inflater.inflate(R.layout.recycler_dialog_list_row_family, parent, false);
        viewHolder = new ListAdapter(viewItem);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        ListAdapter listAdapter = (ListAdapter) holder;
        ListModel listModel = data.get(position);
        listAdapter.tv_row_title.setText(listModel.getName());
        if (position % 2 == 0) {
            listAdapter.tv_row_title.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorRowSecondary));
        } else {
            listAdapter.tv_row_title.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorRowPrimary));

        }

        listAdapter.tv_row_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                itemClickListener.onItemClick(view, position);
            }
        });
    }


    protected class ListAdapter extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_row_title)
        TextView tv_row_title;

        public ListAdapter(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }
}

