package com.aait.mazaqelkhlij.UI.fragments.Provider;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.Provider.NewOrderDetailsActivity;
import com.aait.mazaqelkhlij.UI.adapters.Provider.NewOrderAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.BaseFragment;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.listeners.PaginationScrollListener;
import com.aait.mazaqelkhlij.models.OrderModel;
import com.aait.mazaqelkhlij.models.OrderResponse;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewOrderFragment extends BaseFragment implements OnItemClickListener {
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.progress_wheel)
    ProgressWheel progressWheel;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.iv_no_internet)
    ImageView ivNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.iv_no_item)
    ImageView ivNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.rv_recycle)
    RecyclerView rv_Recycle;

    LinearLayoutManager linearLayoutManager;

    NewOrderAdapter mNewOrderAdapter;

    List<OrderModel> mOrderModels = new ArrayList<>();

    private static final int PAGE_START = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;

    public static NewOrderFragment newInstance() {
        Bundle args = new Bundle();
        NewOrderFragment fragment = new NewOrderFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_offers_family;
    }

    @Override
    protected void initializeComponents(View view) {
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mNewOrderAdapter = new NewOrderAdapter(mContext, mOrderModels);
        mNewOrderAdapter.setOnItemClickListener(this);
        rv_Recycle.setLayoutManager(linearLayoutManager);
        rv_Recycle.setItemAnimator(new DefaultItemAnimator());
        rv_Recycle.setAdapter(mNewOrderAdapter);
        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNewOrder();
            }
        });

        getNewOrder();

    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        NewOrderDetailsActivity.startActivity((AppCompatActivity)mContext,mOrderModels.get(position).getOrder_id());

    }
    private void getNewOrder() {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getNewOrders(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getUser_id())
                .enqueue(new Callback<OrderResponse>() {
                    @Override
                    public void onResponse(Call<OrderResponse> call,
                                           Response<OrderResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()==1) {
                                assert response.body() != null;
                                if (response.body().getData().size() == 0) {
                                    layNoItem.setVisibility(View.VISIBLE);
                                    layNoInternet.setVisibility(View.GONE);
                                    tvNoContent.setText(R.string.no_data);
                                } else {
                                    mNewOrderAdapter.updateAll(response.body().getData());
                                }
                            }else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<OrderResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
}
