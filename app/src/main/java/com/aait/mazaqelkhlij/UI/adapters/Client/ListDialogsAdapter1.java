package com.aait.mazaqelkhlij.UI.adapters.Client;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentRecyclerAdapter;
import com.aait.mazaqelkhlij.base.ParentRecyclerViewHolder;
import com.aait.mazaqelkhlij.models.ListModel;

import java.util.List;

import butterknife.BindView;

public class ListDialogsAdapter1  extends ParentRecyclerAdapter<ListModel> {

    public ListDialogsAdapter1(final Context context, final List<ListModel> data) {
        super(context, data);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        View viewItem = inflater.inflate(R.layout.recycler_dialog_list_row_client1, parent, false);
        viewHolder = new ListDialogsAdapter1.ListAdapter(viewItem);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        ListDialogsAdapter1.ListAdapter listAdapter = (ListDialogsAdapter1.ListAdapter) holder;
        ListModel listModel = data.get(position);
        listAdapter.tv_row_title2.setText(listModel.getName());
        if (position % 2 == 0) {
            listAdapter.tv_row_title2.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorBlack));
        } else {
            listAdapter.tv_row_title2.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorTransparentBlack));

        }

        listAdapter.tv_row_title2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Log.e("postion",position+"");
                itemClickListener1.onItemClick(view, position);
            }
        });
    }


    protected class ListAdapter extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_row_title2)
        TextView tv_row_title2;

        public ListAdapter(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }
}

