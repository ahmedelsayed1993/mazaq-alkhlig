package com.aait.mazaqelkhlij.UI.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentRecyclerAdapter;
import com.aait.mazaqelkhlij.base.ParentRecyclerViewHolder;
import com.aait.mazaqelkhlij.models.NotificationModel;
import com.bumptech.glide.Glide;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class NotificationAdapter extends ParentRecyclerAdapter<NotificationModel> {

    private String errorMsg;

    public NotificationAdapter(final Context context, final List<NotificationModel> data) {
        super(context, data);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        switch (viewType) {
            case Constant.InfinitScroll.ITEM:
                View viewItem = inflater.inflate(R.layout.recycler_notification_row_client, parent, false);
                viewHolder = new NotificationViewHolder(viewItem);
                break;
            case Constant.InfinitScroll.LOADING:
                View viewLoading = inflater.inflate(R.layout.recycle_item_progress_client, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case Constant.InfinitScroll.ITEM:
                final NotificationViewHolder notificationViewHolder = (NotificationViewHolder) holder;
                NotificationModel notificationModel = data.get(position);
               // notificationViewHolder.tvTime.setText(notificationModel.getTime());
//                Glide.with(mcontext).load(notificationModel.getIcon()).asBitmap()
//                        .into(notificationViewHolder.ivNotificationIcon);
                if (notificationModel.getType().equals("order")){
                    notificationViewHolder.tvNotificationTitle.setText(mcontext.getResources().getString(R.string.order));
                }else if (notificationModel.getType().equals("dashboard")){
                    notificationViewHolder.tvNotificationTitle.setText(mcontext.getResources().getString(R.string.dashbord));
                }else if (notificationModel.getType().equals("book")){
                    notificationViewHolder.tvNotificationTitle.setText(mcontext.getResources().getString(R.string.delivery));
                }
                notificationViewHolder.tvNotificationDesc.setText(notificationModel.getMsg());
                notificationViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        itemClickListener.onItemClick(view, position);
                    }
                });
                break;

            case Constant.InfinitScroll.LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    mcontext.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == data.size() - 1 && isLoadingAdded)
                ? Constant.InfinitScroll.LOADING
                : Constant.InfinitScroll.ITEM;
    }


    /**
     * Main list's content ViewHolder
     */

    protected class NotificationViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.iv_notification_icon)
        ImageView ivNotificationIcon;

        @BindView(R.id.tv_notification_title)
        TextView tvNotificationTitle;

        @BindView(R.id.tv_time)
        TextView tvTime;

        @BindView(R.id.tv_notification_desc)
        TextView tvNotificationDesc;


        public NotificationViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }


    protected class LoadingVH extends ParentRecyclerViewHolder {


        @BindView(R.id.progress_wheel)
        ProgressWheel mProgressBar;

        @BindView(R.id.loadmore_retry)
        ImageButton mRetryBtn;

        @BindView(R.id.loadmore_errortxt)
        TextView mErrorTxt;

        @BindView(R.id.loadmore_errorlayout)
        LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            ProgressWheel mProgressBar = (ProgressWheel) findViewById(R.id.progress_wheel);
        }

        @OnClick(R.id.loadmore_retry)
        void onLoadMoreRetryClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }

        @OnClick(R.id.loadmore_errorlayout)
        void onLoadMoreErrorLayoutClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }
    }


    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(data.size() - 1);
        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }


}

