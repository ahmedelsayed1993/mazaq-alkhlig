package com.aait.mazaqelkhlij.UI.activities.Delegate;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.Delegate.OrderDetailsAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.PermissionUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.OrderDetailsResponse;
import com.aait.mazaqelkhlij.models.OrderModel;
import com.aait.mazaqelkhlij.models.OrderProductModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreviousOrdersDetailsActivity extends ParentActivity {

    @BindView(R.id.tv_order_time)
    TextView tvOrderTime;

    @BindView(R.id.iv_clock)
    ImageView ivClock;

    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;

    @BindView(R.id.iv_client_image)
    CircleImageView ivClientImage;

    @BindView(R.id.tv_client_name)
    TextView tvClientName;

    @BindView(R.id.tv_client_city)
    TextView tvClientCity;

    @BindView(R.id.tv_client_mobile)
    TextView tvClientMobile;

    @BindView(R.id.iv_family_image)
    CircleImageView ivFamilyImage;

    @BindView(R.id.tv_family_name)
    TextView tvFamilyName;

    @BindView(R.id.tv_family_city)
    TextView tvFamilyCity;

    @BindView(R.id.tv_family_mobile)
    TextView tvFamilyMobile;

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.tv_total_coast)
    TextView tv_total_coast;



    LinearLayoutManager linearLayoutManager;

    OrderDetailsAdapter mFamilyOrderDetailsAdapter;

    List<OrderProductModel> mFamilyOrderModels = new ArrayList<>();

    OrderModel mOrderModel;
    int Order_id;

    public static void startActivity(AppCompatActivity mAppCompatActivity,int Order_id) {
        Intent mIntent = new Intent(mAppCompatActivity, PreviousOrdersDetailsActivity.class);
        mIntent.putExtra(Constant.BundleData.ORDER, Order_id);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData() {

        Order_id = getIntent().getIntExtra(Constant.BundleData.ORDER,0);
    }

    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.order_details));
        getBundleData();
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mFamilyOrderDetailsAdapter = new OrderDetailsAdapter(mContext, mFamilyOrderModels,
                R.layout.recycle_family_order_details_delivery);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(mFamilyOrderDetailsAdapter);

        getDetails();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_previous_order_details_delivery;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    void setOrderData(OrderModel orderData) {

        Glide.with(mContext).load(orderData.getOrder_user_avatar()).asBitmap()
                .into(ivClientImage);
        Glide.with(mContext).load(orderData.getOrder_provider_avatar()).asBitmap()
                .into(ivFamilyImage);
        tvOrderTime
                .setText(orderData.getOrder_created_at() + " ");
        tvOrderNumber
                .setText(orderData.getOrder_id() + "");

        tvClientName.setText(orderData.getOrder_user_name());
        tvFamilyName.setText(orderData.getOrder_provider_name());

        tvClientCity.setText(orderData.getOrder_user_area());
        tvFamilyCity.setText(orderData.getOrder_provider_area());

        tvClientMobile.setText(orderData.getOrder_user_phone());
        tvFamilyMobile.setText(orderData.getOrder_provider_phone());

        tv_total_coast.setText(orderData.getTotal_price()+mContext.getResources().getString(R.string.SAR));

        tvOrderTime.setText(orderData.getOrder_created_at());

    }

    @OnClick(R.id.tv_client_mobile)
    void onClient(){
        if (!tvClientMobile.getText().toString().equals("")){
            getLocationWithPermission(tvClientMobile.getText().toString());
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.no_data));
        }
    }
    @OnClick(R.id.tv_family_mobile)
    void onFamily(){
        if (!tvFamilyMobile.getText().toString().equals("")){
            getLocationWithPermission(tvFamilyMobile.getText().toString());
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.no_data));
        }
    }
    public void getLocationWithPermission(String number) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(PreviousOrdersDetailsActivity.this, PermissionUtils.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.CALL_PHONE,
                            Constant.RequestPermission.REQUEST_CALL);
                }
            } else {
                callnumber(number);
            }
        } else {
            callnumber(number);
        }

    }
    void callnumber(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        startActivity(intent);
    }
    private void getDetails(){

        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getDetails(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Order_id).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful())
                {
                    if (response.body().getStatus()==1){
                        setOrderData(response.body().getData());
                       // processOrderSteps(response.body().getData().getOrder_status());
                        if (response.body().getData().getOrder_products().size()==0){
                            rvRecycle.setVisibility(View.GONE);
                        }else {
                            mFamilyOrderDetailsAdapter.updateAll(response.body().getData().getOrder_products());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();


            }
        });
    }
}
