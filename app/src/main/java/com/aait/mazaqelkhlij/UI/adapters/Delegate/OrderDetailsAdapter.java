package com.aait.mazaqelkhlij.UI.adapters.Delegate;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentRecyclerAdapter;
import com.aait.mazaqelkhlij.base.ParentRecyclerViewHolder;
import com.aait.mazaqelkhlij.models.OrderModel;
import com.aait.mazaqelkhlij.models.OrderProductModel;
import com.bumptech.glide.Glide;

import java.text.MessageFormat;
import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class OrderDetailsAdapter extends ParentRecyclerAdapter<OrderProductModel> {

    public OrderDetailsAdapter(final Context context, final List<OrderProductModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        OrderProductModel foodModel = data.get(position);

        Glide.with(mcontext).load(foodModel.getProduct_image()).asBitmap().placeholder(R.mipmap.splash)
                .into(viewHolder.civOrderImage);
        viewHolder.tvHeaderItemName.setText(foodModel.getProduct_name());
        viewHolder.tvItemName.setText(foodModel.getProduct_name());
        viewHolder.tvItemNumber.setText(foodModel.getProduct_count() + " ");
        viewHolder.tvTotalCoast.setText(foodModel.getProduct_price() + " "+mcontext.getResources().getString(R.string.SAR));

        // check if the food has discount or not .
        if (foodModel.isProduct_offer()==true) {
            viewHolder.tvItemPrice
                    .setText(MessageFormat.format("{0} {1}", foodModel.getProduct_original_offer_price(), mcontext.getResources()
                            .getString(R.string.SAR)));
        } else {
            viewHolder.tvItemPrice
                    .setText(MessageFormat.format("{0} {1}", foodModel.getProduct_original_price(), mcontext.getResources()
                            .getString(R.string.SAR)));
        }



    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends ParentRecyclerViewHolder {


        @BindView(R.id.civ_order_image)
        CircleImageView civOrderImage;

        @BindView(R.id.tv_header_item_name)
        TextView tvHeaderItemName;

        @BindView(R.id.tv_item_price)
        TextView tvItemPrice;

        @BindView(R.id.tv_item_name)
        TextView tvItemName;

        @BindView(R.id.tv_item_number)
        TextView tvItemNumber;



        @BindView(R.id.tv_total_coast)
        TextView tvTotalCoast;



        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);


        }

    }
}

