package com.aait.mazaqelkhlij.UI.adapters.Provider;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentRecyclerAdapter;
import com.aait.mazaqelkhlij.base.ParentRecyclerViewHolder;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.ListModel;

import java.util.List;

import butterknife.BindView;

public class CategoryChooseAdapter extends ParentRecyclerAdapter<ListModel> {


public CategoryChooseAdapter(final Context context, final List<ListModel> data) {
        super(context, data);
        }

@Override
public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(R.layout.recycle_category_choose_row_family, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
       // holder.setClickableRootView(holder.layContainer);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
        }

@Override
public int getItemCount() {
        return data.size();
        }
 @Override
 public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
   final ViewHolder viewHolder = (ViewHolder) holder;
//        ListModel categoryModel = data.get(position);
//        viewHolder.tvName.setText(categoryModel.getName());
//
//        viewHolder.lay_item.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               // viewHolder.lay_item.setBackgroundColor(ContextCompat.getColor(mcontext,R.color.colorRowPrimary));
//                v.setBackgroundColor(ContextCompat.getColor(mcontext,R.color.colorRowPrimary));
//                itemClickListener.onItemClick(v, position);
//
//
//            }
//        });



//        if (position.isCheckCategory()) {
//        viewHolder.ivCheck.setVisibility(View.VISIBLE);
//        } else {
//        viewHolder.ivCheck.setVisibility(View.GONE);
//        }
        }


public class ViewHolder extends ParentRecyclerViewHolder {

//    @BindView(R.id.tv_name)
//    TextView tvName;
//
//    @BindView(R.id.lay_item)
//    LinearLayout lay_item;
//
//    @BindView(R.id.lay_container)
//    RelativeLayout layContainer;

    ViewHolder(View itemView) {
        super(itemView);
    }

}
}

