package com.aait.mazaqelkhlij.UI.activities.Client;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.BaseResponse;
import com.aait.mazaqelkhlij.models.CartProductsModel;
import com.aait.mazaqelkhlij.models.ProductModel;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditOrderItemActivity extends ParentActivity implements OnItemClickListener {

    @BindView(R.id.iv_image)
    ImageView ivImage;

    @BindView(R.id.tv_food_name)
    TextView tvFoodName;

    @BindView(R.id.tv_normal_price)
    TextView tvNormalPrice;

    @BindView(R.id.tv_discount_price)
    TextView tvDiscountPrice;

    @BindView(R.id.tv_description)
    TextView tvDescription;

    @BindView(R.id.ed_order_number)
    EditText edOrderNumber;

    @BindView(R.id.rv_Recycle)
    RecyclerView rvRecycle;

    LinearLayoutManager linearLayoutManager;



    CartProductsModel mFoodModel;

    public static void startActivity(AppCompatActivity mAppCompatActivity, CartProductsModel productModel) {
        Intent mIntent = new Intent(mAppCompatActivity, EditOrderItemActivity.class);
        mIntent.putExtra(Constant.BundleData.FOOD_MODEL, productModel);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData() {
        mFoodModel = (CartProductsModel) getIntent().getSerializableExtra(Constant.BundleData.FOOD_MODEL);

    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.product_name));
        getBundleData();
        setData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_edit_order_item_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @OnClick(R.id.iv_minus)
    void onMinusClick() {
        if (Integer.parseInt(edOrderNumber.getText().toString()) == 1) {

        } else {
            edOrderNumber.setText((Integer.parseInt(edOrderNumber.getText().toString()) - 1) + "");
        }
    }

    @OnClick(R.id.iv_add)
    void onAddClick() {
        edOrderNumber.setText((Integer.parseInt(edOrderNumber.getText().toString()) + 1) + "");
    }

    @OnClick(R.id.btn_add_to_card)
    void onBtnAddToCardClick() {

        edit();

    }

    void setData() {
        Glide.with(mContext).load(mFoodModel.getProduct_image()).asBitmap().placeholder(R.mipmap.splash)
                .into(ivImage);
        tvFoodName.setText(mFoodModel.getProduct_name());
        tvNormalPrice.setText(mFoodModel.getTotal_product_price() + " " + mContext.getResources().getString(R.string.SAR));
        tvDiscountPrice
                .setText(mFoodModel.getTotal_product_price() + " " + mContext.getResources().getString(R.string.SAR));
        tvDescription.setText(mFoodModel.getProduct_name());
        edOrderNumber.setText(mFoodModel.getProduct_count()+"");
    }
    private void edit(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).updateCart(mSharedPrefManager.getUserData().getUser_id(),mFoodModel.getCart_product_id(),Integer.parseInt(edOrderNumber.getText().toString())).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        onBackPressed();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
