package com.aait.mazaqelkhlij.UI.activities.Client;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.Client.CardBasketAdapter;
import com.aait.mazaqelkhlij.UI.adapters.Client.FamilyOrderDetailsAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.DialogUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.BaseResponse;
import com.aait.mazaqelkhlij.models.CartModel;
import com.aait.mazaqelkhlij.models.CartProductsModel;
import com.aait.mazaqelkhlij.models.CartResponse;
import com.aait.mazaqelkhlij.models.UserModel;
import com.google.gson.Gson;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CardBasketActivity extends ParentActivity implements OnItemClickListener {

    @BindView(R.id.tv_total_coast)
    TextView tvTotalCoast;

    @BindView(R.id.lay_confirm)
    LinearLayout lay_confirm;


    AlertDialog alertDialog;

    LinearLayoutManager linearLayoutManager;

    CardBasketAdapter mCardBasketAdapter;

    List<CartModel> mBasketModels = new ArrayList<>();
    FamilyOrderDetailsAdapter mFamilyOrderDetailsAdapter;


    List<CartProductsModel> mFamilyOrderModels = new ArrayList<>();

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, CardBasketActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.basket));
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mCardBasketAdapter = new CardBasketAdapter(mContext, mBasketModels, R.layout.recycle_card_basket_client);
        mCardBasketAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(mCardBasketAdapter);
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);


        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        if (mSharedPrefManager.getLoginStatus()) {
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getCart();
                }
            });
        }else {

        }


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_card_basket_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(View view, int position) {


        FamilyOrdersActivty.startActivity((AppCompatActivity)mContext,mBasketModels.get(position));
    }
    @Override
    protected void onResume() {
        super.onResume();
        getCart();
    }

    @OnClick(R.id.btn_confirm_order)
    void onConfirmOrder() {
        alertDialog = DialogUtil.showconfirm(mContext, R.string.confirm_order,
                getString(R.string.confirm_order_message), true,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialogInterface, final int i) {
                        alertDialog.dismiss();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialogInterface, final int i) {
                        Addorder();
                    }
                });
    }
    void setCartData(CartResponse cartData) {
        lay_confirm.setVisibility(View.VISIBLE);
        int price = 0;
        for (int i =0;i<cartData.getData().size();i++){
            price =price +(cartData.getData().get(i).getDelegate_price()+cartData.getData().get(i).getTotal_price());

        }
        tvTotalCoast.setText(
                MessageFormat.format("{0} {1}", price, getResources()
                        .getString(R.string.SAR)));
    }
    private void getCart(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getUserPlaylists("3").enqueue(new Callback<List<UserModel>>() {
            @Override
            public void onResponse(Call<List<UserModel>> call, Response<List<UserModel>> response) {

            }

            @Override
            public void onFailure(Call<List<UserModel>> call, Throwable t) {

            }
        });
        RetroWeb.getClient().create(ServiceApi.class).showCart(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id()).enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.no_data);
                            lay_confirm.setVisibility(View.GONE);
                        }else {
                            setCartData(response.body());
                            mCardBasketAdapter.updateAll(response.body().getData());

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {
                CommonUtil.onPrintLog(new Gson().toJson(t));
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    private void Addorder(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).doOrder(mSharedPrefManager.getUserData().getUser_id()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.order_added_succ));
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }

            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
