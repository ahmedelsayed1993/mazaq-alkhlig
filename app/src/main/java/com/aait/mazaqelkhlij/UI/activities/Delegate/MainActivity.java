package com.aait.mazaqelkhlij.UI.activities.Delegate;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.fragments.Delegate.NavigationFragment;
import com.aait.mazaqelkhlij.UI.fragments.Delegate.NewOrderFragment;
import com.aait.mazaqelkhlij.UI.fragments.Delegate.OnTheWayOrderFragment;
import com.aait.mazaqelkhlij.UI.fragments.Delegate.ProcessedOrderFragment;
import com.aait.mazaqelkhlij.UI.views.BottomNavigationViewHelper;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.DrawerListner;

import java.util.Stack;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity  extends ParentActivity
        implements DrawerListner, BottomNavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemReselectedListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.nav_view)
    FrameLayout navView;

    @BindView(R.id.app_bar)
    AppBarLayout app_bar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.iv_menu)
    ImageView ivMenu;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;

    NavigationFragment mNavigationFragment;

    NewOrderFragment mNewOrderFragment;

//    StoreFragment mStoreFragment;

    ProcessedOrderFragment mProcessedOrderFragment;

    OnTheWayOrderFragment mOnTheWayOrderFragment;

    int selectedTab = 0;

    private Stack<Integer> tabsStack = new Stack<>();

    private FragmentManager fragmentManager;

    private FragmentTransaction transaction;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, MainActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    @OnClick(R.id.iv_menu)
    void onMenuClick() {
        this.OpenCloseDrawer();
    }

    @Override
    public void OpenCloseDrawer() {
        mNavigationFragment.setNavData();
        if (drawerLayout != null) {
            if (mLanguagePrefManager.getAppLanguage().equals("en")) {
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            } else {
                if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        }
    }


    @Override
    protected void initializeComponents() {
        //CommonUtil.onPrintLog(mSharedPrefManager.getUserData());
        mNavigationFragment = NavigationFragment.newInstance();
        mNavigationFragment.setDrawerListner(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_view, mNavigationFragment).commit();
        initializeBottomNav();
        BottomNavigationViewHelper.disableShiftMode(bottomNavigation);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main_delivery;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onNavigationItemReselected(@NonNull final MenuItem item) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_on_the_way:
                showHome(true);
                break;
            case R.id.navigaton_new_order:
                showNewOrders(true);
                break;
            case R.id.navigation_processed_orders:
                showProcessedOrders(true);
                break;
        }

        return true;
    }

    public void initializeBottomNav() {

//        mStoreFragment = StoreFragment.newInstance();
        mProcessedOrderFragment = ProcessedOrderFragment.newInstance();
        mOnTheWayOrderFragment = OnTheWayOrderFragment.newInstance();
        mNewOrderFragment = NewOrderFragment.newInstance().newInstance();

        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.home_fragment_container, mOnTheWayOrderFragment);
        transaction.add(R.id.home_fragment_container, mNewOrderFragment);
        transaction.add(R.id.home_fragment_container, mProcessedOrderFragment);
//        transaction.add(R.id.home_fragment_container, mStoreFragment);
        transaction.commit();
        bottomNavigation.setOnNavigationItemSelectedListener(this);
        bottomNavigation.setOnNavigationItemReselectedListener(this);
        bottomNavigation.setSelectedItemId(R.id.navigation_on_the_way);
        showHome(true);
    }

    private void showProcessedOrders(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
        transaction.hide(mOnTheWayOrderFragment);
        transaction.hide(mNewOrderFragment);
        transaction.show(mProcessedOrderFragment);
        transaction.commit();
        selectedTab = R.id.navigation_processed_orders;
        tvTitle.setText(R.string.processed_orders);
    }

    private void showNewOrders(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
        transaction.hide(mOnTheWayOrderFragment);
        transaction.hide(mProcessedOrderFragment);
        transaction.show(mNewOrderFragment);
        transaction.commit();
        selectedTab = R.id.navigation_processed_orders;
        tvTitle.setText(R.string.new_orders);
    }

    private void showHome(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
        transaction.hide(mProcessedOrderFragment);
        transaction.hide(mNewOrderFragment);
        transaction.show(mOnTheWayOrderFragment);
        transaction.commit();
        selectedTab = R.id.navigation_on_the_way;

        tvTitle.setText(R.string.on_the_way);
    }


    @Override
    public void onBackPressed() {
        if (tabsStack.size() > 0) {
            bottomNavigation.setOnNavigationItemSelectedListener(null);
            int selectedTab = tabsStack.pop();
            bottomNavigation.setSelectedItemId(selectedTab);
            switch (selectedTab) {
                case R.id.navigation_on_the_way:
                    showHome(false);
                    break;
                case R.id.navigaton_new_order:
                    showNewOrders(false);
                    break;
                case R.id.navigation_processed_orders:
                    showProcessedOrders(false);
                    break;
            }
            bottomNavigation.setOnNavigationItemSelectedListener(this);
        } else {
//            DialogUtil.showAlertDialog(mContext, getString(R.string.need_to_close_the_app), new OnClickListener() {
//                @Override
//                public void onClick(final DialogInterface dialog, final int which) {
//                    MainActivity.super.onBackPressed();
//                }
//            }, new OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
            MainActivity.this.finish();
        }
    }

}
