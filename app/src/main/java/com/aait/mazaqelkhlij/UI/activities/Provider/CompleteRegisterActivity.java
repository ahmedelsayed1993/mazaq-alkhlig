package com.aait.mazaqelkhlij.UI.activities.Provider;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.ConfirmCodeActivity;
import com.aait.mazaqelkhlij.UI.fragments.Provider.ChooseActivitiesDialogFragment;
import com.aait.mazaqelkhlij.UI.views.CategoriesDialog;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.ProgressRequestBody;
import com.aait.mazaqelkhlij.Uitls.ValidationUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.ChooseCategoryListner;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.CatModel;
import com.aait.mazaqelkhlij.models.CategoriesResponse;
import com.aait.mazaqelkhlij.models.CategoryModel;
import com.aait.mazaqelkhlij.models.ListModel;
import com.aait.mazaqelkhlij.models.ListModelResponse;
import com.aait.mazaqelkhlij.models.ProviderModel;
import com.aait.mazaqelkhlij.models.RegisterResponse;
import com.google.gson.Gson;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompleteRegisterActivity extends ParentActivity implements OnItemClickListener,
        ProgressRequestBody.UploadCallbacks,ChooseCategoryListner {
    @BindView(R.id.til_store_name)
    TextInputLayout tilStoreName;

    @BindView(R.id.et_store_name)
    TextInputEditText etStoreName;

    @BindView(R.id.til_activity)
    TextInputLayout tilActivity;

    @BindView(R.id.et_activity)
    TextInputEditText etActivity;

    @BindView(R.id.myRadioGroup)
    RadioGroup myRadioGroup;

    @BindView(R.id.radio_yes)
    RadioButton radioYes;

    @BindView(R.id.radio_no)
    RadioButton radioNo;

    @BindView(R.id.radio_family)
    RadioButton radioFamily;

    @BindView(R.id.radio_truck)
    RadioButton radioTruck;

    @BindView(R.id.til_delivery_phone)
    TextInputLayout tilDeliveryPhone;

    @BindView(R.id.et_delivery_phone)
    TextInputEditText etDeliveryPhone;

    @BindView(R.id.til_delivry_cost)
    TextInputLayout tilDelivryCost;

    @BindView(R.id.et_delivery_cost)
    TextInputEditText etDeliveryCost;

    @BindView(R.id.lay_show_has_delivery)
    LinearLayout layShowHasDelivery;

    boolean isHasDelivery = false;

    String CategoriesList;
    String type;
    CategoriesDialog categoriesDialog;
    ListModel listModel;
    ArrayList<CategoryModel> category=new ArrayList<>();
    CategoryModel model;
    ArrayList<ListModel> categoryModel = new ArrayList<>();

    ArrayList<ListModel> listModels =new ArrayList<>();
    ArrayList<ArrayList<ListModel>> categories = new ArrayList<>();
    List<String > names = new ArrayList<>();
    List<Integer > ids = new ArrayList<>();
    String id;
   ProviderModel mRegisterSendModel;

    public static void startActivity(AppCompatActivity mAppCompatActivity, ProviderModel registerSendModel,String type) {
        Intent mIntent = new Intent(mAppCompatActivity, CompleteRegisterActivity.class);
        mIntent.putExtra(Constant.BundleData.REGISTER_MODEL, registerSendModel);
        mIntent.putExtra("type",type);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }


    void getBundleData() {
        mRegisterSendModel = (ProviderModel) getIntent().getSerializableExtra(Constant.BundleData.REGISTER_MODEL);
        type = (String)getIntent().getSerializableExtra("type");
    }
    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    @Override
    protected void initializeComponents() {
        getBundleData();
        radioYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                isHasDelivery = b;
                if (b) {
                    layShowHasDelivery.setVisibility(View.VISIBLE);
                } else {
                    layShowHasDelivery.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register_complete_family;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @OnClick(R.id.et_activity)
    void onChooseActivitiesClick() {
        getCategories();
    }



    boolean registerValidation() {
        if (!ValidationUtils.checkError(etStoreName, tilStoreName, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etActivity, tilActivity, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }




    @OnClick(R.id.btn_register)
    void onBtnRegisterClick() {
        if (registerValidation()){
            Register(mRegisterSendModel.getAvatar());
        }
    }

    private void getCategories(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCategory(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){

                    if (response.body().getData().size()!=0){
                        showCategories(getSupportFragmentManager(),new ArrayList(response.body().getData()));

                    }


                    else {

                    }
                }
            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                Log.e("nnn",new Gson().toJson(t));
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }



    private void Register(String pathfromimage){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(pathfromimage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, CompleteRegisterActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).providerRegister(mLanguagePrefManager.getAppLanguage(),"2",mRegisterSendModel.getName(),
                mRegisterSendModel.getPassword(),mRegisterSendModel.getEmail(),mRegisterSendModel.getPhone(),mRegisterSendModel.getCity_id(),
                mRegisterSendModel.getLat(),mRegisterSendModel.getLng(),mRegisterSendModel.getNationality_id(),mRegisterSendModel.getCivil_number(),
                etStoreName.getText().toString(),CategoriesList,filePart).enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        Log.e("jjjjjj",new Gson().toJson(response.body()));
                        CommonUtil.makeToast(mContext,getString(R.string.enter_confirmation_code));
                        ConfirmCodeActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getUser_id()+"");
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }

            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Log.e("iiiii", new Gson().toJson(t));
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    void showCategories(FragmentManager mFragmentManager, ArrayList<CatModel> categoryModels) {
        CommonUtil.PrintLogE("Categories size : " + categoryModels.size());
        ChooseActivitiesDialogFragment chooseActivitiesDialogFragment = ChooseActivitiesDialogFragment
                .newInstance(categoryModels);
        chooseActivitiesDialogFragment.show(mFragmentManager, "Choose Categories");
        chooseActivitiesDialogFragment.setListner(this);
    }

    void setCategoryName(ArrayList<CatModel> categoryName) {
        etActivity.setText("");
        int i = 0;
        String id = "";
        for (CatModel categoryModel : categoryName) {
            i++;
            if (categoryModel.isCheckCategory()) {
                if (i != categoryName.size()) {
                    id = id+categoryModel.getId()+",";
                    etActivity.setText(etActivity.getText().toString() + categoryModel.getName() + " , ");
                } else {
                    id = id + categoryModel.getId() + "";
                    etActivity.setText(etActivity.getText().toString() + categoryModel.getName() + " ");
                }
            }
        }

        CategoriesList = id;
        CommonUtil.PrintLogE("Categories" + CategoriesList);
    }

    @Override
    public void onItemClick(View view, int position) {

//        listModel =listModels.get(position);
//        categoryModel.add(listModel);
//        categories.add(categoryModel);
//
//        for (int i = 0;i<categories.size();i++){
//          model =new CategoryModel (categories.get(i));
//          category.add(model);
//
//            for (int j =0;j<category.size();j++){
//
//                for (int w = 0;w<categories.size();w++) {
//
//                }
//
//            }
//
//
//        }
//        Log.e("name",category.get(category.size()-1).getListModels().get(categories.size()-1).getName()+",");
//        names.add(category.get(category.size()-1).getListModels().get(categories.size()-1).getName()+",");
//        ids.add(category.get(category.size()-1).getListModels().get(categories.size()-1).getId());
//        Log.e("nnn",names+"");
//        Log.e("iii",ids+"");
//        //String ID= ids.toString().replace("[","").replace("]","");
//        id = ids.toString().replace("[","").replace("]","");
//        StringBuilder builder = new StringBuilder();
//        for (String value : names) {
//            builder.append(value);
//        }
//        String text = builder.toString();
//        etActivity.setText(text);
//
//
//        Log.e("idss",id);
//
//
//



    }

    @Override
    public void ChooseCategories(ArrayList<CatModel> categoryModels) {
        setCategoryName(categoryModels);
    }
}
