package com.aait.mazaqelkhlij.UI.activities.Provider;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.Provider.OrderDetailsAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.OrderDetailsResponse;
import com.aait.mazaqelkhlij.models.OrderModel;
import com.aait.mazaqelkhlij.models.OrderProductModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreviousOrderDetailsActivity extends ParentActivity {

    @BindView(R.id.iv_user_image)
    CircleImageView ivUserImage;

    @BindView(R.id.tv_order_time)
    TextView tvOrderTime;

    @BindView(R.id.tv_user_name)
    TextView tvUserName;

    @BindView(R.id.tv_city)
    TextView tvCity;

    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    LinearLayoutManager linearLayoutManager;

    OrderDetailsAdapter mFamilyOrderDetailsAdapter;

    List<OrderProductModel> mFamilyOrderModels = new ArrayList<>();

    OrderModel mOrderModel;
    int Order_id;

    public static void startActivity(AppCompatActivity mAppCompatActivity,int Order_id) {
        Intent mIntent = new Intent(mAppCompatActivity, PreviousOrderDetailsActivity.class);
        mIntent.putExtra(Constant.BundleData.ORDER, Order_id);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData() {
        Order_id =  getIntent().getIntExtra(Constant.BundleData.ORDER,0);
       // setOrderData(mOrderModel);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.order_details));
        getBundleData();
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mFamilyOrderDetailsAdapter = new OrderDetailsAdapter(mContext, mFamilyOrderModels,
                R.layout.recycle_family_order_details_family);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(mFamilyOrderDetailsAdapter);
        getOrderDetails();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_previous_order_details_family;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    void setOrderData(OrderModel orderData) {
        Glide.with(mContext).load(orderData.getOrder_user_avatar()).asBitmap()
                .into(ivUserImage);
        tvUserName.setText(orderData.getOrder_user_name());
        tvCity.setText(orderData.getOrder_user_area());
        tvOrderNumber.setText(orderData.getOrder_id() + "");
        tvOrderTime.setText(orderData.getOrder_created_at());
    }

    private void getOrderDetails(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getDetails(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Order_id).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        setOrderData(response.body().getData());
                      //  processOrderSteps(response.body().getData());
                        mOrderModel = response.body().getData();
                        if (response.body().getData().getOrder_products().size()==0){
                           rvRecycle.setVisibility(View.GONE);
                        }else {
                            mFamilyOrderDetailsAdapter.updateAll(response.body().getData().getOrder_products());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();
               rvRecycle.setVisibility(View.GONE);

            }
        });
    }
}
