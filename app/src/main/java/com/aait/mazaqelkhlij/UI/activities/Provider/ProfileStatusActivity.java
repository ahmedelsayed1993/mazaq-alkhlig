package com.aait.mazaqelkhlij.UI.activities.Provider;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentActivity;

import butterknife.OnClick;

public class ProfileStatusActivity extends ParentActivity {

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, ProfileStatusActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.profile));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_profile_status_family;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @OnClick(R.id.btn_profile_data)
    void onBtnProfileDataClick() {
        ProfileActivity.startActivity((AppCompatActivity) mContext);
    }

    @OnClick(R.id.btn_store_data)
    void onBtnStoreDataClick() {
        StoreProfileActivity.startActivity((AppCompatActivity) mContext);
    }
}
