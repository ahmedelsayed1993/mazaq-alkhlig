package com.aait.mazaqelkhlij.UI.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.TermsResponse;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutUsActivity extends ParentActivity {
    @BindView(R.id.tv_about_app)
    TextView tvAboutApp;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, AboutUsActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.about_app));
        getAboutApp();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_about_app_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    private void getAboutApp() {
       showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAbout(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<TermsResponse>() {
            @Override
            public void onResponse(Call<TermsResponse> call, Response<TermsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                   if (response.body().issucessfull()){
                       tvAboutApp.setText(response.body().getData());
                   }
                }
            }

            @Override
            public void onFailure(Call<TermsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
