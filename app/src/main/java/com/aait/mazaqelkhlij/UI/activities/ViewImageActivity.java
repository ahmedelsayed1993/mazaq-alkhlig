package com.aait.mazaqelkhlij.UI.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.pnikosis.materialishprogress.ProgressWheel;

import butterknife.BindView;

public class ViewImageActivity extends ParentActivity {

    @BindView(R.id.progress_wheel)
    ProgressWheel progressWheel;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, ViewImageActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_images_preview_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
}
