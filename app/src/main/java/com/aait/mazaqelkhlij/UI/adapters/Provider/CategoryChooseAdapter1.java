package com.aait.mazaqelkhlij.UI.adapters.Provider;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentRecyclerAdapter;
import com.aait.mazaqelkhlij.base.ParentRecyclerViewHolder;
import com.aait.mazaqelkhlij.models.CategoriesModel;

import java.util.List;

import butterknife.BindView;

public class CategoryChooseAdapter1 extends ParentRecyclerAdapter<CategoriesModel> {


    public CategoryChooseAdapter1(final Context context, final List<CategoriesModel> data) {
        super(context, data);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        View viewItem = inflater.inflate(R.layout.recycler_dialog_list_row_client, parent, false);
        viewHolder = new ViewHolder(viewItem);

        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final CategoryChooseAdapter1.ViewHolder viewHolder = (CategoryChooseAdapter1.ViewHolder) holder;
        CategoriesModel categoryModel = data.get(position);
        viewHolder.tv_row_title.setText(categoryModel.getCategory_name());

        if (position % 2 == 0) {
            viewHolder.tv_row_title.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorRowSecondary));
        } else {
            viewHolder.tv_row_title.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorRowPrimary));

        }

        viewHolder.tv_row_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Log.e("postion",position+"");
                itemClickListener.onItemClick(view, position);
            }
        });



//        if (position.isCheckCategory()) {
//        viewHolder.ivCheck.setVisibility(View.VISIBLE);
//        } else {
//        viewHolder.ivCheck.setVisibility(View.GONE);
//        }
    }


    protected class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_row_title)
        TextView tv_row_title;

        public ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}




