package com.aait.mazaqelkhlij.UI.fragments.Delegate;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.Delegate.ProcessedOrdersDetailsActivity;
import com.aait.mazaqelkhlij.UI.adapters.Delegate.ProcessedOrderAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.BaseFragment;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.OrderModel;
import com.aait.mazaqelkhlij.models.OrderResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProcessedOrderFragment extends BaseFragment implements OnItemClickListener {

    LinearLayoutManager linearLayoutManager;

    ProcessedOrderAdapter mProcessedOrderAdapter;

    List<OrderModel> mOrderModels = new ArrayList<>();

    private static final int PAGE_START = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;

    public static ProcessedOrderFragment newInstance() {
        Bundle args = new Bundle();
        ProcessedOrderFragment fragment = new ProcessedOrderFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_on_progress_order_delivery;
    }

    @Override
    protected void initializeComponents(View view) {
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mProcessedOrderAdapter = new ProcessedOrderAdapter(mContext, mOrderModels);
        mProcessedOrderAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setItemAnimator(new DefaultItemAnimator());
        rvRecycle.setAdapter(mProcessedOrderAdapter);


        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(false);
               getCurrentOrders();
            }
        });
        getCurrentOrders();
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent=new Intent(getContext(),ProcessedOrdersDetailsActivity.class);
        intent.putExtra("id",mOrderModels.get(position).getOrder_id());
        startActivity(intent);
     //  com.aait.mazaqelkhlij.UI.activities.Delegate.ProccessedOrderDetailsActivity.startActivity((AppCompatActivity)mContext,mOrderModels.get(position).getOrder_id());

    }
    private void getCurrentOrders(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getDelegateCurrentOrders(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getUser_id())
                .enqueue(new Callback<OrderResponse>() {
                    @Override
                    public void onResponse(Call<OrderResponse> call,
                                           Response<OrderResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()==1){
                            if (response.body().getData().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                mProcessedOrderAdapter.updateAll(response.body().getData());
                            }}
                            else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<OrderResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
}
