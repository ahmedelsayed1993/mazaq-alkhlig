package com.aait.mazaqelkhlij.UI.activities.Delegate;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.Client.ProcessedOrderDetailsActivity;
import com.aait.mazaqelkhlij.UI.adapters.Delegate.OrderDetailsAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.DialogUtil;
import com.aait.mazaqelkhlij.Uitls.PermissionUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.ChangeOrderStatusResponse;
import com.aait.mazaqelkhlij.models.OrderDetailsResponse;
import com.aait.mazaqelkhlij.models.OrderModel;
import com.aait.mazaqelkhlij.models.OrderProductModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProcessedOrdersDetailsActivity extends ParentActivity{

    @BindView(R.id.tv_order_time)
    TextView tvOrderTime;

    @BindView(R.id.iv_clock)
    ImageView ivClock;

    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;

    @BindView(R.id.iv_client_image)
    CircleImageView ivClientImage;

    @BindView(R.id.tv_client_name)
    TextView tvClientName;

    @BindView(R.id.tv_client_city)
    TextView tvClientCity;

    @BindView(R.id.tv_client_mobile)
    TextView tvClientMobile;

    @BindView(R.id.iv_family_image)
    CircleImageView ivFamilyImage;

    @BindView(R.id.tv_family_name)
    TextView tvFamilyName;

    @BindView(R.id.tv_family_city)
    TextView tvFamilyCity;

    @BindView(R.id.tv_family_mobile)
    TextView tvFamilyMobile;

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.tv_total_coast)
    TextView tv_total_coast;

    @BindView(R.id.iv_step_one)
    ImageView ivStepOne;

    @BindView(R.id.tv_step_one)
    TextView tvStepOne;

    @BindView(R.id.iv_step_two)
    ImageView ivStepTwo;

    @BindView(R.id.tv_step_two)
    TextView tvStepTwo;

    @BindView(R.id.iv_step_three)
    ImageView ivStepThree;

    @BindView(R.id.tv_step_three)
    TextView tvStepThree;

    @BindView(R.id.iv_step_four)
    ImageView ivStepFour;

    @BindView(R.id.tv_step_four)
    TextView tvStepFour;

    @BindView(R.id.iv_order_status)
    ImageView ivOrderStatus;

    @BindView(R.id.btn_process_order_steps)
    Button btn_process_order_steps;

    LinearLayoutManager linearLayoutManager;

    OrderDetailsAdapter mFamilyOrderDetailsAdapter;

    List<OrderProductModel> mFamilyOrderModels = new ArrayList<>();

    int mOrderModel;

//    public static void startActivity(AppCompatActivity mAppCompatActivity/*, int orderModel*/) {
//        Intent mIntent = new Intent(mAppCompatActivity, PreviousOrdersDetailsActivity.class);
//        //mIntent.putExtra(Constant.BundleData.ORDER, orderModel);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        mAppCompatActivity.startActivity(mIntent);
//    }

    void getBundleData() {
       //mOrderModel = getIntent().getIntExtra(Constant.BundleData.ORDER,0);

    }
//    @Override
//    protected void initializeComponents() {
//        setToolbarTitle(getString(R.string.order_details));
//        Log.e("sdfghj","vcxcvbnm");
//        getBundleData();
//        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
//        mFamilyOrderDetailsAdapter = new OrderDetailsAdapter(mContext, mFamilyOrderModels,
//                R.layout.recycle_family_order_details_delivery);
//        rvRecycle.setLayoutManager(linearLayoutManager);
//        rvRecycle.setAdapter(mFamilyOrderDetailsAdapter);
//        getDetails();
//    }

    @Override
    protected void initializeComponents() {

        mOrderModel=getIntent().getIntExtra("id",0);
        getDetails();
                setToolbarTitle(getString(R.string.order_details));
//        Log.e("sdfghj","vcxcvbnm");
//        getBundleData();
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mFamilyOrderDetailsAdapter = new OrderDetailsAdapter(mContext, mFamilyOrderModels,
                R.layout.recycle_family_order_details_delivery);
        rvRecycle.setLayoutManager(linearLayoutManager);
       rvRecycle.setAdapter(mFamilyOrderDetailsAdapter);
//        getDetails();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_processed_order_details_delivery;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @OnClick(R.id.btn_process_order_steps)
    void processedOrderSteps() {
        DialogUtil.showAlertDialog(mContext, getString(R.string.process_orerd_description), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialogInterface, final int i) {
              ChangeOrder();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    void setOrderData(OrderModel orderData) {

        Glide.with(mContext).load(orderData.getOrder_user_avatar()).asBitmap()
                .into(ivClientImage);
        Glide.with(mContext).load(orderData.getOrder_provider_avatar()).asBitmap()
                .into(ivFamilyImage);
        tvOrderTime
                .setText(orderData.getOrder_created_at() + " ");
        tvOrderNumber
                .setText(orderData.getOrder_id() + "");

        tvClientName.setText(orderData.getOrder_user_name());
        tvFamilyName.setText(orderData.getOrder_provider_name());

        tvClientCity.setText(orderData.getOrder_user_area());
        tvFamilyCity.setText(orderData.getOrder_provider_area());

        tvClientMobile.setText(orderData.getOrder_user_phone());
        tvFamilyMobile.setText(orderData.getOrder_provider_phone());

        tv_total_coast.setText((orderData.getTotal_price()-orderData.getDelegate_price())+"");
        tvOrderTime.setText(orderData.getOrder_created_at());

    }
    @OnClick(R.id.tv_client_mobile)
    void onClient(){
        if (!tvClientMobile.getText().toString().equals("")){
            getLocationWithPermission(tvClientMobile.getText().toString());
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.no_data));
        }
    }
    @OnClick(R.id.tv_family_mobile)
    void onFamily(){
        if (!tvFamilyMobile.getText().toString().equals("")){
            getLocationWithPermission(tvFamilyMobile.getText().toString());
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.no_data));
        }
    }
    public void getLocationWithPermission(String number) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(ProcessedOrdersDetailsActivity.this, PermissionUtils.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.CALL_PHONE,
                            Constant.RequestPermission.REQUEST_CALL);
                }
            } else {
                callnumber(number);
            }
        } else {
            callnumber(number);
        }

    }
    void callnumber(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        startActivity(intent);
    }
    void processOrderSteps(int status) {
        btn_process_order_steps.setVisibility(View.GONE);

        if (status > 4) {
            ivStepTwo.setImageResource(R.mipmap.step_two);
            tvStepTwo.setTextColor(getResources().getColor(R.color.order_step_2_color));
            ivOrderStatus.setImageDrawable(getResources().getDrawable(R.drawable.order_two));
            if (status == 6) {
                btn_process_order_steps.setVisibility(View.VISIBLE);
            }
        }
        if (status > 6) {
            ivStepThree.setImageResource(R.mipmap.step_three);
            tvStepThree.setTextColor(getResources().getColor(R.color.order_step_3_color));
            ivOrderStatus.setImageDrawable(getResources().getDrawable(R.drawable.order_three));
        }
        if (status > 7) {
            ivStepFour.setImageResource(R.mipmap.step_four);
            tvStepFour.setTextColor(getResources().getColor(R.color.order_step_4_color));
            ivOrderStatus.setImageDrawable(getResources().getDrawable(R.drawable.order_four));

        }
    }


    private void getDetails(){

        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getDetails(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),mOrderModel).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful())
                {
                    if (response.body().getStatus()==1){
                        setOrderData(response.body().getData());
                        processOrderSteps(response.body().getData().getOrder_status());
                        if (response.body().getData().getOrder_products().size()==0){
                            rvRecycle.setVisibility(View.GONE);
                        }else {
                            mFamilyOrderDetailsAdapter.updateAll(response.body().getData().getOrder_products());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();


            }
        });
    }
    private void ChangeOrder(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeOrder(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),mOrderModel,6).enqueue(new Callback<ChangeOrderStatusResponse>() {
            @Override
            public void onResponse(Call<ChangeOrderStatusResponse> call, Response<ChangeOrderStatusResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.order_is_finished));
                        com.aait.mazaqelkhlij.UI.activities.Delegate.MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangeOrderStatusResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
