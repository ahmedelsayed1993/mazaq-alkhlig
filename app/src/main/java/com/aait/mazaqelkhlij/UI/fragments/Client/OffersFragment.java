package com.aait.mazaqelkhlij.UI.fragments.Client;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.Client.OrderItemActivity;
import com.aait.mazaqelkhlij.UI.adapters.Client.OffersAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.BaseFragment;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.listeners.PaginationAdapterCallback;
import com.aait.mazaqelkhlij.listeners.PaginationScrollListener;
import com.aait.mazaqelkhlij.models.ProductModel;
import com.aait.mazaqelkhlij.models.SearchResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersFragment extends BaseFragment implements OnItemClickListener,PaginationAdapterCallback {

    LinearLayoutManager linearLayoutManager;

    OffersAdapter mOffersAdapter;

   List<ProductModel> mFoodModels = new ArrayList<>();

    private static final int PAGE_START = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;

    public static OffersFragment newInstance() {
        Bundle args = new Bundle();
        OffersFragment fragment = new OffersFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_offers_client;
    }

    @Override
    protected void initializeComponents(View view) {

        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mOffersAdapter = new OffersAdapter(mContext, mFoodModels);
        mOffersAdapter.setOnItemClickListener(this);
        mOffersAdapter.setOnPaginationClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setItemAnimator(new DefaultItemAnimator());
        rvRecycle.setAdapter(mOffersAdapter);

            rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
                @Override
                protected void loadMoreItems() {
                    isLoading = true;
                    currentPage += 1;
                }

                @Override
                public int getTotalPageCount() {
                    return TOTAL_PAGES;
                }

                @Override
                public boolean isLastPage() {
                    return isLastPage;
                }

                @Override
                public boolean isLoading() {
                    return isLoading;
                }

            });

            swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        if (mSharedPrefManager.getLoginStatus()) {
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getoffers(mSharedPrefManager.getUserData().getUser_id());
                }
            });

            getoffers(mSharedPrefManager.getUserData().getUser_id());
        }
        else
        {
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getoffers(0);
                }
            });

            getoffers(0);

        }
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(View view, int position) {

        OrderItemActivity.startActivity((AppCompatActivity)mContext,mFoodModels.get(position));
    }

    @Override
    public void retryPageLoad() {

    }

    private void getoffers(int user__id) {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getOffers(mLanguagePrefManager.getAppLanguage(), user__id)
                .enqueue(new Callback<SearchResponse>() {
                    @Override
                    public void onResponse(Call<SearchResponse> call,
                                           Response<SearchResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()==1) {
                                if (response.body().getData().size() == 0) {
                                    layNoItem.setVisibility(View.VISIBLE);
                                    layNoInternet.setVisibility(View.GONE);
                                    tvNoContent.setText(R.string.no_data);
                                } else {
                                    mOffersAdapter.updateAll(response.body().getData());
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SearchResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
}
