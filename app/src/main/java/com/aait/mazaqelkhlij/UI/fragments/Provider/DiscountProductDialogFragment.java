package com.aait.mazaqelkhlij.UI.fragments.Provider;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.ValidationUtils;
import com.aait.mazaqelkhlij.listeners.DiscountListner;
import com.aait.mazaqelkhlij.models.ProductModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DiscountProductDialogFragment extends DialogFragment {

    @BindView(R.id.tv_before_discount)
    TextView tvBeforeDiscount;

    @BindView(R.id.ed_discount)
    EditText edDiscount;

    @BindView(R.id.tv_cancel)
    TextView tvCancel;

    @BindView(R.id.tv_ok)
    TextView tvOk;

    int productID;

   ProductModel mFoodModel;

    private DiscountListner mDiscountListner;

    public DiscountProductDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public void setListner(DiscountListner mDiscountListner) {
        this.mDiscountListner = mDiscountListner;
    }

    public static DiscountProductDialogFragment newInstance(ProductModel foodModel) {
        Bundle args = new Bundle();
        DiscountProductDialogFragment fragment = new DiscountProductDialogFragment();
        args.putSerializable(Constant.BundleData.FOOD_MODEL, foodModel);
        fragment.setArguments(args);
        return fragment;
    }

    void getBundleData() {
        mFoodModel = (ProductModel) getArguments().getSerializable(Constant.BundleData.FOOD_MODEL);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_discount_product_family, container);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBundleData();
        tvBeforeDiscount.setText(mFoodModel.getProduct_price() + "");
    }

    @Override
    public void onResume() {
        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);
        getDialog().getWindow().setWindowAnimations(R.style.DialogAnimation_2);
        // Call super onResume after sizing
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.tv_ok)
    void onConfirmClick() {
       // CommonUtil.PrintLogE("Food price : " + mFoodModel.getPrice());
        CommonUtil.PrintLogE("discount price : " + Integer.parseInt(edDiscount.getText().toString()));

        if (ValidateDiscount()) {
            if (Integer.parseInt(mFoodModel.getProduct_price()) > Integer.parseInt(edDiscount.getText().toString())) {
                mDiscountListner.discountOf(edDiscount.getText().toString());
                dismiss();
            } else {
                CommonUtil.makeToast(getActivity(), getString(R.string.discount_big_than_old));
            }
        }
    }

    @OnClick(R.id.tv_cancel)
    void onCancelClick() {
        dismiss();
    }

    boolean ValidateDiscount() {
        if (!ValidationUtils.emptyValidation(edDiscount, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }


}
