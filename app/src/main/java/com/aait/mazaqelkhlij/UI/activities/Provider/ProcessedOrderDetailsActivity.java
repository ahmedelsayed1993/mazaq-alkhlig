package com.aait.mazaqelkhlij.UI.activities.Provider;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.Delegate.MainActivity;
import com.aait.mazaqelkhlij.UI.adapters.Provider.OrderDetailsAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.DialogUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.ChangeOrderStatusResponse;
import com.aait.mazaqelkhlij.models.OrderDetailsResponse;
import com.aait.mazaqelkhlij.models.OrderModel;
import com.aait.mazaqelkhlij.models.OrderProductModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProcessedOrderDetailsActivity extends ParentActivity {

    @BindView(R.id.iv_user_image)
    CircleImageView ivUserImage;

    @BindView(R.id.tv_order_time)
    TextView tvOrderTime;

    @BindView(R.id.tv_user_name)
    TextView tvUserName;

    @BindView(R.id.tv_city)
    TextView tvCity;

    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.iv_step_one)
    ImageView ivStepOne;

    @BindView(R.id.tv_step_one)
    TextView tvStepOne;

    @BindView(R.id.iv_step_two)
    ImageView ivStepTwo;

    @BindView(R.id.tv_step_two)
    TextView tvStepTwo;

    @BindView(R.id.iv_step_three)
    ImageView ivStepThree;

    @BindView(R.id.tv_step_three)
    TextView tvStepThree;

    @BindView(R.id.iv_order_status)
    ImageView ivOrderStatus;

    @BindView(R.id.btn_process_order_steps)
    Button btn_process_order_steps;
    @BindView(R.id.btn_process_order_steps1)
    Button btn_process_order_steps1;

    LinearLayoutManager linearLayoutManager;

    OrderDetailsAdapter mFamilyOrderDetailsAdapter;

    List<OrderProductModel> mFamilyOrderModels = new ArrayList<>();

    OrderModel mOrderModel;
    int Order_id;

    public static void startActivity(AppCompatActivity mAppCompatActivity,int Order_id) {
        Intent mIntent = new Intent(mAppCompatActivity, PreviousOrderDetailsActivity.class);
        mIntent.putExtra(Constant.BundleData.ORDER, Order_id);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }


    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.order_details));
        Order_id = getIntent().getIntExtra("id",0);
       // getBundleData();
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mFamilyOrderDetailsAdapter = new OrderDetailsAdapter(mContext, mFamilyOrderModels,
                R.layout.recycle_family_order_details_family);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(mFamilyOrderDetailsAdapter);
        getDetails();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_processed_order_details_family;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @OnClick(R.id.btn_process_order_steps)
    void processedOrderSteps() {
        DialogUtil.showAlertDialog(mContext, getString(R.string.order_is_finished_descrition), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialogInterface, final int i) {

                ChangeOrder();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

    }

    @OnClick(R.id.btn_process_order_steps1)
    void onBtnClick(){
        ChangeOrder1();
    }
    void setOrderData(OrderModel orderData) {
       // CommonUtil.onPrintLog(orderData);
        Glide.with(mContext).load(orderData.getOrder_user_avatar()).asBitmap()
                .into(ivUserImage);
        tvUserName.setText(orderData.getOrder_user_name());
        tvCity.setText(orderData.getOrder_user_area()+ "");
        tvOrderNumber.setText(orderData.getOrder_id() + "");
        tvOrderTime.setText(orderData.getOrder_created_at());
    }

    void processOrderSteps(int status) {
        btn_process_order_steps.setVisibility(View.GONE);
        btn_process_order_steps1.setVisibility(View.GONE);

        if (status == 4) {
            btn_process_order_steps.setVisibility(View.VISIBLE);
        }
        if (status >= 5) {
            ivStepTwo.setImageResource(R.mipmap.step_two);
            tvStepTwo.setTextColor(getResources().getColor(R.color.order_step_2_color));
            ivOrderStatus.setImageDrawable(getResources().getDrawable(R.drawable.order_two));

        }
        if (status == 5){
            btn_process_order_steps1.setVisibility(View.VISIBLE);
        }
        if (status >= 6) {
            ivStepThree.setImageResource(R.mipmap.step_three);
            tvStepThree.setTextColor(getResources().getColor(R.color.order_step_3_color));
            ivOrderStatus.setImageDrawable(getResources().getDrawable(R.drawable.order_three));

        }
    }

    private void getDetails(){

        RetroWeb.getClient().create(ServiceApi.class).getDetails(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Order_id).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {

                if (response.isSuccessful())
                {
                    if (response.body().getStatus()==1){
                        setOrderData(response.body().getData());
                         processOrderSteps(response.body().getData().getOrder_status());
                        if (response.body().getData().getOrder_products().size()==0){
                            rvRecycle.setVisibility(View.GONE);
                        }else {
                            mFamilyOrderDetailsAdapter.updateAll(response.body().getData().getOrder_products());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();


            }
        });
    }

    private void ChangeOrder(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeOrder(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Order_id,4).enqueue(new Callback<ChangeOrderStatusResponse>() {
            @Override
            public void onResponse(Call<ChangeOrderStatusResponse> call, Response<ChangeOrderStatusResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.order_is_finished));
                        com.aait.mazaqelkhlij.UI.activities.Provider.MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangeOrderStatusResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void ChangeOrder1(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeOrder(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Order_id,5).enqueue(new Callback<ChangeOrderStatusResponse>() {
            @Override
            public void onResponse(Call<ChangeOrderStatusResponse> call, Response<ChangeOrderStatusResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.delegate_recive_order));
                        com.aait.mazaqelkhlij.UI.activities.Provider.MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangeOrderStatusResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
