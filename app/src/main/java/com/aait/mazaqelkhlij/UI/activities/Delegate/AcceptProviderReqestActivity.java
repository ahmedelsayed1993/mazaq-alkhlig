package com.aait.mazaqelkhlij.UI.activities.Delegate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.Provider.AddDelegateActivity;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.BaseResponse;
import com.aait.mazaqelkhlij.models.UserModel;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AcceptProviderReqestActivity extends ParentActivity {
    UserModel userModel;
    @BindView(R.id.civ_delegate)
    CircleImageView civ_delegate;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.area)
    TextView area;
    @BindView(R.id.btn_accept)
    Button btn_accept;
    @BindView(R.id.btn_cancel)
    Button btn_cancel;


    public static void startActivity(AppCompatActivity mAppCompatActivity, UserModel userModel) {
        Intent mIntent = new Intent(mAppCompatActivity, AcceptProviderReqestActivity.class);
        mIntent.putExtra("delegate",userModel);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    void getBundleData(){
        userModel = (UserModel) getIntent().getSerializableExtra("delegate");

    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.provider_requests));
        getBundleData();
        Glide.with(mContext).load(userModel.getAvatar()).asBitmap().placeholder(R.mipmap.splash).into(civ_delegate);
        name.setText(userModel.getName());
        phone.setText(userModel.getPhone());
        area.setText(userModel.getArea_name());
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_accept_provider_request;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.btn_accept)
    void onAcceptClick(){
        book(1);
    }
    @OnClick(R.id.btn_cancel)
    void onCancelClick(){
        book(2);
    }
    private void book(int status){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).book(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),userModel.getUser_id(),status).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
