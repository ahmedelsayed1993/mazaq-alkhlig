package com.aait.mazaqelkhlij.UI.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.Provider.CategoryChooseAdapter1;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.CategoriesModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryDialog extends Dialog {

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.lay_no_data)
    LinearLayout lay_no_data;

    @BindView(R.id.tv_title)
    TextView tv_title;


    Context mContext;


    String title;


    //ChooseCategoryListner mChooseCategoryListner;

    RecyclerView.LayoutManager layoutManager;

    CategoryChooseAdapter1 mCategoryChooseAdapter;
    OnItemClickListener onItemClickListener;

    ArrayList<CategoriesModel> mCategoryModels = new ArrayList<>();

    //    ArrayList<ListModel> userChooseCategries = new ArrayList<>();
//    ListModel cateoryModel;
    public CategoryDialog(Context mContext, OnItemClickListener onItemClickListener, ArrayList<CategoriesModel>mCategoryModels,
                          String title) {
        super(mContext);
        this.mContext = mContext;
        this.onItemClickListener = onItemClickListener;
        this.mCategoryModels = mCategoryModels;
        this.title = title;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom_layout_family);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(false);
        ButterKnife.bind(this);
        initializeComponents();
    }
    private void initializeComponents() {
        tv_title.setText(title);
        layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(layoutManager);
        mCategoryChooseAdapter = new CategoryChooseAdapter1(mContext, mCategoryModels);
        mCategoryChooseAdapter.setOnItemClickListener(onItemClickListener);
        rvRecycle.setAdapter(mCategoryChooseAdapter);

        if (mCategoryModels.size() == 0) {
            lay_no_data.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.iv_close)
    void onCloseClicked() {
        dismiss();
    }

//    @OnClick(R.id.tv_done)
//    void onConfirmClick() {
//        filterCorrectData();
//        mChooseCategoryListner.ChooseCategories(userChooseCategries);
//        dismiss();
//    }


//    void filterCorrectData() {
//        for (ListModel categoryModel : mCategoryModels) {
////            if (categoryModel.isCheckCategory()) {
////                userChooseCategries.add(categoryModel);
////            }
//        }
//    }
}

