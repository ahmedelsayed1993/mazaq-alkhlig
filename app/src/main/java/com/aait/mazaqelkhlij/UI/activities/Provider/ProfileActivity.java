package com.aait.mazaqelkhlij.UI.activities.Provider;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.ChangePasswordActivity;
import com.aait.mazaqelkhlij.UI.activities.MapDetectLocationActivity;
import com.aait.mazaqelkhlij.UI.views.ListDialog;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.PermissionUtils;
import com.aait.mazaqelkhlij.Uitls.ProgressRequestBody;
import com.aait.mazaqelkhlij.Uitls.ValidationUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.ListModel;
import com.aait.mazaqelkhlij.models.ListModelResponse;
import com.aait.mazaqelkhlij.models.LoginResponse;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.mazaqelkhlij.App.Constant.RequestPermission.REQUEST_IMAGES;

public class ProfileActivity extends ParentActivity implements OnItemClickListener,ProgressRequestBody.UploadCallbacks {


    @BindView(R.id.lay_splash)
    LinearLayout laySplash;

    @BindView(R.id.civ_profile_pic)
    CircleImageView civProfilePic;

    @BindView(R.id.til_name)
    TextInputLayout tilName;

    @BindView(R.id.et_name)
    TextInputEditText etName;

    @BindView(R.id.til_email)
    TextInputLayout tilEmail;

    @BindView(R.id.et_email)
    TextInputEditText etEmail;

    @BindView(R.id.til_phone)
    TextInputLayout tilPhone;

    @BindView(R.id.et_phone)
    TextInputEditText etPhone;

    @BindView(R.id.til_location)
    TextInputLayout tilLocation;

    @BindView(R.id.et_location)
    TextInputEditText etLocation;

    @BindView(R.id.til_password)
    TextInputLayout tilPassword;

    @BindView(R.id.et_password)
    TextInputEditText etPassword;

    @BindView(R.id.til_id)
    TextInputLayout tilId;

    @BindView(R.id.et_id)
    TextInputEditText etId;

    @BindView(R.id.til_nationality)
    TextInputLayout tilNationality;

    @BindView(R.id.et_nationality)
    TextInputEditText etNationality;

    @BindView(R.id.til_location_address)
    TextInputLayout tilLocationAddress;

    @BindView(R.id.et_location_address)
    TextInputEditText etLocationAddress;
    @BindView(R.id.til_area)
    TextInputLayout tilCity;
    @BindView(R.id.et_area)
    TextInputEditText etCity;

    ArrayList<ListModel> mListModels;

    String mAdresse, mLang, mLat = null;

    ListDialog mListDialog;

    ListModel cityModel;

    ListModel NationalityModel;
    ListModel areaModel;


    // select image from callery
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;

    int NationalityOrCity = 0;
    boolean isCVlicable =false;


    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, ProfileActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.profile));
        etLocation.setClickable(false);
        setUserData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_profile_family;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        mListDialog.dismiss();
        if (NationalityOrCity == 0) {
            areaModel = mListModels.get(position);
            etCity.setText(areaModel.getName());
            isCVlicable =true;
            etLocation.setClickable(true);
            CommonUtil.PrintLogE("Area id : " + areaModel.getId());
        } else if (NationalityOrCity == 1) {
            NationalityModel = mListModels.get(position);
            etNationality.setText(NationalityModel.getName());
            CommonUtil.PrintLogE("Nationality id : " + NationalityModel.getId());
        } else if (NationalityOrCity == 2){
            cityModel = mListModels.get(position);
            etLocation.setText(cityModel.getName());
            CommonUtil.PrintLogE("City_id : "+cityModel.getId());
        }

    }



    @OnClick(R.id.btn_save_changes)
    void onChangeClick(){
        if (editValidation()){
            if (etPassword.getText().toString().equals("") && ImageBasePath==null){
                update(null);
            }else if (!(etPassword.getText().toString().equals("")) && ImageBasePath==null){
                update(etPassword.getText().toString());
            }else if (etPassword.getText().toString().equals("") && ImageBasePath!=null){
                updatewithImage(null,ImageBasePath);
            }else if (!(etPassword.getText().toString().equals(""))&& ImageBasePath != null){
                updatewithImage(etPassword.getText().toString(),ImageBasePath);
            }
        }
    }

    @OnClick(R.id.et_area)
    void onAreaClick() {
        NationalityOrCity = 0;
        getAreas();
    }

    @OnClick(R.id.et_nationality)
    void onNationalityClick() {
        NationalityOrCity = 1;
        getNations();
    }
    @OnClick(R.id.et_location)
    void onCityClick(){
        if (isCVlicable) {
            NationalityOrCity = 2;
            getCities(areaModel.getId() + "");
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.choose_city));
        }

    }

    @OnClick(R.id.et_location_address)
    void onLocationAdreesClick() {
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity) mContext);
    }

    @OnClick(R.id.civ_profile_pic)
    void onImageClick() {
        getPickImageWithPermission();

    }

    void setUserData() {

        areaModel = new ListModel(Integer.parseInt(mSharedPrefManager.getUserData().getArea_id()),mSharedPrefManager.getUserData().getArea_name());
        cityModel = new ListModel(Integer.parseInt(mSharedPrefManager.getUserData().getCity_id()),mSharedPrefManager.getUserData().getCity_name());
        NationalityModel = new ListModel(Integer.parseInt(mSharedPrefManager.getUserData().getNationality_id()),mSharedPrefManager.getUserData().getNationality_name());
        mLat = mSharedPrefManager.getUserData().getLat();
        mLang = mSharedPrefManager.getUserData().getLng();

        etName.setText(mSharedPrefManager.getUserData().getName());
        etEmail.setText(mSharedPrefManager.getUserData().getEmail());
        etPhone.setText(mSharedPrefManager.getUserData().getPhone());
        etCity.setText(mSharedPrefManager.getUserData().getArea_name());
        etLocation.setText(mSharedPrefManager.getUserData().getCity_name());
        etId.setText(mSharedPrefManager.getUserData().getCivil_number());
        etNationality.setText(mSharedPrefManager.getUserData().getNationality_name());
        Glide.with(mContext).load(mSharedPrefManager.getUserData().getAvatar()).into(civProfilePic);

    }

    boolean editValidation() {
        if (!ValidationUtils.checkError(etName, tilName, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etPhone, tilPhone, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etId, tilId, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etEmail, tilEmail, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.validateEmail(etEmail, tilEmail)) {
            return false;
        } else if (!ValidationUtils.checkError(etNationality, tilNationality, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etLocation, tilLocation, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }


    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        civProfilePic.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }


    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    private void getCities(String id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCities(mLanguagePrefManager.getAppLanguage(),id).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().issucessfull()){
                        mListModels = response.body().getData();
                        mListDialog = new ListDialog(mContext, com.aait.mazaqelkhlij.UI.activities.Provider.ProfileActivity.this
                                ,mListModels,getString(R.string.city));
                        mListDialog.show();
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {

                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }

    private void getAreas(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAreas(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().issucessfull()){
                        mListModels = response.body().getData();
                        mListDialog = new ListDialog(mContext, com.aait.mazaqelkhlij.UI.activities.Provider.ProfileActivity.this,
                                mListModels,getString(R.string.area));
                        mListDialog.show();
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }
    private void getNations(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getNations(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mListModels = response.body().getData();
                        mListDialog = new ListDialog(mContext, com.aait.mazaqelkhlij.UI.activities.Provider.ProfileActivity.this,mListModels,getString(R.string.nationality));
                        mListDialog.show();
                    }else {
                        CommonUtil.makeToast(mContext,getString(R.string.please_wait));
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void update(String password){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).update(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id()+"",etName.getText().toString(),password,etEmail.getText().toString(),etPhone.getText().toString(),cityModel.getId()+"",mLat,mLang,NationalityModel.getId()+"",etId.getText().toString()).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.profile_update_succ));
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void updatewithImage(String password,String PathfromImage){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(PathfromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ProfileActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).updateWithImage(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id()+"",etName.getText().toString(),password,etEmail.getText().toString(),etPhone.getText().toString(),cityModel.getId()+"",mLat,mLang,NationalityModel.getId()+"",etId.getText().toString(),filePart).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.profile_update_succ));
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse);
                    etLocationAddress.setText(mAdresse);
                }
            }
        }
    }
}
