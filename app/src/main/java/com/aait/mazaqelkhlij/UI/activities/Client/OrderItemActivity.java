package com.aait.mazaqelkhlij.UI.activities.Client;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.RecyclerPoupupAds;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.BaseResponse;
import com.aait.mazaqelkhlij.models.ImagesModel;
import com.aait.mazaqelkhlij.models.ProductDetailsResponse;
import com.aait.mazaqelkhlij.models.ProductModel;
import com.bumptech.glide.Glide;
import com.pixelcan.inkpageindicator.InkPageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderItemActivity extends ParentActivity {
    @BindView(R.id.ads_lay)
    RelativeLayout images;
    @BindView(R.id.viewpager1)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    InkPageIndicator indicator;

    @BindView(R.id.iv_image)
    ImageView ivImage;
    @BindView(R.id.iv_image1)
    ImageView ivImage1;
    @BindView(R.id.iv_image2)
    ImageView ivImage2;
    @BindView(R.id.btn_share)
    Button btnShare;
    @BindView(R.id.btn_rate)
    Button btnRate;
    @BindView(R.id.tv_food_name)
    TextView tvFoodName;

    @BindView(R.id.tv_normal_price)
    TextView tvNormalPrice;

    @BindView(R.id.tv_discount_price)
    TextView tvDiscountPrice;

    @BindView(R.id.tv_description)
    TextView tvDescription;


    @BindView(R.id.tv_offer_image)
    ImageView tv_offer_image;


    @BindView(R.id.iv_minus)
    ImageView ivMinus;

    @BindView(R.id.iv_add)
    ImageView ivAdd;

    @BindView(R.id.ed_order_number)
    EditText edOrderNumber;

    @BindView(R.id.tv_familey_name)
    TextView tvFamilyName;



    @BindView(R.id.rv_Recycle)
    RecyclerView rvRecycle;

    ArrayList<ImagesModel> imagesModel;

   ProductModel mFoodModel;

    int number;
    RecyclerPoupupAds recyclerPoupupAds;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    public static void startActivity(AppCompatActivity mAppCompatActivity, ProductModel productModel) {
        Intent mIntent = new Intent(mAppCompatActivity, OrderItemActivity.class);
        mIntent.putExtra(Constant.BundleData.FOOD_MODEL, productModel);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData() {
        mFoodModel = (ProductModel) getIntent().getSerializableExtra(Constant.BundleData.FOOD_MODEL);


    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.product_name));
        getBundleData();
        if (mSharedPrefManager.getLoginStatus()) {
            showProduct(mSharedPrefManager.getUserData().getUser_id());
        }
        else {
            showProduct(0);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_item_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }


    @OnClick(R.id.iv_minus)
    void onMinusClick() {
        if (Integer.parseInt(edOrderNumber.getText().toString()) == 1) {

        } else {
            edOrderNumber.setText((Integer.parseInt(edOrderNumber.getText().toString()) - 1) + "");
        }
    }
    @OnClick(R.id.btn_share)
    void onBtnShareClick(){
        CommonUtil.ShareProductName(mContext,mFoodModel.getProduct_image()+"\n"+getString(R.string.product_name)+": "+mFoodModel.getProduct_name()+"\n"+getString(R.string.product_description)+": "+mFoodModel.getProduct_disc());
    }

    @OnClick(R.id.btn_rate)
    void onBtnRate(){
        if (mSharedPrefManager.getLoginStatus()) {
            RateActivity.startActivity((AppCompatActivity) mContext, mFoodModel.getShop_id());
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }

    @OnClick(R.id.iv_add)
    void onAddClick() {
        edOrderNumber.setText((Integer.parseInt(edOrderNumber.getText().toString()) + 1) + "");
    }
    @OnClick(R.id.btn_add_to_card)
    void onBtnAddToCardClick() {
        if (mSharedPrefManager.getLoginStatus()) {
            addToCart();
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
    }
    @OnClick(R.id.tv_familey_name)
    void onFamilyNameClick(){
        FamilyProductsActivity.startActivity((AppCompatActivity)mContext,mFoodModel.getShop_id());
    }

    void setData(ProductModel productModel) {
        Glide.with(mContext).load(productModel.getProduct_image()).asBitmap().placeholder(R.mipmap.splash)
                .into(ivImage);
        imagesModel = productModel.getImages();
        recyclerPoupupAds = new RecyclerPoupupAds(OrderItemActivity.this,imagesModel);
        viewPager.setAdapter(recyclerPoupupAds);

        indicator.setViewPager(viewPager);

        NUM_PAGES = recyclerPoupupAds.getCount();
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == NUM_PAGES){
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++,true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);
            }
        }, 2500, 2500);

        Log.e("family",productModel.getProduct_disc());
        tvFamilyName.setText(productModel.getShop_name());
        tvFoodName.setText(productModel.getProduct_name());
        tvNormalPrice
                .setText(productModel.getProduct_price() + " " + getResources().getString(R.string.SAR));
        if (productModel.isProduct_have_offer()==true) {
            CommonUtil.setStrokInText(tvNormalPrice);
            tvDiscountPrice
                    .setText(productModel.getProduct_offer() + " " + getResources()
                            .getString(R.string.SAR));
        } else {
            tvDiscountPrice.setVisibility(View.GONE);
            tv_offer_image.setVisibility(View.GONE);
        }

        tvDescription.setText(productModel.getProduct_disc());

    }
    private void showProduct(int user_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).showproduct(user_id,mFoodModel.getProduct_id()).enqueue(new Callback<ProductDetailsResponse>() {
            @Override
            public void onResponse(Call<ProductDetailsResponse> call, Response<ProductDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mFoodModel = response.body().getData();
                        setData(mFoodModel);
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void addToCart(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).addtocart(mSharedPrefManager.getUserData().getUser_id(),mFoodModel.getProduct_id(),Integer.parseInt(edOrderNumber.getText().toString())).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
