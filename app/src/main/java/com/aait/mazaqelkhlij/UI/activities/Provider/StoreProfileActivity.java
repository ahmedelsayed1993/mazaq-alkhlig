package com.aait.mazaqelkhlij.UI.activities.Provider;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.fragments.Provider.ChooseActivitiesDialogFragment;
import com.aait.mazaqelkhlij.UI.views.CategoriesDialog;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.ValidationUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.ChooseCategoryListner;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.CatModel;
import com.aait.mazaqelkhlij.models.CategoriesModel;
import com.aait.mazaqelkhlij.models.CategoriesResponse;
import com.aait.mazaqelkhlij.models.CategoryModel;
import com.aait.mazaqelkhlij.models.ListModel;
import com.aait.mazaqelkhlij.models.ListModelResponse;
import com.aait.mazaqelkhlij.models.LoginResponse;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreProfileActivity extends ParentActivity implements OnItemClickListener,ChooseCategoryListner {

    @BindView(R.id.til_store_name)
    TextInputLayout tilStoreName;

    @BindView(R.id.et_store_name)
    TextInputEditText etStoreName;

    @BindView(R.id.til_activity)
    TextInputLayout tilActivity;

    @BindView(R.id.et_activity)
    TextInputEditText etActivity;
    @BindView(R.id.add_modob)
    Button add_mandob;










    ListModel mCategories;
    ArrayList<ListModel> listModels =new ArrayList<>();
    CategoriesDialog categoriesDialog;

    boolean isHasDelivery = false;

    String CategoriesList;

    boolean editCat = false;
    ArrayList<ArrayList<ListModel>> categories = new ArrayList<>();
    List<String > names = new ArrayList<>();
    List<Integer > ids = new ArrayList<>();
    String id;

    ArrayList<CategoryModel> category=new ArrayList<>();
    CategoryModel model;
    ArrayList<ListModel> categoryModel = new ArrayList<>();
    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, StoreProfileActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getResources().getString(R.string.store_data));
        setUserData();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_store_profile_family;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @OnClick(R.id.et_activity)
    void onChooseActivitiesClick() {
        editCat = true;
        getCategories();
       // getCategories(mLanguagePrefManager.getAppLanguage());
    }
    @OnClick(R.id.add_modob)
    void onAddMondobClick(){
        DelegatesActivity.startActivity((AppCompatActivity)mContext);
    }



    boolean registerValidation() {
        if (!ValidationUtils.checkError(etStoreName, tilStoreName, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etActivity, tilActivity, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }
    @OnClick(R.id.btn_save_changes)
    void onUpdateClickClick() {
        if (registerValidation()){
            update();
        }
    }





    private void setActivitiesData(ArrayList<CategoriesModel> categoryName) {
        etActivity.setText("");

        for (int i=0;i<categoryName.size();i++) {

            if (i != categoryName.size()) {
                etActivity.setText(etActivity.getText().toString() + categoryName.get(i).getCategory_name() + " , ");
            } else {
                etActivity.setText(etActivity.getText().toString() + categoryName.get(i).getCategory_name() + " ");
            }
        }

//        CategoriesList = new Gson().toJson(categoryName);
//        CommonUtil.PrintLogE("Categories Data : " + CategoriesList);
    }

    private void setUserData() {
        CommonUtil.onPrintLog(mSharedPrefManager.getUserData().getShop().getShop_categories().get(0));
        setActivitiesData(mSharedPrefManager.getUserData().getShop().getShop_categories());
        etStoreName.setText(mSharedPrefManager.getUserData().getShop().getShop_name());


    }

    @Override
    public void onItemClick(View view, int position) {
//        mCategories =listModels.get(position);
//        categoryModel.add(mCategories);
//        categories.add(categoryModel);
//
//        for (int i = 0;i<categories.size();i++){
//            model =new CategoryModel (categories.get(i));
//            category.add(model);
//
//            for (int j =0;j<category.size();j++){
//
//                for (int w = 0;w<categories.size();w++) {
//
//                }
//
//            }
//
//
//        }
//        Log.e("name",category.get(category.size()-1).getListModels().get(categories.size()-1).getName()+",");
//        names.add(category.get(category.size()-1).getListModels().get(categories.size()-1).getName()+",");
//        ids.add(category.get(category.size()-1).getListModels().get(categories.size()-1).getId());
//        Log.e("nnn",names+"");
//        Log.e("iii",ids+"");
//        //String ID= ids.toString().replace("[","").replace("]","");
//        id = ids.toString().replace("[","").replace("]","");
//        StringBuilder builder = new StringBuilder();
//        for (String value : names) {
//            builder.append(value);
//        }
//        String text = builder.toString();
//        etActivity.setText(text);
//
//
//        Log.e("idss",id);


    }
    private void getCategories(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCategory(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){

                    if (response.body().getData().size()!=0){
                        showCategories(getSupportFragmentManager(),new ArrayList(response.body().getData()));

                    }


                    else {

                    }
                }
            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                Log.e("nnn",new Gson().toJson(t));
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    private void update(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).updateShop(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),etStoreName.getText().toString(),CategoriesList).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if(response.body().getStatus()==1){
                        mSharedPrefManager.setUserData(response.body().getData());
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        setUserData();
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    void showCategories(FragmentManager mFragmentManager, ArrayList<CatModel> categoryModels) {
        CommonUtil.PrintLogE("Categories size : " + categoryModels.size());
        ChooseActivitiesDialogFragment chooseActivitiesDialogFragment = ChooseActivitiesDialogFragment
                .newInstance(categoryModels);
        chooseActivitiesDialogFragment.show(mFragmentManager, "Choose Categories");
        chooseActivitiesDialogFragment.setListner(this);
    }

    void setCategoryName(ArrayList<CatModel> categoryName) {
        etActivity.setText("");
        int i = 0;
        String id = "";
        for (CatModel categoryModel : categoryName) {
            i++;
            if (categoryModel.isCheckCategory()) {
                if (i != categoryName.size()) {
                    id = id+categoryModel.getId()+",";
                    etActivity.setText(etActivity.getText().toString() + categoryModel.getName() + " , ");
                } else {
                    id = id + categoryModel.getId() + "";
                    etActivity.setText(etActivity.getText().toString() + categoryModel.getName() + " ");
                }
            }
        }

        CategoriesList = id;
        CommonUtil.PrintLogE("Categories" + CategoriesList);
    }

    @Override
    public void ChooseCategories(ArrayList<CatModel> categoryModels) {
        setCategoryName(categoryModels);
    }
}
