package com.aait.mazaqelkhlij.UI.adapters.Delegate;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentRecyclerAdapter;
import com.aait.mazaqelkhlij.base.ParentRecyclerViewHolder;
import com.aait.mazaqelkhlij.models.OrderModel;
import com.bumptech.glide.Glide;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProcessedOrderAdapter extends ParentRecyclerAdapter<OrderModel> {

    private String errorMsg;

    public ProcessedOrderAdapter(final Context context, final List<OrderModel> data) {
        super(context, data);
    }


    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        switch (viewType) {
            case Constant.InfinitScroll.ITEM:
                View viewItem = inflater.inflate(R.layout.recycle_order_row_delivery, parent, false);
                viewHolder = new ProcessedOrderViewHolder(viewItem);
                break;
            case Constant.InfinitScroll.LOADING:
                View viewLoading = inflater.inflate(R.layout.recycle_item_progress_delivery, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case Constant.InfinitScroll.ITEM:
                ProcessedOrderViewHolder processedOrderViewHolder = (ProcessedOrderViewHolder) holder;
                OrderModel orderModel = data.get(position);

                Glide.with(mcontext).load(orderModel.getOrder_user_avatar()).asBitmap()
                        .into(processedOrderViewHolder.ivClientImage);
                Glide.with(mcontext).load(orderModel.getOrder_provider_avatar()).asBitmap()
                        .into(processedOrderViewHolder.ivFamilyImage);
                processedOrderViewHolder.tvOrderTime
                        .setText(orderModel.getOrder_created_at() + "");
                processedOrderViewHolder.tvOrderNumber
                        .setText(orderModel.getOrder_id() + "");

                processedOrderViewHolder.tvClientName.setText(orderModel.getOrder_user_name());
                processedOrderViewHolder.tvFamilyName.setText(orderModel.getOrder_provider_name());

                processedOrderViewHolder.tvClientCity.setText(orderModel.getOrder_user_area());
                processedOrderViewHolder.tvFamilyCity.setText(orderModel.getOrder_provider_area());

                processedOrderViewHolder.tv_total_coast
                        .setText(orderModel.getTotal_price() + " " + mcontext.getString(R.string.SAR));
                break;

            case Constant.InfinitScroll.LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    mcontext.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == data.size() - 1 && isLoadingAdded)
                ? Constant.InfinitScroll.LOADING
                : Constant.InfinitScroll.ITEM;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    /**
     * Main list's content ViewHolder
     */

    protected class ProcessedOrderViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_order_time)
        TextView tvOrderTime;

        @BindView(R.id.tv_total_coast)
        TextView tv_total_coast;

        @BindView(R.id.iv_clock)
        ImageView ivClock;

        @BindView(R.id.tv_order_number)
        TextView tvOrderNumber;

        @BindView(R.id.iv_client_image)
        CircleImageView ivClientImage;

        @BindView(R.id.tv_client_name)
        TextView tvClientName;

        @BindView(R.id.tv_client_city)
        TextView tvClientCity;

        @BindView(R.id.iv_family_image)
        CircleImageView ivFamilyImage;

        @BindView(R.id.tv_family_name)
        TextView tvFamilyName;

        @BindView(R.id.tv_family_city)
        TextView tvFamilyCity;

        public ProcessedOrderViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    itemClickListener.onItemClick(view, getPosition());
                }
            });
        }
    }


    protected class LoadingVH extends ParentRecyclerViewHolder {


        @BindView(R.id.progress_wheel)
        ProgressWheel mProgressBar;

        @BindView(R.id.loadmore_retry)
        ImageButton mRetryBtn;

        @BindView(R.id.loadmore_errortxt)
        TextView mErrorTxt;

        @BindView(R.id.loadmore_errorlayout)
        LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            ProgressWheel mProgressBar = (ProgressWheel) findViewById(R.id.progress_wheel);
        }

        @OnClick(R.id.loadmore_retry)
        void onLoadMoreRetryClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }

        @OnClick(R.id.loadmore_errorlayout)
        void onLoadMoreErrorLayoutClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }
    }


    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(data.size() - 1);
        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }


}

