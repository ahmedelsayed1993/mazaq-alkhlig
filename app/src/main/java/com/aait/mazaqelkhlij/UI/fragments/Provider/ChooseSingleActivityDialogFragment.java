package com.aait.mazaqelkhlij.UI.fragments.Provider;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Pereferences.SharedPrefManager;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.listeners.ChooseCategoryListner;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.CatModel;
import com.aait.mazaqelkhlij.models.CategoryModel;
import com.aait.mazaqelkhlij.models.ListModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseSingleActivityDialogFragment extends DialogFragment implements OnItemClickListener {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_cancel)
    TextView tvCancel;

    @BindView(R.id.tv_done)
    TextView tvDone;

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    ChooseCategoryListner mChooseCategoryListner;

    RecyclerView.LayoutManager layoutManager;

    //CategoryChooseAdapter mCategoryChooseAdapter;

    ArrayList<CatModel> mCategoryModels = new ArrayList<>();

    ArrayList<CatModel> userChooseCategries = new ArrayList<>();

    SharedPrefManager mSharedPrefManager ;
    public static ChooseSingleActivityDialogFragment newInstance() {
        Bundle bundle = new Bundle();
        ChooseSingleActivityDialogFragment fragment = new ChooseSingleActivityDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public ChooseSingleActivityDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_chhose_categories_family, container);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSharedPrefManager = new SharedPrefManager(getActivity());
       // mCategoryModels = mSharedPrefManager.getUserData().getCategories();
        layoutManager = new LinearLayoutManager(getActivity());
        rvRecycle.setLayoutManager(layoutManager);
//        mCategoryChooseAdapter = new CategoryChooseAdapter(getActivity(), mCategoryModels,
//                R.layout.recycle_category_choose_row_family);
//        mCategoryChooseAdapter.setOnItemClickListener(this);
//        rvRecycle.setAdapter(mCategoryChooseAdapter);

    }

    @Override
    public void onResume() {
        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);
        getDialog().getWindow().setWindowAnimations(R.style.DialogAnimation_2);
        // Call super onResume after sizing
        super.onResume();
    }

    public void setListner(ChooseCategoryListner chooseCategoryListner) {
        this.mChooseCategoryListner = chooseCategoryListner;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.tv_done)
    void onConfirmClick() {
        filterCorrectData();
        mChooseCategoryListner.ChooseCategories(userChooseCategries);
        dismiss();
    }

    @OnClick(R.id.tv_cancel)
    void onCancelClick() {
        dismiss();
    }


    @Override
    public void onItemClick(final View view, final int position) {

//        if (mCategoryModels.get(position).isCheckCategory()) {
//            for (int i = 0; i < mCategoryModels.size(); i++) {
//                mCategoryModels.get(i).setCheckCategory(false);
//            }
//            mCategoryChooseAdapter.notifyDataSetChanged();
//        } else {
//            for (int i = 0; i < mCategoryModels.size(); i++) {
//                mCategoryModels.get(i).setCheckCategory(false);
//            }
//            mCategoryModels.get(position).setCheckCategory(true);
//            mCategoryChooseAdapter.notifyDataSetChanged();
//        }
    }


    void filterCorrectData() {
        for (CatModel categoryModel : mCategoryModels) {
//            if (categoryModel.isCheckCategory()) {
//                userChooseCategries.add(categoryModel);
//            }
        }
    }
}


