package com.aait.mazaqelkhlij.UI.activities;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.ValidationUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.ConfirmCodeModel;
import com.aait.mazaqelkhlij.models.ForgotPasswordResponse;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends ParentActivity {

    @BindView(R.id.lay_splash)
    LinearLayout laySplash;

    @BindView(R.id.til_mobile)
    TextInputLayout tilMobile;

    @BindView(R.id.et_mobile)
    TextInputEditText etMobile;

String type;
    public static void startActivity(AppCompatActivity mAppCompatActivity,String type) {
        Intent mIntent = new Intent(mAppCompatActivity, ForgetPasswordActivity.class);
        mIntent.putExtra("type",type);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    void getBundleData(){
        type = (String)getIntent().getSerializableExtra("type");
    }
    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_forget_password_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.btn_send)
    void onBtnSendClick() {
        if (forgetPasswordValidation()) {
            forgetPassword();
        }
    }

    boolean forgetPasswordValidation() {
         if (!ValidationUtils.checkError(etMobile, tilMobile, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.validateEmail(etMobile, tilMobile)) {
            return false;
        }
        return true;
    }
    private void forgetPassword(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).forgetPass(etMobile.getText().toString()).enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        ConfirmPassActivity.startActivityNewPass((AppCompatActivity)mContext,response.body().getData().getUser_id()+"",type);
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }
}
