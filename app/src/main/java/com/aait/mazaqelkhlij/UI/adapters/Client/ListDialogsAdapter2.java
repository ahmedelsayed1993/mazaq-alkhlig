package com.aait.mazaqelkhlij.UI.adapters.Client;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentRecyclerAdapter;
import com.aait.mazaqelkhlij.base.ParentRecyclerViewHolder;
import com.aait.mazaqelkhlij.models.ShopsModel;

import java.util.List;

import butterknife.BindView;

public class ListDialogsAdapter2 extends ParentRecyclerAdapter<ShopsModel> {

    public ListDialogsAdapter2(final Context context, final List<ShopsModel> data) {
        super(context, data);
    }
    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        View viewItem = inflater.inflate(R.layout.recycler_dialog_list_row_client2, parent, false);
        viewHolder = new ListDialogsAdapter2.ListAdapter(viewItem);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        ListDialogsAdapter2.ListAdapter listAdapter = (ListDialogsAdapter2.ListAdapter) holder;
        ShopsModel listModel = data.get(position);
        listAdapter.tv_row_title2.setText(listModel.getShop_name());
        if (position % 2 == 0) {
            listAdapter.tv_row_title2.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorRowSecondary));
        } else {
            listAdapter.tv_row_title2.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorRowPrimary));

        }

        listAdapter.tv_row_title2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Log.e("postion",position+"");
                itemClickListener.onItemClick(view, position);
            }
        });
    }


    protected class ListAdapter extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_row_title2)
        TextView tv_row_title2;

        public ListAdapter(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }
}
