package com.aait.mazaqelkhlij.UI.activities.Provider;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.Client.OrderItemActivity;
import com.aait.mazaqelkhlij.UI.adapters.RecyclerPoupupAds;
import com.aait.mazaqelkhlij.UI.fragments.Provider.DiscountProductDialogFragment;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.DialogUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.DiscountListner;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.BaseResponse;
import com.aait.mazaqelkhlij.models.ImagesModel;
import com.aait.mazaqelkhlij.models.ProductDetailsResponse;
import com.aait.mazaqelkhlij.models.ProductModel;
import com.aait.mazaqelkhlij.models.ProductResponse;
import com.bumptech.glide.Glide;
import com.pixelcan.inkpageindicator.InkPageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailsActivity extends ParentActivity implements OnItemClickListener,DiscountListner {

    @BindView(R.id.iv_image)
    ImageView ivImage;
    @BindView(R.id.ads_lay)
    RelativeLayout images;
    @BindView(R.id.viewpager1)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    InkPageIndicator indicator;

    @BindView(R.id.iv_image1)
    ImageView ivImage1;
    @BindView(R.id.iv_image2)
    ImageView ivImage2;
    @BindView(R.id.iv_has_offer)
    ImageView iv_has_offer;

    @BindView(R.id.tv_food_name)
    TextView tvFoodName;

    @BindView(R.id.tv_normal_price)
    TextView tvNormalPrice;

    @BindView(R.id.tv_discount_price)
    TextView tvDiscountPrice;

    @BindView(R.id.tv_description)
    TextView tvDescription;


    @BindView(R.id.rv_Recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.btn_share)
    Button btnShare;

    LinearLayoutManager linearLayoutManager;


    ArrayList<ImagesModel> imagesModel;

    int mFoodModel;
    ProductModel productModel;
   // ProductModel mFoodModel;
//    ImagesModel mImagesModel;
    RecyclerPoupupAds recyclerPoupupAds;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    public static void startActivity(AppCompatActivity mAppCompatActivity, int productModel) {
        Intent mIntent = new Intent(mAppCompatActivity, ProductDetailsActivity.class);
        mIntent.putExtra(Constant.BundleData.FOOD_MODEL, productModel);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    @Override
    public void discountOf(final String discunt) {
       AddOffer(discunt);
    }

    void getBundleData() {
        mFoodModel =  getIntent().getIntExtra(Constant.BundleData.FOOD_MODEL,0);
       // setData(mFoodModel);

    }

    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.product_name));
        getBundleData();
       showProduct();

        createOptionsMenu(R.menu.product_menu_family);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_item_family;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }


    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.product_edit:
                UpdateProductActivity.startActivity((AppCompatActivity)mContext,productModel);
                break;
            case R.id.product_discount:

                showDiscountProduct(getSupportFragmentManager());
                return true;
            case R.id.product_delete:
                DialogUtil.showAlertDialog(mContext, getString(R.string.delete_the_product),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialogInterface, final int i) {
                                Delete();
                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                return true;
            default:
                onBackPressed();
                break;
        }
        return true;
    }

    void showDiscountProduct(FragmentManager mFragmentManager) {
        DiscountProductDialogFragment batteryDetailsDialogFragment = DiscountProductDialogFragment
                .newInstance(productModel);
        batteryDetailsDialogFragment.show(mFragmentManager, "EditProduct");
        batteryDetailsDialogFragment.setListner(this);
    }

    @OnClick(R.id.btn_share)
    void onBtnShareClick(){

        CommonUtil.ShareProductName(mContext,productModel.getProduct_image()+"\n"+getString(R.string.product_name)+": "+productModel.getProduct_name()+"\n"+getString(R.string.product_description)+": "+productModel.getProduct_disc());
    }

    void setData(ProductModel productModel) {
        Glide.with(mContext).load(productModel.getProduct_image()).asBitmap().placeholder(R.mipmap.splash)
                .into(ivImage);
        imagesModel = productModel.getImages();
        recyclerPoupupAds = new RecyclerPoupupAds(ProductDetailsActivity.this,imagesModel);
        viewPager.setAdapter(recyclerPoupupAds);

        indicator.setViewPager(viewPager);

        NUM_PAGES = recyclerPoupupAds.getCount();
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == NUM_PAGES){
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++,true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);
            }
        }, 2500, 2500);

        Log.e("family",productModel.getProduct_disc());
        //tvFamilyName.setText(productModel.getShop_name());
        tvFoodName.setText(productModel.getProduct_name());
        tvNormalPrice
                .setText(productModel.getProduct_price() + " " + getResources().getString(R.string.SAR));
        if (productModel.isProduct_have_offer()==true) {
            CommonUtil.setStrokInText(tvNormalPrice);
            tvDiscountPrice
                    .setText(productModel.getProduct_offer() + " " + getResources()
                            .getString(R.string.SAR));
        } else {
            tvDiscountPrice.setVisibility(View.GONE);
            iv_has_offer.setVisibility(View.GONE);
        }

        tvDescription.setText(productModel.getProduct_disc());


    }

    private void showProduct() {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).showproduct(mSharedPrefManager.getUserData().getUser_id(), mFoodModel).enqueue(new Callback<ProductDetailsResponse>() {
            @Override
            public void onResponse(Call<ProductDetailsResponse> call, Response<ProductDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 1) {
                        productModel = response.body().getData();
                        setData(productModel);
                    } else {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void AddOffer(String discount){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).addOffer(mSharedPrefManager.getUserData().getUser_id(),productModel.getProduct_id(),discount,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    private void Delete(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).deleteProduct(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),productModel.getProduct_id()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
