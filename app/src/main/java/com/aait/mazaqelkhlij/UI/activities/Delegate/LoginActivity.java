package com.aait.mazaqelkhlij.UI.activities.Delegate;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.ConfirmCodeActivity;
import com.aait.mazaqelkhlij.UI.activities.ForgetPasswordActivity;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.ValidationUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.fcm.MyFirebaseInstanceIDService;
import com.aait.mazaqelkhlij.models.LoginResponse;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends ParentActivity {

    @BindView(R.id.lay_splash)
    LinearLayout laySplash;

    @BindView(R.id.til_name)
    TextInputLayout tilName;

    @BindView(R.id.et_name)
    TextInputEditText etName;

    @BindView(R.id.til_password)
    TextInputLayout tilPassword;

    @BindView(R.id.et_password)
    TextInputEditText etPassword;

    @BindView(R.id.tv_forget_pass)
    TextView tvForgetPass;

    String type;
    public static void startActivity(AppCompatActivity mAppCompatActivity,String type) {
        Intent mIntent = new Intent(mAppCompatActivity, LoginActivity.class);
        mIntent.putExtra("type",type);
        mIntent.addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData(){
        type = (String) getIntent().getSerializableExtra("type");
    }
    @Override
    protected void initializeComponents() {
        getBundleData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login_delivery;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @OnClick(R.id.btn_login)
    void onBtnLoginClick() {
        if (loginValidation()) {

            login();
        }
    }

    @OnClick(R.id.btn_new_account)
    void onBtnNewAccountClick() {
        RegisterActivity.startActivity((AppCompatActivity) mContext,type);
    }

    @OnClick(R.id.tv_forget_pass)
    void onForgetPAssClick() {
        ForgetPasswordActivity.startActivity((AppCompatActivity) mContext,type);
    }


    boolean loginValidation() {
        if (!ValidationUtils.checkError(etName, tilName, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etPassword, tilName, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }

    private void login(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).Login(mLanguagePrefManager.getAppLanguage(),etName.getText().toString(),etPassword.getText().toString()
        ,"else",MyFirebaseInstanceIDService.getToken(mContext)).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if(response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mSharedPrefManager.setLoginStatus(true);
                        mSharedPrefManager.setUserData(response.body().getData());
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }else if (response.body().getStatus()==2){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        ConfirmCodeActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getUser_id()+"");
                    }
                    else if (response.body().getStatus()==0){
                        emptyForm();
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    void emptyForm() {
        etName.setText("");
        etPassword.setText("");
    }

}
