package com.aait.mazaqelkhlij.UI.activities.Client;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.Client.OffersAdapter;
import com.aait.mazaqelkhlij.UI.adapters.Provider.FamilyCategoriesAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.listeners.PaginationScrollListener;
import com.aait.mazaqelkhlij.models.CategoriesModel;
import com.aait.mazaqelkhlij.models.ProductModel;
import com.aait.mazaqelkhlij.models.ProductsResponse;
import com.aait.mazaqelkhlij.models.ProductsResponses;
import com.aait.mazaqelkhlij.models.ShowShopModel;
import com.aait.mazaqelkhlij.models.ShowShopResponse;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FamilyProductsActivity extends ParentActivity implements OnItemClickListener {

    @BindView(R.id.tv_family_name)
    TextView tvFamilyName;

    @BindView(R.id.tv_city)
    TextView tvCity;

    @BindView(R.id.rb_rating)
    AppCompatRatingBar rbRating;

    @BindView(R.id.rv_recycle_categores)
    RecyclerView rvRecycleCategores;

    @BindView(R.id.iv_next)
    ImageView ivNext;

    @BindView(R.id.civ_image)
    CircleImageView civ_image;

    @BindView(R.id.iv_background)
    ImageView Background;



    LinearLayoutManager linearLayoutManager;

    FamilyCategoriesAdapter mFamilyCategoriesAdapter;

    List<CategoriesModel> mCategoryModels = new ArrayList<>();


    LinearLayoutManager linearLayoutManagerVertical;

    OffersAdapter mOffersAdapter;

    List<ProductModel> mFamilyModels = new ArrayList<>();

    private static final int PAGE_START = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;



    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;
    int selectedPosition;
    // List<AdditionModel> mSpecialAdditiveModels = new ArrayList<>();
    ShowShopModel myShopModel;

    ProductModel mFoodModel;
    int CategoryID;
    int Shop_id;

    public static void startActivity(AppCompatActivity mAppCompatActivity, int Shop_id) {
        Intent mIntent = new Intent(mAppCompatActivity, FamilyProductsActivity.class);
        mIntent.putExtra(Constant.BundleData.FAMILY_ID, Shop_id);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData() {

        Shop_id =  getIntent().getIntExtra(Constant.BundleData.FAMILY_ID,0);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.prods));
        getBundleData();
        if (mSharedPrefManager.getLoginStatus()) {
            getShopData(mSharedPrefManager.getUserData().getUser_id());
            linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            rvRecycleCategores.setLayoutManager(linearLayoutManager);
            mFamilyCategoriesAdapter = new FamilyCategoriesAdapter(mContext, mCategoryModels,
                    R.layout.recycle_family_categories_row_family);
            mFamilyCategoriesAdapter.setOnItemClickListener(this);
            rvRecycleCategores.setAdapter(mFamilyCategoriesAdapter);
            if (mCategoryModels.size() != 0) {
                getProductsByCategory(mCategoryModels.get(0).getCategory_id(), mSharedPrefManager.getUserData().getUser_id());
            }
            linearLayoutManagerVertical = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            rvRecycle.setLayoutManager(linearLayoutManagerVertical);
            mOffersAdapter = new OffersAdapter(mContext, mFamilyModels);
            mOffersAdapter.setOnItemClickListener(this);
            rvRecycle.setAdapter(mOffersAdapter);

            swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (mCategoryModels.size() != 0) {
                        getProductsByCategory(mCategoryModels.get(0).getCategory_id(), mSharedPrefManager.getUserData().getUser_id());
                    }
                }
            });
        }else {
            getShopData(0);
            linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            rvRecycleCategores.setLayoutManager(linearLayoutManager);
            mFamilyCategoriesAdapter = new FamilyCategoriesAdapter(mContext, mCategoryModels,
                    R.layout.recycle_family_categories_row_family);
            mFamilyCategoriesAdapter.setOnItemClickListener(this);
            rvRecycleCategores.setAdapter(mFamilyCategoriesAdapter);
            if (mCategoryModels.size() != 0) {
                getProductsByCategory(mCategoryModels.get(0).getCategory_id(), 0);
            }
            linearLayoutManagerVertical = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            rvRecycle.setLayoutManager(linearLayoutManagerVertical);
            mOffersAdapter = new OffersAdapter(mContext, mFamilyModels);
            mOffersAdapter.setOnItemClickListener(this);
            rvRecycle.setAdapter(mOffersAdapter);

            swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (mCategoryModels.size() != 0) {
                        getProductsByCategory(mCategoryModels.get(0).getCategory_id(), 0);
                    }
                }
            });

        }


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_family_products;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }





    @Override
    public void onItemClick(View view, int position) {
        if (mSharedPrefManager.getLoginStatus()) {
            if (view.getId() == R.id.tv_categories) {
                // click on categories section
                CommonUtil.PrintLogE("categories click");
                selectedPosition = position;
                getProductsByCategory(mCategoryModels.get(position).getCategory_id(), mSharedPrefManager.getUserData().getUser_id());
            } else {
                OrderItemActivity.startActivity((AppCompatActivity)mContext,mFamilyModels.get(position));

            }
        }else {
            if (view.getId() == R.id.tv_categories) {
                // click on categories section
                CommonUtil.PrintLogE("categories click");
                selectedPosition = position;
                getProductsByCategory(mCategoryModels.get(position).getCategory_id(), 0);
            } else {
                CommonUtil.makeToast(mContext,getString(R.string.must_login));
            }
        }
    }

    void setData(){

    }
    private void getShopData(int user_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).showShop(mLanguagePrefManager.getAppLanguage(),user_id,Shop_id).
                enqueue(new Callback<ShowShopResponse>() {
                    @Override
                    public void onResponse(Call<ShowShopResponse> call, Response<ShowShopResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()){
                            if (response.body().getStatus()==1){
                                myShopModel = response.body().getData();
                                tvCity.setText(response.body().getData().getUser_city());
                                tvFamilyName.setText(response.body().getData().getShop_name());
                                rbRating.setRating(response.body().getData().getShop_rate());
                                Glide.with(mContext).load(response.body().getData().getUser_image()).asBitmap().placeholder(R.mipmap.logo).into(civ_image);
                                Glide.with(mContext).load(response.body().getData().getUser_image()).asBitmap().placeholder(R.mipmap.logo).into(Background);
                                mCategoryModels = response.body().getData().getUser_categories();
                                mFamilyCategoriesAdapter.updateAll(response.body().getData().getUser_categories());
                                mOffersAdapter.updateAll(response.body().getData().getProducts());
                            }
                            else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ShowShopResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext,t);
                        t.printStackTrace();
                        hideProgressDialog();

                    }
                });
    }
    private void getProductsByCategory(int Category_id,int user_id){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getProducts(user_id,Shop_id,Category_id,mLanguagePrefManager.getAppLanguage()).
                enqueue(new Callback<ProductsResponses>() {
                    @Override
                    public void onResponse(Call<ProductsResponses> call, Response<ProductsResponses> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getData().getProducts().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                mOffersAdapter.updateAll(response.body().getData().getProducts());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductsResponses> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }


}
