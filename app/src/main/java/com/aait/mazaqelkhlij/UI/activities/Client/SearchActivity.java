package com.aait.mazaqelkhlij.UI.activities.Client;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.MapDetectLocationActivity;
import com.aait.mazaqelkhlij.UI.adapters.Client.OffersAdapter;
import com.aait.mazaqelkhlij.UI.views.ListDialog;
import com.aait.mazaqelkhlij.UI.views.ListDialog2;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.listeners.PaginationScrollListener;
import com.aait.mazaqelkhlij.models.ListModel;
import com.aait.mazaqelkhlij.models.ListModelResponse;
import com.aait.mazaqelkhlij.models.ProductModel;
import com.aait.mazaqelkhlij.models.SearchResponse;
import com.aait.mazaqelkhlij.models.ShopsModel;
import com.aait.mazaqelkhlij.models.ShopsResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends ParentActivity implements OnItemClickListener {

    @BindView(R.id.ed_categories)
    EditText edCategories;

    @BindView(R.id.ed_city)
    EditText edCity;

    @BindView(R.id.ed_area)
    EditText edArea;

    @BindView(R.id.ed_family)
    EditText edFamily;
    @BindView(R.id.ed_prices)
    EditText edPrices;

    @BindView(R.id.iv_location)
    ImageView ivLocation;
    boolean cityIsChecked = false;

    ArrayList<ListModel> mListModels;

    ArrayList<ShopsModel> mFamiliesModel;
    ListDialog mListDialog;
    ListDialog2 mListDialog2;

    ListModel categoresModel = null;

    ListModel cityModel = null;

    ListModel areaModel = null;

    ListModel priceModel = null;

    ShopsModel familyModel = null;

    int listDialogType;


    // recycle items
    LinearLayoutManager linearLayoutManager;

    OffersAdapter mOffersAdapter;

    List<ProductModel> mFoodModels = new ArrayList<>();

    private static final int PAGE_START = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;

    String mAdresse, mLang, mLat = null;
    String text;


    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, SearchActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.search));
        edArea.setVisibility(View.VISIBLE);
        edArea.setClickable(false);
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mOffersAdapter = new OffersAdapter(mContext, mFoodModels);
        mOffersAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setItemAnimator(new DefaultItemAnimator());
        rvRecycle.setAdapter(mOffersAdapter);
        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        if (mSharedPrefManager.getLoginStatus()) {
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Search(mSharedPrefManager.getUserData().getUser_id(), 0, 0, 0, 0, "", "");

                }
            });

            Search(mSharedPrefManager.getUserData().getUser_id(), 0, 0, 0, 0, "", "");
        }else {
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Search(0, 0, 0, 0, 0, "", "");

                }
            });

            Search(0, 0, 0, 0, 0, "", "");
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_search_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(View view, int position) {
        if (mSharedPrefManager.getLoginStatus()) {
            if (view.getId() == R.id.tv_row_title) {
                mListDialog.dismiss();
                if (listDialogType == Constant.SearchKeys.categories) {
                    categoresModel = mListModels.get(position);
                    edCategories.setText(categoresModel.getName());
                    Search(mSharedPrefManager.getUserData().getUser_id(), categoresModel.getId(), 0, 0, 0, "", "");
                    CommonUtil.PrintLogE("cat id : " + categoresModel.getId());
                } else if (listDialogType == Constant.SearchKeys.city) {
                    cityModel = mListModels.get(position);
                    edCity.setText(cityModel.getName());
                    edArea.setVisibility(View.VISIBLE);
                    //getAreas(mLanguagePrefManager.getAppLanguage(),cityModel.getId());
                    //setSearch(mLanguagePrefManager.getAppLanguage(), 0, cityModel.getId(), 0, "", "");
                    edArea.setClickable(true);
                    cityIsChecked = true;

                    CommonUtil.PrintLogE("city id : " + cityModel.getId());
                } else if (listDialogType == Constant.SearchKeys.price) {
                    priceModel = mListModels.get(position);
                    edPrices.setText(priceModel.getName());
                    Search(mSharedPrefManager.getUserData().getUser_id(), 0, 0, 0, priceModel.getId(), "", "");
                    CommonUtil.PrintLogE("price id : " + priceModel.getId());

                } else if (listDialogType == Constant.SearchKeys.area) {
                    areaModel = mListModels.get(position);
                    edArea.setText(areaModel.getName());
                    Search(mSharedPrefManager.getUserData().getUser_id(), 0, areaModel.getId(), 0, 0, "", "");
                    // setSearch(mLanguagePrefManager.getAppLanguage(),0,areaModel.getId(),0,"","");
                }
            } else if (view.getId() == R.id.tv_row_title2) {
                mListDialog2.dismiss();

                // if (listDialogType == Constant.SearchKeys.family){
                familyModel = mFamiliesModel.get(position);
                //int id = familyModel.getId();
                edFamily.setText("");
                Log.e("family_id", familyModel.getShop_id() + "");
                Search(mSharedPrefManager.getUserData().getUser_id(), 0, 0, familyModel.getShop_id(), 0, "", "");
                // getproducts(mLanguagePrefManager.getAppLanguage(),familyModel.getId());
                //}

            } else {
                OrderItemActivity.startActivity((AppCompatActivity) mContext, mFoodModels.get(position));
            }
        }else {
            if (view.getId() == R.id.tv_row_title) {
                mListDialog.dismiss();
                if (listDialogType == Constant.SearchKeys.categories) {
                    categoresModel = mListModels.get(position);
                    edCategories.setText(categoresModel.getName());
                    Search(0, categoresModel.getId(), 0, 0, 0, "", "");
                    CommonUtil.PrintLogE("cat id : " + categoresModel.getId());
                } else if (listDialogType == Constant.SearchKeys.city) {
                    cityModel = mListModels.get(position);
                    edCity.setText(cityModel.getName());
                    edArea.setVisibility(View.VISIBLE);
                    //getAreas(mLanguagePrefManager.getAppLanguage(),cityModel.getId());
                    //setSearch(mLanguagePrefManager.getAppLanguage(), 0, cityModel.getId(), 0, "", "");
                    edArea.setClickable(true);
                    cityIsChecked = true;

                    CommonUtil.PrintLogE("city id : " + cityModel.getId());
                } else if (listDialogType == Constant.SearchKeys.price) {
                    priceModel = mListModels.get(position);
                    edPrices.setText(priceModel.getName());
                    Search(0, 0, 0, 0, priceModel.getId(), "", "");
                    CommonUtil.PrintLogE("price id : " + priceModel.getId());

                } else if (listDialogType == Constant.SearchKeys.area) {
                    areaModel = mListModels.get(position);
                    edArea.setText(areaModel.getName());
                    Search(0, 0, areaModel.getId(), 0, 0, "", "");
                    // setSearch(mLanguagePrefManager.getAppLanguage(),0,areaModel.getId(),0,"","");
                }
            } else if (view.getId() == R.id.tv_row_title2) {
                mListDialog2.dismiss();

                // if (listDialogType == Constant.SearchKeys.family){
                familyModel = mFamiliesModel.get(position);
                //int id = familyModel.getId();
                edFamily.setText("");
                Log.e("family_id", familyModel.getShop_id() + "");
                Search(0, 0, 0, familyModel.getShop_id(), 0, "", "");
                // getproducts(mLanguagePrefManager.getAppLanguage(),familyModel.getId());
                //}

            } else {
                OrderItemActivity.startActivity((AppCompatActivity) mContext, mFoodModels.get(position));
            }
        }
    }

    @OnClick(R.id.iv_location)
    void onAdressSearch() {
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity) mContext);
    }

    @OnClick(R.id.ed_categories)
    void onCategoriesClick() {
        listDialogType = Constant.SearchKeys.categories;
        getCategories();
    }

    @OnClick(R.id.ed_city)
    void onCityClick() {
        listDialogType = Constant.SearchKeys.city;
        getAreas();
    }

    @OnClick(R.id.ed_area)
    void onAreaClick() {
        if (cityIsChecked) {
            listDialogType = Constant.SearchKeys.area;
            getCities(cityModel.getId()+"");
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.choose_area));
        }
    }

    @OnClick(R.id.ed_prices)
    void onPricesClick() {
        listDialogType = Constant.SearchKeys.price;
        getPriceList();
    }
    @OnClick(R.id.ed_family)
    void onFamilyClick(){
        if (edFamily.getText().toString().equals(""))
        {
            CommonUtil.makeToast(mContext,R.string.fill_empty);
        }else {
           if (mSharedPrefManager.getLoginStatus()) {
                getShops(mSharedPrefManager.getUserData().getUser_id());
            }else {
                   getShops(0);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mSharedPrefManager.getLoginStatus()) {
            if (data != null) {
                if (requestCode == Constant.RequestCode.GET_LOCATION) {
                    if (resultCode == RESULT_OK) {
                        mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                        mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                        mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                        Search(mSharedPrefManager.getUserData().getUser_id(), 0, 0, 0, 0, mLat, mLang);
                    }
                }
            }
        }else {
            if (data != null) {
                if (requestCode == Constant.RequestCode.GET_LOCATION) {
                    if (resultCode == RESULT_OK) {
                        mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                        mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                        mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                        Search(0, 0, 0, 0, 0, mLat, mLang);
                    }
                }
            }
        }
    }
    private void getCategories(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCategories(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mListModels =response.body().getData();
                        mListDialog = new ListDialog(mContext,SearchActivity.this,mListModels,getString(R.string.categories));
                        mListDialog.show();
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getAreas(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAreas(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mListModels =response.body().getData();
                        mListDialog = new ListDialog(mContext,SearchActivity.this,mListModels,getString(R.string.area));
                        mListDialog.show();
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    private void getCities(String area_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCities(mLanguagePrefManager.getAppLanguage(),area_id).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mListModels =response.body().getData();
                        mListDialog = new ListDialog(mContext,SearchActivity.this,mListModels,getString(R.string.city));
                        mListDialog.show();
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    private void getPriceList(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getPriceList(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mListModels = response.body().getData();
                        mListDialog = new ListDialog(mContext,SearchActivity.this,mListModels,getString(R.string.prices_range));
                        mListDialog.show();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getShops(int user_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getShops(user_id,edFamily.getText().toString()).enqueue(new Callback<ShopsResponse>() {
            @Override
            public void onResponse(Call<ShopsResponse> call, Response<ShopsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mFamiliesModel = response.body().getData();
                        mListDialog2 =new ListDialog2(mContext,SearchActivity.this,mFamiliesModel,getString(R.string.store));
                        mListDialog2.show();
                    }else {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ShopsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void Search(int userId,int catId,int cityId,int shopId,int priceId,String lat,String lng){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).search(mLanguagePrefManager.getAppLanguage(),userId,catId,cityId,priceId,lat,lng,shopId).enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.no_data);
                        }else {
                            mOffersAdapter.updateAll(response.body().getData());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });
    }

}
