package com.aait.mazaqelkhlij.UI.adapters.Provider;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentRecyclerAdapter;
import com.aait.mazaqelkhlij.base.ParentRecyclerViewHolder;
import com.aait.mazaqelkhlij.models.CategoriesModel;

import java.util.List;

import butterknife.BindView;

public class FamilyCategoriesAdapter extends ParentRecyclerAdapter<CategoriesModel> {

    public FamilyCategoriesAdapter(final Context context, final List<CategoriesModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }
    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        CategoriesModel categoryModel = data.get(position);

        viewHolder.tvCategories.setText(categoryModel.getCategory_name());
        viewHolder.tvCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                itemClickListener.onItemClick(view, position);
                view.setBackgroundColor(mcontext.getResources().getColor(R.color.colorWhite));
                //view..setTextColor(mcontext.getResources().getColor(R.color.colorBlack));
            }
        });
//        if (categoryModel.isChecked()) {
//            viewHolder.tvCategories.setBackgroundColor(mcontext.getResources().getColor(R.color.colorWhite));
//            viewHolder.tvCategories.setTextColor(mcontext.getResources().getColor(R.color.colorBlack));
//        } else {
//            viewHolder.tvCategories
//                    .setBackgroundColor(mcontext.getResources().getColor(R.color.colorWhiteOrange));
//            viewHolder.tvCategories.setTextColor(mcontext.getResources().getColor(R.color.colorWhite));
//        }
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_categories)
        TextView tvCategories;
        @BindView(R.id.cat_id)
        CardView cat_id;

        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}

