package com.aait.mazaqelkhlij.UI.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.fcm.MyFirebaseInstanceIDService;

import butterknife.BindView;

public class SplashActivity extends ParentActivity {

    @BindView(R.id.lay_splash)
    LinearLayout mLinearLayout;

    Animation fade;


    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, SplashActivity.class);
        mAppCompatActivity.startActivity(mIntent);
    }

    @Override
    protected void initializeComponents() {

        CommonUtil.PrintLogE("Token : " +
                MyFirebaseInstanceIDService.getToken(mContext));

        if (mSharedPrefManager.getLoginStatus()){
            if (mSharedPrefManager.getUserData().getType().equals("client")){
                com.aait.mazaqelkhlij.UI.activities.Client.MainActivity.startActivity((AppCompatActivity)mContext);
            }else if (mSharedPrefManager.getUserData().getType().equals("delegate")){
                com.aait.mazaqelkhlij.UI.activities.Delegate.MainActivity.startActivity((AppCompatActivity)mContext);
            }else if (mSharedPrefManager.getUserData().getType().equals("provider")){
                com.aait.mazaqelkhlij.UI.activities.Provider.MainActivity.startActivity((AppCompatActivity)mContext);
            }
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        fade = AnimationUtils.loadAnimation(this, R.anim.alpha);
        mLinearLayout.clearAnimation();
        mLinearLayout.startAnimation(fade);
        fade.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(final Animation animation) {

            }

            @Override
            public void onAnimationEnd(final Animation animation) {

                ChooseLoginActivity.startActivity((AppCompatActivity) mContext);
                SplashActivity.this.finish();

            }

            @Override
            public void onAnimationRepeat(final Animation animation) {
            }
        });
    }
}
