package com.aait.mazaqelkhlij.UI.activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.Client.ProcessedOrderDetailsActivity;
import com.aait.mazaqelkhlij.UI.activities.Delegate.ProcessedOrdersDetailsActivity;
import com.aait.mazaqelkhlij.UI.activities.Delegate.ProvidersActivity;
import com.aait.mazaqelkhlij.UI.adapters.NotificationAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.listeners.PaginationScrollListener;
import com.aait.mazaqelkhlij.models.NotificationModel;
import com.aait.mazaqelkhlij.models.NotificationResponse;
import com.aait.mazaqelkhlij.models.OrderDetailsResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends ParentActivity implements OnItemClickListener {

    LinearLayoutManager linearLayoutManager;

    NotificationAdapter mNotificationAdapter;

    List<NotificationModel> mNotificationList = new ArrayList<>();

    private static final int PAGE_START = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, NotificationActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.notification));
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mNotificationAdapter = new NotificationAdapter(mContext, mNotificationList);
        mNotificationAdapter.setOnItemClickListener(this);
        rvRecycle.setAdapter(mNotificationAdapter);


        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ShowNotifications();
            }
        });

        ShowNotifications();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_recycle_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(View view, int position) {
        NotificationModel notificationModel = mNotificationList.get(position);
        if (notificationModel.getType().equals("order")){
            RetroWeb.getClient().create(ServiceApi.class).getDetails(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Integer.parseInt(notificationModel.getTaggable_id())).enqueue(new Callback<OrderDetailsResponse>() {
                @Override
                public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {

                    if (response.isSuccessful())
                    {
                        if (response.body().getStatus()==1){
                            //setOrderData(response.body().getData());
                            // processOrderStep(response.body().getData().getOrder_status());
                            if (response.body().getData().getOrder_products().size()==0){
                               // rvRecycle.setVisibility(View.GONE);
                            }else {
                                if (mSharedPrefManager.getUserData().getUser_id()!=response.body().getData().getOrder_provider_id()&&
                                        mSharedPrefManager.getUserData().getUser_id()!=response.body().getData().getOrder_delegate_id()) {
                                    Log.e("mmmmm",response.body().getData().getOrder_status()+"");
                                    if (response.body().getData().getOrder_status()==9){
                                        com.aait.mazaqelkhlij.UI.activities.Client.PreviousOrdersDetailsActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getOrder_id());
                                    }else {

                                        com.aait.mazaqelkhlij.UI.activities.Client.ProcessedOrderDetailsActivity.startActivity((AppCompatActivity) mContext, response.body().getData().getOrder_id());
                                    }
                                }else if (mSharedPrefManager.getUserData().getUser_id()==response.body().getData().getOrder_provider_id()){
                                    if (response.body().getData().getOrder_status()==1){
                                        com.aait.mazaqelkhlij.UI.activities.Provider.NewOrderDetailsActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getOrder_id());
                                    }
                                    else if (response.body().getData().getOrder_status()>1|| response.body().getData().getOrder_status()<9){
                                        Intent intent=new Intent(getApplicationContext(),com.aait.mazaqelkhlij.UI.activities.Provider.ProcessedOrderDetailsActivity.class);
                                        intent.putExtra("id",response.body().getData().getOrder_id());
                                        startActivity(intent);
                                    }
                                    else if (response.body().getData().getOrder_status()==9){
                                        com.aait.mazaqelkhlij.UI.activities.Provider.PreviousOrderDetailsActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getOrder_id());
                                    }
                                }
                                if (mSharedPrefManager.getUserData().getType().equals("delegate")){
                                    if (response.body().getData().getOrder_status()==2){
                                        com.aait.mazaqelkhlij.UI.activities.Delegate.NewOrdersDetailsActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getOrder_id());
                                    }else if (response.body().getData().getOrder_status()>=4&& response.body().getData().getOrder_status()<9){
                                        Intent intent=new Intent(getApplicationContext(),com.aait.mazaqelkhlij.UI.activities.Delegate.ProcessedOrdersDetailsActivity.class);
                                        intent.putExtra("id",response.body().getData().getOrder_id());
                                        startActivity(intent);
                                    }else if (response.body().getData().getOrder_status()==9){
                                        com.aait.mazaqelkhlij.UI.activities.Delegate.PreviousOrdersDetailsActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getOrder_id());
                                    }

                                }
                               // mFamilyOrderDetailsAdapter.updateAll(response.body().getData().getOrder_products());
                            }
                        }else {
                            CommonUtil.makeToast(mContext,response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext,t);
                    t.printStackTrace();


                }
            });
        }
        else if (notificationModel.getType().equals("book")){
            ProvidersActivity.startActivity((AppCompatActivity)mContext);
        }else {

        }

        }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mSharedPrefManager.getUserData().getType().equals("client")){
            com.aait.mazaqelkhlij.UI.activities.Client.MainActivity.startActivity((AppCompatActivity)mContext);
        }else if (mSharedPrefManager.getUserData().getType().equals("delegate")){
            com.aait.mazaqelkhlij.UI.activities.Delegate.MainActivity.startActivity((AppCompatActivity)mContext);
        }else if (mSharedPrefManager.getUserData().getType().equals("provider")){
            com.aait.mazaqelkhlij.UI.activities.Provider.MainActivity.startActivity((AppCompatActivity)mContext);
        }
    }

    private void ShowNotifications(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).showNotification(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getUser_id())
                .enqueue(new Callback<NotificationResponse>() {
                    @Override
                    public void onResponse(Call<NotificationResponse> call,
                                           Response<NotificationResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if(response.body().getStatus()==1){
                            if (response.body().getData().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                mNotificationAdapter.updateAll(response.body().getData());

                            }}else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<NotificationResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
}
