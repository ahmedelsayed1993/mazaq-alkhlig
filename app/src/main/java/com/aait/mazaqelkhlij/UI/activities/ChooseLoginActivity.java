package com.aait.mazaqelkhlij.UI.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.Client.LoginActivity;
import com.aait.mazaqelkhlij.base.ParentActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class ChooseLoginActivity extends ParentActivity {

    @BindView(R.id.delivery_btn)
    Button delivery;
    @BindView(R.id.family_btn)
    Button family;
    @BindView(R.id.client_btn)
    Button client;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, ChooseLoginActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_choose_login;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.client_btn)
    void onClientClick(){
        com.aait.mazaqelkhlij.UI.activities.Client.LoginActivity.startActivity((AppCompatActivity)mContext,"1");
    }
    @OnClick(R.id.family_btn)
    void onFamilyClick(){
       com.aait.mazaqelkhlij.UI.activities.Provider.LoginActivity.startActivity((AppCompatActivity)mContext,"provider");
    }
    @OnClick(R.id.delivery_btn)
    void onDeliveryClick(){
       com.aait.mazaqelkhlij.UI.activities.Delegate.LoginActivity.startActivity((AppCompatActivity)mContext,"delegate");
    }
}
