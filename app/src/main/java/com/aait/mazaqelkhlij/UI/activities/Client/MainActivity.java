package com.aait.mazaqelkhlij.UI.activities.Client;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.fragments.Client.HomeFragment;
import com.aait.mazaqelkhlij.UI.fragments.Client.NavigationFragment;
import com.aait.mazaqelkhlij.UI.fragments.Client.OffersFragment;
import com.aait.mazaqelkhlij.UI.fragments.Client.OrdersFragment;
import com.aait.mazaqelkhlij.UI.views.BottomNavigationViewHelper;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.DrawerListner;
import com.aait.mazaqelkhlij.models.CartResponse;
import com.google.gson.Gson;

import java.util.Stack;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends ParentActivity implements DrawerListner, BottomNavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemReselectedListener  {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.nav_view)
    FrameLayout navView;

    @BindView(R.id.app_bar)
    AppBarLayout app_bar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.iv_menu)
    ImageView ivMenu;

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.card_text)
    TextView card_text;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;

    NavigationFragment mNavigationFragment;

    HomeFragment mHomeFragment;
    // SearchFragment mSearchFragment;

    OrdersFragment mOrdersFragment;

    OffersFragment mOffersFragment;

    int selectedTab = 0;

    private Stack<Integer> tabsStack = new Stack<>();

    private FragmentManager fragmentManager;

    private FragmentTransaction transaction;
    static String i = "";

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, MainActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    @OnClick(R.id.iv_menu)
    void onMenuClick() {
        this.OpenCloseDrawer();
    }

    @OnClick(R.id.iv_card)
    void onCardClick() {
        if (mSharedPrefManager.getLoginStatus()) {
            CardBasketActivity.startActivity((AppCompatActivity) mContext);
        }else {
            CommonUtil.makeToast(mContext,R.string.must_login);
        }
    }

    @OnClick(R.id.iv_search)
    void onSearchClick() {

        SearchActivity.startActivity((AppCompatActivity) mContext);
    }
    @Override
    public void onNavigationItemReselected(@NonNull MenuItem item) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                showHome(true);
                break;
            case R.id.navigation_order:
                showOrders(true);
                break;
            case R.id.navigation_offers:
                showOffers(true);
                break;
        }
        return true;

    }

    public void initializeBottomNav() {

        mHomeFragment = HomeFragment.newInstance();
        mOrdersFragment = OrdersFragment.newInstance();
        mOffersFragment = OffersFragment.newInstance();
        // mSearchFragment = SearchFragment.newInstance();

        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.home_fragment_container, mHomeFragment);
        // transaction.add(R.id.home_fragment_container, mSearchFragment);
        transaction.add(R.id.home_fragment_container, mOrdersFragment);
        transaction.add(R.id.home_fragment_container, mOffersFragment);
        transaction.commit();
        bottomNavigation.setOnNavigationItemSelectedListener(this);
        bottomNavigation.setOnNavigationItemReselectedListener(this);
        bottomNavigation.setSelectedItemId(R.id.navigation_home);
        showHome(true);
    }

    private void showOffers(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
        transaction.hide(mHomeFragment);
        transaction.hide(mOrdersFragment);
        transaction.show(mOffersFragment);
        transaction.commit();
        selectedTab = R.id.navigation_offers;

        tvTitle.setText(R.string.offers);
        showParElevation(true);

    }

    private void showOrders(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
        transaction.hide(mOffersFragment);
        transaction.hide(mHomeFragment);
        transaction.show(mOrdersFragment);
        transaction.commit();
        selectedTab = R.id.navigation_order;
        tvTitle.setText(R.string.orders);
        showParElevation(true);

    }

    private void showHome(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
        transaction.hide(mOrdersFragment);
        transaction.hide(mOffersFragment);
        transaction.show(mHomeFragment);
        transaction.commit();
        selectedTab = R.id.navigation_home;

        tvTitle.setText(R.string.home);
        showParElevation(false);
    }

    public void showParElevation(boolean showHide) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            if (showHide) {
                app_bar.setElevation((float) 5.0);

            } else {
                app_bar.setElevation((float) 0.0);
            }
        }

    }

    @Override
    public void onBackPressed() {
        if (tabsStack.size() > 0) {
            bottomNavigation.setOnNavigationItemSelectedListener(null);
            int selectedTab = tabsStack.pop();
            bottomNavigation.setSelectedItemId(selectedTab);
            switch (selectedTab) {
                case R.id.navigation_home:
                    showHome(false);
                    break;
                case R.id.navigation_order:
                    showOrders(false);
                    break;
                case R.id.navigation_offers:
                    showOffers(false);
                    break;
            }
            bottomNavigation.setOnNavigationItemSelectedListener(this);
        } else {
//            DialogUtil.showAlertDialog(mContext, getString(R.string.need_to_close_the_app), new OnClickListener() {
//                @Override
//                public void onClick(final DialogInterface dialog, final int which) {
//                    MainActivity.super.onBackPressed();
//                }
//            }, new OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
            MainActivity.this.finish();
        }

    }

    @Override
    protected void initializeComponents() {
        mNavigationFragment = NavigationFragment.newInstance();
        mNavigationFragment.setDrawerListner(this);

        getSupportFragmentManager().beginTransaction().replace(R.id.nav_view, mNavigationFragment).commit();
        initializeBottomNav();
        BottomNavigationViewHelper.disableShiftMode(bottomNavigation);
        getCart();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void OpenCloseDrawer() {
        mNavigationFragment.setNavData();
        if (drawerLayout != null) {
            if (mLanguagePrefManager.getAppLanguage().equals("en")) {
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            } else {
                if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        }
    }
    private void getCart(){
        if (mSharedPrefManager.getLoginStatus()) {
            RetroWeb.getClient().create(ServiceApi.class).showCart(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getUser_id()).enqueue(new Callback<CartResponse>() {
                @Override
                public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getStatus() == 1) {
                            if (response.body().getData().size() == 0) {
                                card_text.setVisibility(View.GONE);

                            } else {
                                card_text.setVisibility(View.VISIBLE);
                                card_text.setText(response.body().getData().size() + "");
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<CartResponse> call, Throwable t) {
                    CommonUtil.onPrintLog(new Gson().toJson(t));
                    CommonUtil.handleException(mContext, t);
                    t.printStackTrace();

                }
            });
        }else {

        }
    }
}
