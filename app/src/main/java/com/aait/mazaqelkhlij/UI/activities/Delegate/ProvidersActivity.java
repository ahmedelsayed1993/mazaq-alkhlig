package com.aait.mazaqelkhlij.UI.activities.Delegate;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.Provider.StoreActivity;
import com.aait.mazaqelkhlij.UI.adapters.Provider.DelegatesAgapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.DelegatesResponse;
import com.aait.mazaqelkhlij.models.UserModel;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProvidersActivity extends ParentActivity implements OnItemClickListener {

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.progress_wheel)
    ProgressWheel progressWheel;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.iv_no_internet)
    ImageView ivNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.iv_no_item)
    ImageView ivNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.rv_recycle)
    RecyclerView rv_Recycle;
    ArrayList<UserModel> userModels = new ArrayList<>();
    DelegatesAgapter delegatesAgapter;
    LinearLayoutManager linearLayoutManager;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, ProvidersActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.provider_requests));
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        delegatesAgapter = new DelegatesAgapter(mContext, userModels);
        delegatesAgapter.setOnItemClickListener(this);
        rv_Recycle.setLayoutManager(linearLayoutManager);
        rv_Recycle.setItemAnimator(new DefaultItemAnimator());
        rv_Recycle.setAdapter(delegatesAgapter);

        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProviders();
            }
        });

        getProviders();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_provider_to_delegate;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(View view, int position) {
        AcceptProviderReqestActivity.startActivity((AppCompatActivity)mContext,userModels.get(position));

    }
    private void getProviders(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getProviders(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id()).enqueue(new Callback<DelegatesResponse>() {
            @Override
            public void onResponse(Call<DelegatesResponse> call, Response<DelegatesResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()) {
                    if (response.body().getData().size() == 0) {
                        layNoItem.setVisibility(View.VISIBLE);
                        layNoInternet.setVisibility(View.GONE);
                        tvNoContent.setText(R.string.no_data);
                    } else {
                        delegatesAgapter.updateAll(response.body().getData());
                    }
                }
            }

            @Override
            public void onFailure(Call<DelegatesResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });
    }
}
