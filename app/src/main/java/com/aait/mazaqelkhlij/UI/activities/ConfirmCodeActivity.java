package com.aait.mazaqelkhlij.UI.activities;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.Client.LoginActivity;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.ValidationUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.fcm.MyFirebaseInstanceIDService;
import com.aait.mazaqelkhlij.models.ConfirmCodeModel;
import com.aait.mazaqelkhlij.models.ForgetPasswordModel;
import com.aait.mazaqelkhlij.models.ForgotPasswordResponse;
import com.aait.mazaqelkhlij.models.NewPasswordResponse;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmCodeActivity extends ParentActivity {

    @BindView(R.id.til_code)
    TextInputLayout tilCode;

    @BindView(R.id.et_code)
    TextInputEditText etCode;


    String forgetPassModel;
    String reg ;
    String type;

String user_id;
    public static void startActivity(AppCompatActivity mAppCompatActivity,String user_id ){
        Intent mIntent = new Intent(mAppCompatActivity, ConfirmCodeActivity.class);
        mIntent.putExtra("user_id",user_id);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }



    void getBundleData() {
        user_id = (String) getIntent().getSerializableExtra("user_id");
       // forgetPassModel = (String) getIntent().getSerializableExtra(Constant.BundleData.FORGET_PASS_MODEL);
       // type = (String) getIntent().getSerializableExtra("type");
    }
    @Override
    protected void initializeComponents() {
        getBundleData();
        reg = MyFirebaseInstanceIDService.getToken(mContext);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_confirm_account_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    boolean confirmCodeValidation() {
        if (!ValidationUtils.checkError(etCode, tilCode, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }

    @OnClick(R.id.btn_register)
    void onBtnRegisterClick() {
        if (confirmCodeValidation()){
//         if (!forgetPassModel.equals("")) {
//            confirmnewpass();
//         }else {
            confirm();
        // }
        }
    }
    private void confirm(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).confirm(user_id,etCode.getText().toString()).enqueue(new Callback<ConfirmCodeModel>() {
            @Override
            public void onResponse(Call<ConfirmCodeModel> call, Response<ConfirmCodeModel> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().issucessfull()){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        if (response.body().getData().equals("client")) {
                           com.aait.mazaqelkhlij.UI.activities.Client. LoginActivity.startActivity((AppCompatActivity) mContext, response.body().getData());
                        }else if (response.body().getData().equals("delegate")){
                            com.aait.mazaqelkhlij.UI.activities.Delegate.LoginActivity.startActivity((AppCompatActivity)mContext,response.body().getData());
                        }else if (response.body().getData().equals("provider")){
                            com.aait.mazaqelkhlij.UI.activities.Provider.LoginActivity.startActivity((AppCompatActivity)mContext,response.body().getData());
                        }
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ConfirmCodeModel> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }
    private void confirmnewpass(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).forgetPassword(forgetPassModel,etCode.getText().toString())
                .enqueue(new Callback<NewPasswordResponse>() {
            @Override
            public void onResponse(Call<NewPasswordResponse> call, Response<NewPasswordResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        NewPasswordActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getUser_id()+"",type);

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<NewPasswordResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
