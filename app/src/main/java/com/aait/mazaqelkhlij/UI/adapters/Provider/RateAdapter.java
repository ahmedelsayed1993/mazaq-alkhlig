package com.aait.mazaqelkhlij.UI.adapters.Provider;

import android.content.Context;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentRecyclerAdapter;
import com.aait.mazaqelkhlij.base.ParentRecyclerViewHolder;
import com.aait.mazaqelkhlij.models.ShopRateModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class RateAdapter extends ParentRecyclerAdapter<ShopRateModel> {

    public RateAdapter(final Context context, final List<ShopRateModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }
    @Override
    public int getItemCount() {
        return data.size();
    }
    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        ShopRateModel rateModel = data.get(position);
        Glide.with(mcontext).load(rateModel.getComment_user_img()).asBitmap().skipMemoryCache(true)
                .placeholder(R.mipmap.splash)
                .into(viewHolder.civImage);
        viewHolder.tvName.setText(rateModel.getComment_username());
        viewHolder.tvRate.setText(rateModel.getComment());
        viewHolder.tvDate.setText(rateModel.getComment_date());
       // viewHolder.ratingBar.setRating(Float.parseFloat(rateModel.getRate()));

    }


    public class ViewHolder extends ParentRecyclerViewHolder {


        @BindView(R.id.civ_image)
        CircleImageView civImage;

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.rating_bar)
        AppCompatRatingBar ratingBar;

        @BindView(R.id.tv_rate)
        TextView tvRate;


        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}

