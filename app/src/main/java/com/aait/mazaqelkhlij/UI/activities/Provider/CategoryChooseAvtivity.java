package com.aait.mazaqelkhlij.UI.activities.Provider;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.CategoryModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class CategoryChooseAvtivity extends ParentActivity implements OnItemClickListener {

    @BindView(R.id.tv_cancel)
    TextView tvCancel;

    @BindView(R.id.tv_done)
    TextView tvDone;

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    RecyclerView.LayoutManager layoutManager;

  //  CategoryChooseAdapter mCategoryChooseAdapter;

    List<CategoryModel> mCategoryModels = new ArrayList<>();

    ArrayList<String> userChooseCategries = new ArrayList<>();

    String categoriesJson = "";

    String categoriesJsonId = "";

    boolean isitemSelected = false;
    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_choose_category_family;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @OnClick(R.id.tv_done)
    void onDoneClick() {
        sedResultBackAndFinish();
    }

    @OnClick(R.id.tv_cancel)
    void onCancelClick() {
        finish();
    }

    void sedResultBackAndFinish() {
        Intent intent = new Intent();
        intent.putExtra(Constant.ResultData.CHOOSE_CATEGORIES, categoriesJson);
        setResult(RESULT_OK, intent);
        finish();
    }
}
