package com.aait.mazaqelkhlij.UI.adapters.Provider;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.base.ParentRecyclerAdapter;
import com.aait.mazaqelkhlij.base.ParentRecyclerViewHolder;
import com.aait.mazaqelkhlij.models.BanksModel;

import java.util.List;

import butterknife.BindView;

public class AccountsAdapter extends ParentRecyclerAdapter<BanksModel> {
    public AccountsAdapter(final Context context, final List<BanksModel> data, final int layoutId) {
        super(context,data,layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        BanksModel accountsModel = data.get(position);
        viewHolder.account_name.setText(accountsModel.getAcc_name());
        viewHolder.account_number.setText(accountsModel.getBank_number());
        viewHolder.bank_name.setText(accountsModel.getBank_name());
        viewHolder.iban.setText(accountsModel.getIban());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends ParentRecyclerViewHolder{

        @BindView(R.id.account_number)
        TextView account_number;

        @BindView(R.id.bank_name)
        TextView bank_name;

        @BindView(R.id.account_name)
        TextView account_name;
        @BindView(R.id.iban)
        TextView iban;
        public ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }


}

