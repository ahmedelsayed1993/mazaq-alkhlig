package com.aait.mazaqelkhlij.UI.fragments.Provider;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.listeners.OrderHasDelivery;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseDeliveryDialogFragment extends DialogFragment {

    @BindView(R.id.myRadioGroup)
    RadioGroup myRadioGroup;

    @BindView(R.id.radio_family_delivery)
    RadioButton radioFamilyDelivery;

    @BindView(R.id.radio_another_delivery)
    RadioButton radioAnotherDelivery;

    OrderHasDelivery listner;

    public ChooseDeliveryDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public void setListner(OrderHasDelivery listner) {
        this.listner = listner;
    }

    public static ChooseDeliveryDialogFragment newInstance() {
        ChooseDeliveryDialogFragment frag = new ChooseDeliveryDialogFragment();
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_choose_delivery_family, container);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // radioAnotherDelivery.setChecked(true);
    }

    @Override
    public void onResume() {
        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);
        getDialog().getWindow().setWindowAnimations(R.style.DialogAnimation_2);
        // Call super onResume after sizing
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.tv_ok)
    void onConfirmClick() {
        if (radioFamilyDelivery.isChecked()) {
            listner.orderHasDelivery(true);
        } else if (radioAnotherDelivery.isChecked()) {
            listner.orderHasDelivery(false);
        }
        dismiss();
    }

    @OnClick(R.id.tv_cancel)
    void onCancelClick() {
        dismiss();
    }
}
