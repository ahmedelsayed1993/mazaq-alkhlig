package com.aait.mazaqelkhlij.UI.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.models.ImagesModel;
import com.bumptech.glide.Glide;

import java.util.List;

public class RecyclerPoupupAds extends PagerAdapter {

    Context context;
    List<ImagesModel> data;
    // private ImageView.ScaleType scaleType;
    private LayoutInflater layoutInflater;

    public RecyclerPoupupAds(Context context, List<ImagesModel>data) {
        this.context = context;

        this.data=data;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View imageLayout = layoutInflater.inflate(R.layout.recycler_image,null);
        // assert imageLayout != null;
        final ImageView imageView = imageLayout.findViewById(R.id.food_img);

        Glide.with(context).load(data.get(position).getImage_path()).into(imageView);
        ViewPager vp =(ViewPager) container;
        vp.addView(imageLayout,0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}
