package com.aait.mazaqelkhlij.UI.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.PermissionUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.ContactUsResponse;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsActivity extends ParentActivity {

    @BindView(R.id.tv_email)
    TextView tvEmail;

    @BindView(R.id.tv_mobile_numer)
    TextView tvMobileNumer;

    @BindView(R.id.tv_contact_via_facebook)
    TextView tvContactViaFacebook;
    @BindView(R.id.tv_contact_via_twitter)
    TextView tvContactViaTwitter;
    @BindView(R.id.tv_contact_via_insta)
    TextView tvContactViaInsta;
    @BindView(R.id.tv_contact_via_snapchat)
    TextView tvContactViaSnapChat;

    String facebook, email, number,twitter,insta,snap;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, ContactUsActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.contact_us));
        contactUS();
    }

    @Override
    protected int getLayoutResource() {
        return  R.layout.activity_contact_us_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @OnClick(R.id.tv_contact_via_twitter)
    void onTwitterClick() {
        if (!twitter.equals("")) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(twitter)));
        } else {
            CommonUtil.makeToast(mContext, getString(R.string.no_data));
        }
    }
    @OnClick(R.id.tv_contact_via_insta)
    void onInstaClick() {
        if (!insta.equals("")) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(insta)));
        } else {
            CommonUtil.makeToast(mContext, getString(R.string.no_data));
        }
    }
    @OnClick(R.id.tv_contact_via_snapchat)
    void onSnapChatClick() {
        if (!snap.equals("")) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(snap)));
        } else {
            CommonUtil.makeToast(mContext, getString(R.string.no_data));
        }
    }
    @OnClick(R.id.tv_contact_via_facebook)
    void onFacebookClick() {
        if (!facebook.equals("")) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(facebook)));
        } else {
            CommonUtil.makeToast(mContext, getString(R.string.no_data));
        }
    }

    @OnClick(R.id.lay_email)
    void onEmailClick() {
        if (!email.equals("")) {
            sendEmail(email);
        } else {
            CommonUtil.makeToast(mContext, getString(R.string.no_data));
        }
    }


    @OnClick(R.id.lay_number)
    void onNumberClick() {
        if (!number.equals("")) {
            getLocationWithPermission(number);
        } else {
            CommonUtil.makeToast(mContext, getString(R.string.no_data));
        }
    }


    public void getLocationWithPermission(String number) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(ContactUsActivity.this, PermissionUtils.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.CALL_PHONE,
                            Constant.RequestPermission.REQUEST_CALL);
                }
            } else {
                callnumber(number);
            }
        } else {
            callnumber(number);
        }

    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        switch (permsRequestCode) {
            case 800: {
                if (grantResults.length > 0) {
                    boolean Locationpermission = (grantResults[0] == PackageManager.PERMISSION_GRANTED);
                    callnumber(number);
                    for (int i = 0; i < grantResults.length; i++) {
                    }
                } else {
                }
                return;
            }
        }
    }

    void sendEmail(String email) {

        Log.i("Send email", "");
        String[] TO = {email};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.i("Finished sending emal.", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }}

    void callnumber(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        startActivity(intent);
    }

    private void contactUS() {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAppInfo(mLanguagePrefManager.getAppLanguage())
                .enqueue(new Callback<ContactUsResponse>() {
                    @Override
                    public void onResponse(Call<ContactUsResponse> call,
                                           Response<ContactUsResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().issucessfull()) {
                                tvEmail.setText(response.body().getData().getEmail());
                                tvMobileNumer.setText(response.body().getData().getPhone());
                                facebook = response.body().getData().getFacebook();
                                email = response.body().getData().getEmail();
                                number = response.body().getData().getPhone();
                                twitter = response.body().getData().getTwitter();
                                insta = response.body().getData().getInstagram();
                                snap = response.body().getData().getSnapchat();

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ContactUsResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        hideProgressDialog();
                    }
                });
    }


}
