package com.aait.mazaqelkhlij.UI.fragments.Client;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.Client.OrderItemActivity;
import com.aait.mazaqelkhlij.UI.adapters.Client.OffersAdapters1;
import com.aait.mazaqelkhlij.UI.views.ListDialog1;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.BaseFragment;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.listeners.PaginationAdapterCallback;
import com.aait.mazaqelkhlij.listeners.PaginationScrollListener;
import com.aait.mazaqelkhlij.models.ListModel;
import com.aait.mazaqelkhlij.models.ListModelResponse;
import com.aait.mazaqelkhlij.models.ProductModel;
import com.aait.mazaqelkhlij.models.SearchResponse;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends BaseFragment implements OnItemClickListener,PaginationAdapterCallback {

    @BindView(R.id.ed_categories)
    EditText edCategories;

    @BindView(R.id.ed_city)
    EditText edCity;
    @BindView(R.id.ed_area)
    EditText edArea;
    // @BindView(R.id.ed_prices)
    //  EditText edPrices;

    // @BindView(R.id.iv_location)
    // ImageView ivLocation;
    ListDialog1 mListDialog;
    ArrayList<ListModel> mListModels;
    ListModel categoresModel = null;

    ListModel cityModel = null;

    ListModel areaModel = null;

    ListModel priceModel = null;

    int listDialogType;


    // recycle items
    GridLayoutManager linearLayoutManager;

    OffersAdapters1 mOffersAdapter;

    List<ProductModel> mFoodModels = new ArrayList<>();

    private static final int PAGE_START = 1;
    boolean cityIsChecked = false;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;

    String mAdresse, mLang, mLat = null;
    String city_id ;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home_client;
    }

    @Override
    protected void initializeComponents(View view) {
        edArea.setVisibility(View.VISIBLE);
        edArea.setClickable(false);
        linearLayoutManager = new GridLayoutManager(mContext, 2);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mOffersAdapter = new OffersAdapters1(mContext, mFoodModels);
        mOffersAdapter.setOnItemClickListener(this);
        mOffersAdapter.setOnPaginationClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setItemAnimator(new DefaultItemAnimator());
        rvRecycle.setAdapter(mOffersAdapter);
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

        });

        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        if (mSharedPrefManager.getLoginStatus()){
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                search(mSharedPrefManager.getUserData().getUser_id(), 0, 0);

            }
        });

        search(mSharedPrefManager.getUserData().getUser_id(),0,0);}
        else {
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    search(0, 0, 0);

                }
            });

            search(0,0,0);
        }
     }


    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(View view, int position) {
        if (mSharedPrefManager.getLoginStatus()) {
            if (view.getId() == R.id.tv_row_title2) {
                mListDialog.dismiss();
                if (listDialogType == Constant.SearchKeys.categories) {
                    categoresModel = mListModels.get(position);
                    edCategories.setText(categoresModel.getName());
                    search(mSharedPrefManager.getUserData().getUser_id(), categoresModel.getId(), 0);
                    CommonUtil.PrintLogE("cat id : " + categoresModel.getId());
                } else if (listDialogType == Constant.SearchKeys.city) {

                    cityModel = mListModels.get(position);
                    edCity.setText(cityModel.getName());
                    city_id = cityModel.getId() + "";
                    // edArea.setVisibility(View.VISIBLE);
                    edArea.setClickable(true);
                    cityIsChecked = true;
                    //getAreas(mLanguagePrefManager.getAppLanguage(),cityModel.getId());
                    //setSearch(mLanguagePrefManager.getAppLanguage(), 0, cityModel.getId(), 0, "", "");

                    CommonUtil.PrintLogE("city id : " + cityModel.getId());
                } else if (listDialogType == Constant.SearchKeys.area) {
                    areaModel = mListModels.get(position);
                    edArea.setText(areaModel.getName());
                    search(mSharedPrefManager.getUserData().getUser_id(), 0, areaModel.getId());
                    CommonUtil.PrintLogE("area id : " + areaModel.getId());
                }
            } else {
                OrderItemActivity.startActivity((AppCompatActivity) mContext, mFoodModels.get(position));
            }
        }else {
            if (view.getId() == R.id.tv_row_title2) {
                mListDialog.dismiss();
                if (listDialogType == Constant.SearchKeys.categories) {
                    categoresModel = mListModels.get(position);
                   // categoresModel = new ListModel(mListModels.get(position).getId(),mListModels.get(position).getName());
                    edCategories.setText(categoresModel.getName());
                    search(0, categoresModel.getId(), 0);
                    CommonUtil.PrintLogE("cat id : " + categoresModel.getId());
                } else if (listDialogType == Constant.SearchKeys.city) {

                    cityModel = mListModels.get(position);
                    edCity.setText(cityModel.getName());
                    city_id = cityModel.getId() + "";
                    // edArea.setVisibility(View.VISIBLE);
                    edArea.setClickable(true);
                    cityIsChecked = true;
                    //getAreas(mLanguagePrefManager.getAppLanguage(),cityModel.getId());
                    //setSearch(mLanguagePrefManager.getAppLanguage(), 0, cityModel.getId(), 0, "", "");

                    CommonUtil.PrintLogE("city id : " + cityModel.getId());
                } else if (listDialogType == Constant.SearchKeys.area) {
                    areaModel = mListModels.get(position);
                    //areaModel = new ListModel(mListModels.get(position).getId(),mListModels.get(position).getName());
                    edArea.setText(areaModel.getName());

                    search(0, 0, areaModel.getId());
                    CommonUtil.PrintLogE("area id : " + areaModel.getId());
                }
            } else {
                OrderItemActivity.startActivity((AppCompatActivity) mContext, mFoodModels.get(position));
            }
        }

    }

    @Override
    public void retryPageLoad() {

    }

    @OnClick(R.id.ed_categories)
    void onCategoriesClick() {
        listDialogType = Constant.SearchKeys.categories;
        getCategories();
    }

    @OnClick(R.id.ed_city)
    void onCityClick() {
        listDialogType = Constant.SearchKeys.city;
        getAreas();
    }

    @OnClick(R.id.ed_area)
    void onAreaClick() {
        if (cityIsChecked) {
            listDialogType = Constant.SearchKeys.area;
            getCities(cityModel.getId()+"");
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.choose_area));
        }
    }
    private void getCategories(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCategories(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mListModels =response.body().getData();
                        mListDialog = new ListDialog1(mContext,HomeFragment.this,mListModels,getString(R.string.categories));
                        mListDialog.show();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {

                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getAreas(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAreas(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mListModels = response.body().getData();
                        mListDialog= new ListDialog1(mContext,HomeFragment.this,mListModels,getString(R.string.area));
                        mListDialog.show();
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getCities(String area_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCities(mLanguagePrefManager.getAppLanguage(),area_id).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mListModels = response.body().getData();
                        mListDialog = new ListDialog1(mContext,HomeFragment.this,mListModels,getString(R.string.city));
                        mListDialog.show();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void search(int user_id,int cat_id,int city_id){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).search(mLanguagePrefManager.getAppLanguage(),user_id,cat_id,city_id,0,"","",0).enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.no_data);
                        }else {
                            mOffersAdapter.updateAll(response.body().getData());
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                Log.e("ddd",new Gson().toJson(t));
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);

            }
        });
    }
}
