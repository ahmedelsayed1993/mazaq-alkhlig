package com.aait.mazaqelkhlij.UI.activities.Provider;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.NewAdapter;
import com.aait.mazaqelkhlij.UI.adapters.Provider.RecyclerViewAddPhotosAddAD;
import com.aait.mazaqelkhlij.UI.adapters.RecyclerPoupupAds;
import com.aait.mazaqelkhlij.UI.views.CategoryDialog;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.PermissionUtils;
import com.aait.mazaqelkhlij.Uitls.ProgressRequestBody;
import com.aait.mazaqelkhlij.Uitls.ValidationUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.ChooseCategoryListner;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.CategoriesModel;
import com.aait.mazaqelkhlij.models.CategoryModel;
import com.aait.mazaqelkhlij.models.ImagesModel;
import com.aait.mazaqelkhlij.models.ListModel;
import com.aait.mazaqelkhlij.models.ProductModel;
import com.aait.mazaqelkhlij.models.ProductResponse;
import com.google.gson.Gson;
import com.pixelcan.inkpageindicator.InkPageIndicator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.mazaqelkhlij.App.Constant.RequestPermission.REQUEST_IMAGES;
import static gun0912.tedbottompicker.util.RealPathUtil.getDataColumn;
import static gun0912.tedbottompicker.util.RealPathUtil.isDownloadsDocument;
import static gun0912.tedbottompicker.util.RealPathUtil.isExternalStorageDocument;
import static gun0912.tedbottompicker.util.RealPathUtil.isGooglePhotosUri;
import static gun0912.tedbottompicker.util.RealPathUtil.isMediaDocument;

public class AddProductActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks,OnItemClickListener {







    @BindView(R.id.til_product_name)
    TextInputLayout tilProductName;

    @BindView(R.id.et_product_name)
    TextInputEditText etProductName;

    @BindView(R.id.til_product_name_arabic)
    TextInputLayout tilProductNameArabic;

    @BindView(R.id.et_product_name_arabic)
    TextInputEditText etProductNameArabic;


    @BindView(R.id.til_product_price)
    TextInputLayout tilProductPrice;

    @BindView(R.id.et_product_price)
    TextInputEditText etProductPrice;

    @BindView(R.id.til_product_description)
    TextInputLayout tilProductDescription;

    @BindView(R.id.et_product_description)
    TextInputEditText etProductDescription;


    @BindView(R.id.til_activity)
    TextInputLayout tilActivity;

    @BindView(R.id.et_activity)
    TextInputEditText etActivity;

    @BindView(R.id.tv_addition_title)
    TextView tvAdditionTitle;

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.btn_add)
    ImageView btnAdd;
    @BindView(R.id.add)
    ImageView add;
    ArrayList<MultipartBody.Part> imgs;
    static int i =0;

    LinearLayoutManager linearLayoutManager;




    RecyclerViewAddPhotosAddAD adapter;

    String additionsList;

    String CategoriesList;

    //RegisterSendModel mRegisterSendModel;

    // select image from callery
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;
    String ImageBasePath1 = null;
    String ImageBasePath2 = null;
    ArrayList<String> images = new ArrayList<>();


    CategoryModel mCategoryModel;
    @BindView(R.id.ads_lay)
    RelativeLayout Images;
    @BindView(R.id.viewpager1)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    InkPageIndicator indicator;
    ArrayList<CategoriesModel> mCategories = new ArrayList<>();
    CategoriesModel categoriesModel;
    CategoryDialog categoriesDialog;





    ProductModel mFoodModel;
    // ProductModel mFoodModel;
//    ImagesModel mImagesModel;
    NewAdapter recyclerPoupupAds;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;


    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, AddProductActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.add_product));

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_add_product_family;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    boolean validateAddProduct() {
        if (images.size() == 0) {
            CommonUtil.makeToast(mContext, getString(R.string.choose_product_image));
            return false;

        } else if (!ValidationUtils
                .checkError(etProductNameArabic, tilProductNameArabic, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etProductName, tilProductName, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etProductPrice, tilProductPrice, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils
                .checkError(etProductDescription, tilProductDescription, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etActivity, tilActivity, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }








     @OnClick(R.id.btn_add_order)
     void onAddProdClick(){
        if (validateAddProduct()) {
            AddProduct(images);
        }
     }
    private void Categories(){
        mCategories = mSharedPrefManager.getUserData().getShop().getShop_categories();
        categoriesDialog = new CategoryDialog(mContext,AddProductActivity.this,mCategories,getString(R.string.categories));
        categoriesDialog.show();

    }
    @OnClick(R.id.et_activity)
    void onActivityClick(){
        Categories();
    }

    @OnClick(R.id.add)
    void onAddClick(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        for (int i =0;i<ImageList.size();i++){
                            images.add(ImageList.get(i).getPath());
                        }
                        recyclerPoupupAds = new NewAdapter(AddProductActivity.this,images);
                        viewPager.setAdapter(recyclerPoupupAds);

                        indicator.setViewPager(viewPager);

                        NUM_PAGES = recyclerPoupupAds.getCount();
                        final Handler handler = new Handler();
                        final Runnable update = new Runnable() {
                            @Override
                            public void run() {
                                if (currentPage == NUM_PAGES){
                                    currentPage = 0;
                                }
                                viewPager.setCurrentItem(currentPage++,true);
                            }
                        };
                        Timer swipeTimer = new Timer();
                        swipeTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                handler.post(update);
                            }
                        }, 2500, 2500);
                        ImageBasePath = ImageList.get(0).getPath();
                        //civProfilePic.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(10)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }


    @Override
    public void onItemClick(View view, int position) {
        categoriesDialog.dismiss();
        categoriesModel = new CategoriesModel(mCategories.get(position).getCategory_id(),mCategories.get(position).getCategory_name());
        etActivity.setText(categoriesModel.getCategory_name());
        Log.e("category",categoriesModel.getCategory_name());
    }
    private void AddProduct(ArrayList<String> paths){
        List<MultipartBody.Part> img = new ArrayList<>();
        showProgressDialog(getString(R.string.please_wait));
        for (String photo :paths) {
            MultipartBody.Part filePart = null;
            File ImageFile = new File(photo);
            ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, AddProductActivity.this);
            filePart = MultipartBody.Part.createFormData("images[]", ImageFile.getName(), fileBody);
            img.add(filePart);
        }
        RetroWeb.getClient().create(ServiceApi.class).addProduct(mSharedPrefManager.getUserData().getUser_id(),etProductNameArabic.getText().toString(),etProductName.getText().toString(),etProductDescription.getText().toString(),etProductPrice.getText().toString(),categoriesModel.getCategory_id()+"",img).enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.product_added));
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
