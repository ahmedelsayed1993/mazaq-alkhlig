package com.aait.mazaqelkhlij.UI.fragments.Client;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.Network.Urls;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.AboutUsActivity;
import com.aait.mazaqelkhlij.UI.activities.ChooseLoginActivity;
import com.aait.mazaqelkhlij.UI.activities.Client.PreviousOrdersActivity;
import com.aait.mazaqelkhlij.UI.activities.Client.ProfileActivity;
import com.aait.mazaqelkhlij.UI.activities.ComplainsActivity;
import com.aait.mazaqelkhlij.UI.activities.ContactUsActivity;
import com.aait.mazaqelkhlij.UI.activities.NotificationActivity;
import com.aait.mazaqelkhlij.UI.activities.SettingsActivity;
import com.aait.mazaqelkhlij.UI.activities.SplashActivity;
import com.aait.mazaqelkhlij.UI.activities.TermsAndConditions;
import com.aait.mazaqelkhlij.UI.adapters.NavigationDrawerAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.DialogUtil;
import com.aait.mazaqelkhlij.base.BaseFragment;
import com.aait.mazaqelkhlij.fcm.MyFirebaseInstanceIDService;
import com.aait.mazaqelkhlij.listeners.DrawerListner;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.BaseResponse;
import com.aait.mazaqelkhlij.models.NavigationModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NavigationFragment extends BaseFragment implements OnItemClickListener {

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.lay_profile)
    LinearLayout lay_profile;

    @BindView(R.id.tv_user_mobile)
    TextView tv_user_mobile;

    @BindView(R.id.civ_user_image)
    CircleImageView civ_user_image;


    ArrayList<NavigationModel> mNavigationModels;

    NavigationDrawerAdapter drawerAdapter;

    DrawerListner drawerListner;

    public static NavigationFragment newInstance() {
        Bundle args = new Bundle();
        NavigationFragment fragment = new NavigationFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_navigation_menu_client;
    }

    @Override
    protected void initializeComponents(View view) {
        setNavData();
        setMenuData();
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        if (mSharedPrefManager.getLoginStatus()){
        switch (position) {
            case 0:
                ProfileActivity.startActivity((AppCompatActivity) mContext);
                break;
            case 1:
               NotificationActivity.startActivity((AppCompatActivity) mContext);
                break;
            case 2:
               PreviousOrdersActivity.startActivity((AppCompatActivity) mContext);
                break;
            case 3:
                ComplainsActivity.startActivity((AppCompatActivity) mContext);
                break;
            case 4:
                ContactUsActivity.startActivity((AppCompatActivity) mContext);
                break;
            case 5:
                AboutUsActivity.startActivity((AppCompatActivity) mContext);
                break;
            case 6:
                TermsAndConditions.startActivity((AppCompatActivity) mContext);
                break;
            case 7:
                CommonUtil.ShareApp(mContext);
                break;
            case 8:
                SettingsActivity.startActivity((AppCompatActivity) mContext);
                break;
            case 9:
                DialogUtil.showAlertDialog(mContext, getString(R.string.logout_description),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialogInterface, final int i) {

                                logout();

                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                break;
        }}
        else {
            switch (position) {
                case 0:
                    ContactUsActivity.startActivity((AppCompatActivity)mContext);
                    break;
                case 1:
                    AboutUsActivity.startActivity((AppCompatActivity)mContext);
                    break;
                case 2:
                    TermsAndConditions.startActivity((AppCompatActivity) mContext);
                    break;
                case 3:
                    CommonUtil.ShareApp(mContext);
                    break;
                case 4:
                    ChooseLoginActivity.startActivity((AppCompatActivity) mContext);
                    break;
                case 5:
                    ChooseLoginActivity.startActivity((AppCompatActivity) mContext);
                    break;

            }
        }
    }

    public void setDrawerListner(DrawerListner drawerListner) {
        this.drawerListner = drawerListner;
    }
    public void setMenuData() {
        mNavigationModels = new ArrayList<>();
        if(mSharedPrefManager.getLoginStatus()){
            mNavigationModels
                    .add(new NavigationModel(getString(R.string.profile), R.mipmap.user));
            mNavigationModels.add(new NavigationModel(getString(R.string.Notifications), R.mipmap.alarm));
            mNavigationModels.add(new NavigationModel(getString(R.string.previous_orders), R.mipmap.old_orders));
            mNavigationModels.add(new NavigationModel(getString(R.string.complains), R.mipmap.complaint));
            mNavigationModels.add(new NavigationModel(getString(R.string.call_us), R.mipmap.user));
            mNavigationModels.add(new NavigationModel(getString(R.string.about_app), R.mipmap.about));
            mNavigationModels.add(new NavigationModel(getString(R.string.terms_and_conditions), R.mipmap.terms));
            mNavigationModels.add(new NavigationModel(getString(R.string.share_app), R.mipmap.share));
            mNavigationModels.add(new NavigationModel(getString(R.string.setting), R.mipmap.setting));
            mNavigationModels.add(new NavigationModel(getString(R.string.logout), R.mipmap.sign_out));
    }
        else{
            // mNavigationModels.add(new NavigationModel(getString(R.string.profile), R.mipmap.user));
            //   mNavigationModels.add(new NavigationModel(getString(R.string.Notifications), R.mipmap.alarm));
            //  mNavigationModels.add(new NavigationModel(getString(R.string.previous_orders), R.mipmap.old_orders));
            // mNavigationModels.add(new NavigationModel(getString(R.string.complains), R.mipmap.complaint));
            mNavigationModels.add(new NavigationModel(getString(R.string.call_us), R.mipmap.user));
            mNavigationModels.add(new NavigationModel(getString(R.string.about_app), R.mipmap.about));
            mNavigationModels.add(new NavigationModel(getString(R.string.terms_and_conditions), R.mipmap.terms));
            mNavigationModels.add(new NavigationModel(getString(R.string.share_app), R.mipmap.share));
            mNavigationModels.add(new NavigationModel(getString(R.string.login), R.mipmap.user));
            mNavigationModels.add(new NavigationModel(getString(R.string.register), R.mipmap.user));

        }

        rvRecycle.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        drawerAdapter = new NavigationDrawerAdapter(mContext, mNavigationModels,
                R.layout.recycle_navigation_row_client);
        drawerAdapter.setOnItemClickListener(this);
        rvRecycle.setAdapter(drawerAdapter);
    }
    public void setNavData() {
       if (mSharedPrefManager.getLoginStatus()) {
            tv_user_mobile.setText(mSharedPrefManager.getUserData().getName());
           Glide.with(mContext).load(mSharedPrefManager.getUserData().getAvatar()).asBitmap()
                    .placeholder(R.mipmap.logo).error(R.mipmap.logo).into(civ_user_image);
       } else {
          tv_user_mobile.setVisibility(View.GONE);
           civ_user_image.setVisibility(View.GONE);
        }


    }
    private void logout(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).logout(mSharedPrefManager.getUserData().getUser_id(), MyFirebaseInstanceIDService.getToken(mContext)).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mSharedPrefManager.Logout();
                        SplashActivity.startActivity((AppCompatActivity)mContext);
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }

                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
