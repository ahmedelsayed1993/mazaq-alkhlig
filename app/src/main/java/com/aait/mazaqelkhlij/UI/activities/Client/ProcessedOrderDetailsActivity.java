package com.aait.mazaqelkhlij.UI.activities.Client;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.ContactUsActivity;
import com.aait.mazaqelkhlij.UI.adapters.Client.OrderDetailsAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.DialogUtil;
import com.aait.mazaqelkhlij.Uitls.PermissionUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.OrderDetailsResponse;
import com.aait.mazaqelkhlij.models.OrderModel;
import com.aait.mazaqelkhlij.models.OrderProductModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProcessedOrderDetailsActivity extends ParentActivity {

    @BindView(R.id.iv_user_image)
    CircleImageView ivUserImage;

    @BindView(R.id.tv_order_time)
    TextView tvOrderTime;

    @BindView(R.id.tv_user_name)
    TextView tvUserName;

    @BindView(R.id.tv_city)
    TextView tvCity;

    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.iv_step_one)
    ImageView ivStepOne;

    @BindView(R.id.tv_step_one)
    TextView tvStepOne;

    @BindView(R.id.iv_step_two)
    ImageView ivStepTwo;

    @BindView(R.id.tv_step_two)
    TextView tvStepTwo;

    @BindView(R.id.iv_step_three)
    ImageView ivStepThree;

    @BindView(R.id.tv_step_three)
    TextView tvStepThree;

    @BindView(R.id.iv_step_four)
    ImageView ivStepFour;

    @BindView(R.id.tv_step_four)
    TextView tvStepFour;

    @BindView(R.id.iv_step_five)
    ImageView ivStepFive;

    @BindView(R.id.tv_step_five)
    TextView tvStepFive;

    @BindView(R.id.iv_order_status)
    ImageView ivOrderStatus;
    @BindView(R.id.delegate_lay)
    LinearLayout delegate_lay;
    @BindView(R.id.delegate_name)
    TextView delegate_name;
    @BindView(R.id.delegate_phone)
    TextView delegate_phone;

    @BindView(R.id.btn_process_order_steps)
    Button btn_process_order_steps;


    LinearLayoutManager linearLayoutManager;

    OrderDetailsAdapter mFamilyOrderDetailsAdapter;

    List<OrderProductModel> mFamilyOrderModels = new ArrayList<>();

    int mOrderModel;

    public static void startActivity(AppCompatActivity mAppCompatActivity,int order_id) {
        Intent mIntent = new Intent(mAppCompatActivity, ProcessedOrderDetailsActivity.class);
        mIntent.putExtra(Constant.BundleData.ORDER, order_id);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData() {
        mOrderModel = getIntent().getIntExtra(Constant.BundleData.ORDER,0);
//        setOrderData(mOrderModel);
//        processOrderSteps(mOrderModel.getOrder_status());

    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.order_details));
        getBundleData();

        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mFamilyOrderDetailsAdapter = new OrderDetailsAdapter(mContext, mFamilyOrderModels,
                R.layout.recycle_family_order_details_client);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(mFamilyOrderDetailsAdapter);
//        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
//        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                getDetails();
//
//            }
//        });

        getDetails();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_processed_order_details_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @OnClick(R.id.btn_process_order_steps)
    void processedOrderSteps() {
        DialogUtil.showAlertDialog(mContext, getString(R.string.recived_order_details), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialogInterface, final int i) {
                chage();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

    }

    void setOrderData(OrderModel orderData) {
        Glide.with(mContext).load(orderData.getOrder_provider_avatar()).asBitmap()
                .into(ivUserImage);
        tvUserName.setText(orderData.getOrder_provider_name());
        tvCity.setText(orderData.getOrder_provider_city());
        tvOrderNumber.setText(orderData.getOrder_id() + "");
        tvOrderTime.setText(orderData.getOrder_created_at());
        if (orderData.getOrder_delegate_id()==0){
            delegate_lay.setVisibility(View.GONE);
        }else {
            delegate_lay.setVisibility(View.VISIBLE);
            delegate_name.setText(getString(R.string.delivery)+":  "+orderData.getOrder_delegate_name());
            delegate_phone.setText(orderData.getOrder_delegate_phone());

        }

    }
    @OnClick(R.id.delegate_phone)
    void onDelegate(){
        if (!delegate_phone.getText().toString().equals("")){
            getLocationWithPermission(delegate_phone.getText().toString());
        }else {
            CommonUtil.makeToast(mContext, getString(R.string.no_data));
        }
    }
    public void getLocationWithPermission(String number) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(ProcessedOrderDetailsActivity.this, PermissionUtils.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.CALL_PHONE,
                            Constant.RequestPermission.REQUEST_CALL);
                }
            } else {
                callnumber(number);
            }
        } else {
            callnumber(number);
        }

    }
    void callnumber(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        startActivity(intent);
    }

    void processOrderStep(int status) {
        btn_process_order_steps.setVisibility(View.GONE);
        if (status==3){
            ivStepOne.setVisibility(View.GONE);
            ivStepTwo.setVisibility(View.GONE);
            ivStepThree.setVisibility(View.GONE);
            ivStepFour.setVisibility(View.GONE);
            ivStepFive.setVisibility(View.GONE);
            tvStepOne.setVisibility(View.GONE);
            tvStepTwo.setVisibility(View.GONE);
            tvStepThree.setVisibility(View.GONE);
            tvStepFour.setVisibility(View.GONE);
            tvStepFive.setVisibility(View.GONE);
            ivOrderStatus.setVisibility(View.GONE);
            CommonUtil.makeToast(mContext,getString(R.string.order_rejected));

        }

        if (status >= 2) {
            ivStepTwo.setImageResource(R.mipmap.step_two);
            tvStepTwo.setTextColor(getResources().getColor(R.color.order_step_2_color));
            ivOrderStatus.setImageDrawable(getResources().getDrawable(R.drawable.order_two));
        }
        if (status >= 5) {
            ivStepThree.setImageResource(R.mipmap.step_three);
            tvStepThree.setTextColor(getResources().getColor(R.color.order_step_3_color));
            ivOrderStatus.setImageDrawable(getResources().getDrawable(R.drawable.order_three));

        }
        if (status >= 7) {
            ivStepFour.setImageResource(R.mipmap.step_four);
            tvStepFour.setTextColor(getResources().getColor(R.color.order_step_4_color));
            ivOrderStatus.setImageDrawable(getResources().getDrawable(R.drawable.order_four));

            if (status == 8) {
                btn_process_order_steps.setVisibility(View.VISIBLE);
            }
        }
        if (status >= 8) {
            ivStepFive.setImageResource(R.mipmap.step_five);
            tvStepFive.setTextColor(getResources().getColor(R.color.order_step_5_color));
            ivOrderStatus.setImageDrawable(getResources().getDrawable(R.drawable.order_five));

        }
    }
    private void getDetails(){

        RetroWeb.getClient().create(ServiceApi.class).getDetails(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),mOrderModel).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {

                if (response.isSuccessful())
                {
                    if (response.body().getStatus()==1){
                        setOrderData(response.body().getData());
                        processOrderStep(response.body().getData().getOrder_status());
                        if (response.body().getData().getOrder_products().size()==0){
                            rvRecycle.setVisibility(View.GONE);
                        }else {
                            mFamilyOrderDetailsAdapter.updateAll(response.body().getData().getOrder_products());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();


            }
        });
    }
    private void chage(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeStatus(mSharedPrefManager.getUserData().getUser_id(),mOrderModel,8,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.order_delivered));
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
