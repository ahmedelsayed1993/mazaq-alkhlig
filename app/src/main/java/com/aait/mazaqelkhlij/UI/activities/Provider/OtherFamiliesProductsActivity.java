package com.aait.mazaqelkhlij.UI.activities.Provider;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.Client.OffersAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.ProductModel;
import com.aait.mazaqelkhlij.models.SearchResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtherFamiliesProductsActivity extends ParentActivity implements OnItemClickListener {
    LinearLayoutManager linearLayoutManager;

    OffersAdapter mOffersAdapter;

    List<ProductModel> mFoodModels = new ArrayList<>();

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, OtherFamiliesProductsActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.other_products));
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mOffersAdapter = new OffersAdapter(mContext, mFoodModels);
        mOffersAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setItemAnimator(new DefaultItemAnimator());
        rvRecycle.setAdapter(mOffersAdapter);
        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Search(mSharedPrefManager.getUserData().getUser_id(),0,0,0,0,"","");

            }
        });

        Search(mSharedPrefManager.getUserData().getUser_id(),0,0,0,0,"","");
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_other_families_products;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }
    private void Search(int userId,int catId,int cityId,int shopId,int priceId,String lat,String lng){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).search(mLanguagePrefManager.getAppLanguage(),userId,catId,cityId,priceId,lat,lng,shopId).enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.no_data);
                        }else {
                            mOffersAdapter.updateAll(response.body().getData());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {

    }
}
