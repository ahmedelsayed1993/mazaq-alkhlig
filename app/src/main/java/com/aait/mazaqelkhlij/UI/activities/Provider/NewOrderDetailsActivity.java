package com.aait.mazaqelkhlij.UI.activities.Provider;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.Provider.OrderDetailsAdapter;
import com.aait.mazaqelkhlij.UI.fragments.Provider.ChooseDeliveryDialogFragment;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.OrderHasDelivery;
import com.aait.mazaqelkhlij.models.OrderDetailsResponse;
import com.aait.mazaqelkhlij.models.OrderModel;
import com.aait.mazaqelkhlij.models.OrderProductModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewOrderDetailsActivity extends ParentActivity implements OrderHasDelivery {

    @BindView(R.id.iv_user_image)
    CircleImageView ivUserImage;

    @BindView(R.id.tv_order_time)
    TextView tvOrderTime;

    @BindView(R.id.tv_user_name)
    TextView tvUserName;

    @BindView(R.id.tv_city)
    TextView tvCity;

    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.btn_accept)
    Button btn_accept;
    @BindView(R.id.btn_cancel)
    Button cancel;

    LinearLayoutManager linearLayoutManager;

    OrderDetailsAdapter mFamilyOrderDetailsAdapter;

    List<OrderProductModel> mFamilyOrderModels = new ArrayList<>();

    OrderModel mOrderModel;
    int Order_id;

    public static void startActivity(AppCompatActivity mAppCompatActivity,int order_id) {
        Intent mIntent = new Intent(mAppCompatActivity, NewOrderDetailsActivity.class);
        mIntent.putExtra(Constant.BundleData.ORDER, order_id);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }


    void getBundleData() {

        Order_id = getIntent().getIntExtra(Constant.BundleData.ORDER,0);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.order_details));
        getBundleData();
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mFamilyOrderDetailsAdapter = new OrderDetailsAdapter(mContext, mFamilyOrderModels,
                R.layout.recycle_family_order_details_family);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(mFamilyOrderDetailsAdapter);
        getDetails();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_new_order_details_family;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void orderHasDelivery(boolean hasDelivery) {
        if (hasDelivery) {
//            acceptRejectOrder(Urls.BASE_TOKEN + mSharedPrefManager.getUserData().getJwt_token(),
//                    mLanguagePrefManager.getAppLanguage(),
//                    mOrderModel.getId(),
//                    Constant.acceptRejectOrder.ACCEPT, 1);
        } else {
//            acceptRejectOrder(Urls.BASE_TOKEN + mSharedPrefManager.getUserData().getJwt_token(),
//                    mLanguagePrefManager.getAppLanguage(),
//                    mOrderModel.getId(),
//                    Constant.acceptRejectOrder.ACCEPT, 0);
        }
    }

    @OnClick(R.id.btn_accept)
    void onBtnAcceptClick() {
        acceptOrder();
    }


    @OnClick(R.id.btn_cancel)
    void onBtnCancelClick() {
       rejectOrder();
    }

    void setOrderData(OrderModel orderData) {
        Glide.with(mContext).load(orderData.getOrder_user_avatar()).asBitmap()
                .into(ivUserImage);
        tvUserName.setText(orderData.getOrder_user_name());
        tvCity.setText(orderData.getOrder_user_area());
        tvOrderNumber.setText(orderData.getOrder_id() + "");
        tvOrderTime.setText(orderData.getOrder_created_at() + "");
    }
    private void getDetails(){

        RetroWeb.getClient().create(ServiceApi.class).getDetails(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),Order_id).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {

                if (response.isSuccessful())
                {
                    if (response.body().getStatus()==1){
                        setOrderData(response.body().getData());
                       // processOrderStep(response.body().getData().getOrder_status());
                        if (response.body().getData().getOrder_products().size()==0){
                            rvRecycle.setVisibility(View.GONE);
                        }else {
                            mFamilyOrderDetailsAdapter.updateAll(response.body().getData().getOrder_products());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();


            }
        });
    }
    private void acceptOrder(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeStatus(mSharedPrefManager.getUserData().getUser_id(),Order_id,1,mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.order_accepted));
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void rejectOrder(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).rejectOrder(mSharedPrefManager.getUserData().getUser_id(),Order_id,2,"",mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.order_rejected));
                        MainActivity.startActivity((AppCompatActivity)mContext);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }


}
