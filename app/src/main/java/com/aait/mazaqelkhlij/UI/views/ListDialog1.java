package com.aait.mazaqelkhlij.UI.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.Client.ListDialogsAdapter1;
import com.aait.mazaqelkhlij.UI.fragments.Client.HomeFragment;
import com.aait.mazaqelkhlij.models.ListModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListDialog1 extends Dialog {
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.lay_no_data)
    LinearLayout lay_no_data;

    @BindView(R.id.tv_title)
    TextView tv_title;

    LinearLayoutManager mLinearLayoutManager;

    Context mContext;

    HomeFragment onItemClickListener1;

    // OnItemClickListener onItemClickListener;
    List<ListModel> mCarsList;

    ListDialogsAdapter1 mListAdapter;

    String title;



    public ListDialog1(Context mContext, HomeFragment onItemClickListener, List<ListModel> listModels,
                       String title) {
        super(mContext);
        this.mContext = mContext;
        this.mCarsList = listModels;
        this.onItemClickListener1 = onItemClickListener;
        this.title = title;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom_layout_client);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(false);
        ButterKnife.bind(this);
        initializeComponents();
    }

    private void initializeComponents() {
        tv_title.setText(title);
        mLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(mLinearLayoutManager);
        mListAdapter = new ListDialogsAdapter1(mContext, mCarsList);
        mListAdapter.setOnItemClickListener1(onItemClickListener1);
        rvRecycle.setAdapter(mListAdapter);

        if (mCarsList.size() == 0) {
            lay_no_data.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.iv_close)
    void onCloseClicked() {
        dismiss();
    }
}

