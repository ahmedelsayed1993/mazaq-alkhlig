package com.aait.mazaqelkhlij.UI.adapters.Provider;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aait.mazaqelkhlij.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class RecyclerViewAddPhotosAddAD extends RecyclerView.Adapter<RecyclerViewAddPhotosAddAD.ViewHolder> {

    private List<Uri> photos;
    Context context;

    private LayoutInflater mInflater;
    private RecyclerViewAddPhotosAddAD.ItemClickListener mClickListener;

    // data is passed into the constructor
    public RecyclerViewAddPhotosAddAD(Context context,List<Uri> photos) {

        this.mInflater = LayoutInflater.from(context);
        this.context=context;


        this.photos=photos;

    }

    // inflates the row layout from xml when needed
    @Override
    public RecyclerViewAddPhotosAddAD.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.card_add_photos_add_ad, parent, false);
        return new RecyclerViewAddPhotosAddAD.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(RecyclerViewAddPhotosAddAD.ViewHolder holder, int position) {
        Uri photo = photos.get(position);


        Glide.with(context).load(photo).asBitmap()
                .placeholder(R.mipmap.splash).error(R.mipmap.splash).into(holder.photo);

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return photos.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView photo;

        ViewHolder(View itemView) {
            super(itemView);
            photo=itemView.findViewById(R.id.imageView_card_addAd);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    // String getItem(int id) {return photos.get(id);}

    // allows clicks events to be caught
    public void setClickListener(RecyclerViewAddPhotosAddAD.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void addItem(Uri item) {
        photos.add(item);
        notifyDataSetChanged();
    }
}
