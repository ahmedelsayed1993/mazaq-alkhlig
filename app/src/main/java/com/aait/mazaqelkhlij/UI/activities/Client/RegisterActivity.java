package com.aait.mazaqelkhlij.UI.activities.Client;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.ConfirmCodeActivity;
import com.aait.mazaqelkhlij.UI.activities.MapDetectLocationActivity;
import com.aait.mazaqelkhlij.UI.activities.TermsAndConditions;
import com.aait.mazaqelkhlij.UI.views.ListDialog;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.PermissionUtils;
import com.aait.mazaqelkhlij.Uitls.ProgressRequestBody;
import com.aait.mazaqelkhlij.Uitls.ValidationUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.ListModel;
import com.aait.mazaqelkhlij.models.ListModelResponse;
import com.aait.mazaqelkhlij.models.RegisterResponse;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.mazaqelkhlij.App.Constant.RequestPermission.REQUEST_IMAGES;

public class RegisterActivity extends ParentActivity implements OnItemClickListener,ProgressRequestBody.UploadCallbacks {

    @BindView(R.id.lay_splash)
    LinearLayout laySplash;

    @BindView(R.id.civ_profile_pic)
    CircleImageView civProfilePic;

    @BindView(R.id.til_name)
    TextInputLayout tilName;

    @BindView(R.id.et_name)
    TextInputEditText etName;

    @BindView(R.id.til_password)
    TextInputLayout tilPassword;

    @BindView(R.id.et_password)
    TextInputEditText etPassword;

    @BindView(R.id.til_confirm_password)
    TextInputLayout tilConfirmPassword;

    @BindView(R.id.et_confirm_password)
    TextInputEditText etConfirmPassword;

    @BindView(R.id.til_email)
    TextInputLayout tilEmail;

    @BindView(R.id.et_email)
    TextInputEditText etEmail;

    @BindView(R.id.til_phone)
    TextInputLayout tilPhone;

    @BindView(R.id.et_phone)
    TextInputEditText etPhone;

    @BindView(R.id.til_location)
    TextInputLayout tilLocation;

    @BindView(R.id.et_location)
    TextInputEditText etLocation;

    @BindView(R.id.til_area)
    TextInputLayout tilArea;

    @BindView(R.id.et_area)
    TextInputEditText etArea;

    @BindView(R.id.et_address)
    TextInputEditText et_address;

    @BindView(R.id.til_address)
     TextInputLayout til_address;

    @BindView(R.id.tv_terms_and_conditions)
    TextView tvTermsAndConditions;
    ListDialog mListDialog;

    ArrayList<ListModel> mListModels;
    ArrayList<ListModel> mListModels1;

    ListModel cityModel;
    ListModel areaModel;

    // select image from callery
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;
    int nationalityorCity = 0;

    String mAdresse, mLang, mLat = null;
    String type;
    boolean cityisclicked = false;

    public static void startActivity(AppCompatActivity mAppCompatActivity,String type) {
        Intent mIntent = new Intent(mAppCompatActivity, RegisterActivity.class);
        mIntent.putExtra("type",type);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData(){
        type = (String)getIntent().getSerializableExtra("type");
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    @Override
    protected void initializeComponents() {
       getBundleData();
        etArea.setClickable(false);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        mListDialog.dismiss();
        if (nationalityorCity==0){
            areaModel = mListModels.get(position);
            etLocation.setText(areaModel.getName());
            etArea.setClickable(true);
            cityisclicked = true;
        }else if (nationalityorCity == 1){
            cityModel = mListModels.get(position);
            etArea.setText(cityModel.getName());
        }


        CommonUtil.PrintLogE("City id : " + areaModel.getId());
    }
    @OnClick(R.id.et_area)
    void onCityClick() {
        if (cityisclicked) {
            nationalityorCity = 1;
            getCities(areaModel.getId() + "");
        }
        else {
            CommonUtil.makeToast(mContext,getString(R.string.choose_city));
        }

    }

    @OnClick(R.id.et_location)
    void onAreaClick(){
        nationalityorCity = 0;
        getAreas();


    }

    @OnClick(R.id.btn_register)
    void onBtnRegisterClick() {
        if (registerValidation()) {
            if (ImageBasePath!=null){
                Register(mLanguagePrefManager.getAppLanguage(),type,etName.getText().toString(),etEmail.getText().toString(),
                        etPhone.getText().toString(),mLat,mLang,cityModel.getId()+"",etPassword.getText().toString(),ImageBasePath
                );
            }

        }
        //ConfirmCodeActivity.startActivity((AppCompatActivity)mContext,type);
    }

    @OnClick(R.id.civ_profile_pic)
    void onImageClick() {
        getPickImageWithPermission();

    }
    @OnClick(R.id.et_address)
    void onAddressClick(){
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }

    @OnClick(R.id.tv_terms_and_conditions)
    void onTermsAndConditionClick() {
        //TODO implement
        TermsAndConditions.startActivity((AppCompatActivity) mContext);
    }
    boolean registerValidation() {
        if (!ValidationUtils.checkError(etName, tilName, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etPhone, tilPhone, getString(R.string.fill_empty))) {
            return false;
        }else if(!ValidationUtils.ckeckPhoneNumber(etPhone,tilPhone,getString(R.string.should_with05))){
            return false;
        }
        else if (!ValidationUtils.checkError(etEmail, tilEmail, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.validateEmail(etEmail, tilEmail)) {
            return false;
        }
        else if (!ValidationUtils.checkError(et_address,til_address,getString(R.string.fill_empty))){
            return false;
        }
        else if (!ValidationUtils.checkError(etLocation, tilLocation, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etPassword, tilPassword, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils
                .checkMatch(etPassword, etConfirmPassword, tilPassword, getString(R.string.fill_empty))) {
            return false;
        }else if (ImageBasePath==null){

            CommonUtil.makeToast(mContext,getString(R.string.choose_your_avatar));
            return false;
        }
        return true;

    }

    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        civProfilePic.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }

    private void getCities(String id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCities(mLanguagePrefManager.getAppLanguage(),id).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().issucessfull()){
                        mListModels = response.body().getData();
                        mListDialog = new ListDialog(mContext,RegisterActivity.this
                                ,mListModels,getString(R.string.city));
                        mListDialog.show();
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }

   private void getAreas(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAreas(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().issucessfull()){
                        mListModels = response.body().getData();
                        mListDialog = new ListDialog(mContext,RegisterActivity.this,
                                mListModels,getString(R.string.area));
                        mListDialog.show();
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }
    private void Register(String lang,String type,String name,String email,String phone,
                          String lat,String lng,String City_id,String password ,String pathFromImage){
        showProgressDialog(getResources().getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(pathFromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, RegisterActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).clientRegister(lang,type,name,password,email,phone,City_id,lat,lng,filePart)
                .enqueue(new Callback<RegisterResponse>() {
                    @Override
                    public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()){
                            if (response.body().issucessfull()){
                                CommonUtil.makeToast(mContext,getString(R.string.enter_confirmation_code));
                                ConfirmCodeActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getUser_id()+"");
                            }else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        hideProgressDialog();
                    }
                });

    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse);
                    et_address.setText(mAdresse);
                }
            }
        }
    }

}
