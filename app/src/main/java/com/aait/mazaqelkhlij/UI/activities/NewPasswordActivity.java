package com.aait.mazaqelkhlij.UI.activities;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.ValidationUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.BaseResponse;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewPasswordActivity extends ParentActivity {

    @BindView(R.id.lay_splash)
    LinearLayout laySplash;

    @BindView(R.id.til_password)
    TextInputLayout tilPassword;

    @BindView(R.id.et_password)
    TextInputEditText etPassword;

    @BindView(R.id.til_confirm_password)
    TextInputLayout tilConfirmPassword;

    @BindView(R.id.et_confirm_password)
    TextInputEditText etConfirmPassword;

    String mForgetPassModel;
    String type;

    public static void startActivity(AppCompatActivity mAppCompatActivity, String forgetPassModel,String type) {
        Intent mIntent = new Intent(mAppCompatActivity, NewPasswordActivity.class);
        mIntent.putExtra(Constant.BundleData.FORGET_PASS_MODEL, forgetPassModel);
        mIntent.putExtra("type",type);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    void getBundleData() {
        mForgetPassModel = (String) getIntent().getSerializableExtra(Constant.BundleData.FORGET_PASS_MODEL);
        type = (String) getIntent().getSerializableExtra("type");
    }
    @Override
    protected void initializeComponents() {
        getBundleData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_new_password_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.btn_send_new_pass)
    void onBtnSendNewPassClick() {
        if (newPasswordValidation()) {
            NewPass();
        }
    }

    boolean newPasswordValidation() {
        if (!ValidationUtils.checkError(etPassword, tilPassword, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils
                .checkMatch(etPassword, etConfirmPassword, tilPassword, getString(R.string.fill_empty))) {
            return false;
        }
        return true;

    }

    private void NewPass(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).updatePass(mForgetPassModel,etPassword.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        com.aait.mazaqelkhlij.UI.activities.ChooseLoginActivity.startActivity((AppCompatActivity)mContext);
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
