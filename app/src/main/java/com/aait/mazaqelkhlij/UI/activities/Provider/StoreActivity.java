package com.aait.mazaqelkhlij.UI.activities.Provider;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.Client.OffersAdapter;
import com.aait.mazaqelkhlij.UI.adapters.Provider.FamilyCategoriesAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.CategoriesModel;
import com.aait.mazaqelkhlij.models.CategoryModel;
import com.aait.mazaqelkhlij.models.ListModel;
import com.aait.mazaqelkhlij.models.ProductModel;
import com.aait.mazaqelkhlij.models.ProductsResponse;
import com.aait.mazaqelkhlij.models.ProductsResponses;
import com.aait.mazaqelkhlij.models.ShowShopResponse;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreActivity extends ParentActivity implements OnItemClickListener {

    @BindView(R.id.tv_family_name)
    TextView tvFamilyName;

    @BindView(R.id.tv_city)
    TextView tvCity;

    @BindView(R.id.rb_rating)
    AppCompatRatingBar rbRating;

    @BindView(R.id.rv_recycle_categores)
    RecyclerView rvRecycleCategores;

    @BindView(R.id.iv_next)
    ImageView ivNext;

    @BindView(R.id.civ_image)
    CircleImageView civ_image;

    @BindView(R.id.iv_background)
    ImageView Background;

    @BindView(R.id.btn_rate)
    Button btnRate;
    @BindView(R.id.rv_recycle_products)
    RecyclerView rv_recycle_products;

    LinearLayoutManager linearLayoutManager;

    FamilyCategoriesAdapter mFamilyCategoriesAdapter;

    List<CategoriesModel> mCategoryModels = new ArrayList<>();
    ListModel categorylist;


    LinearLayoutManager linearLayoutManagerVertical;

    OffersAdapter mOffersAdapter;

    List<ProductModel> mFoodModels = new ArrayList<>();


    int selectedPosition;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, StoreActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.family_name));
        getShopData();
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvRecycleCategores.setLayoutManager(linearLayoutManager);
        mFamilyCategoriesAdapter = new FamilyCategoriesAdapter(mContext, mCategoryModels,
                R.layout.recycle_family_categories_row_family);
        mFamilyCategoriesAdapter.setOnItemClickListener(this);
        rvRecycleCategores.setAdapter(mFamilyCategoriesAdapter);

        linearLayoutManagerVertical = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rv_recycle_products.setLayoutManager(linearLayoutManagerVertical);
        mOffersAdapter = new OffersAdapter(mContext, mFoodModels);
        mOffersAdapter.setOnItemClickListener(this);
        rv_recycle_products.setAdapter(mOffersAdapter);


//        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
//        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                if (mCategoryModels.size() != 0) {
//                   // getProductsByCategory( mCategoryModels.get(0).getCategory_id());
//                }
//            }
//        });
        if (mCategoryModels.size() != 0) { getProductCategories( mCategoryModels.get(0).getCategory_id());
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_family_store_details_family;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }
    @OnClick(R.id.btn_rate)
    void onRateClick() {
        RateActivity.startActivity((AppCompatActivity) mContext);
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId() == R.id.tv_categories) {
            // click on categories section
            CommonUtil.PrintLogE("categories click");
            selectedPosition = position;
            getProductCategories(mCategoryModels.get(position).getCategory_id());
        }
        else {
            ProductDetailsActivity.startActivity((AppCompatActivity)mContext,mFoodModels.get(position).getProduct_id());
            Log.e("ttt",new Gson().toJson(mFoodModels.get(position).getProduct_id()));
        }
    }


    private void getProductCategories(  int categoryId) {

        RetroWeb.getClient().create(ServiceApi.class).getProducts( mSharedPrefManager.getUserData().getUser_id(),mSharedPrefManager.getUserData().getShop().getShop_id(), categoryId, mLanguagePrefManager.getAppLanguage())
                .enqueue(new Callback<ProductsResponses>() {
                    @Override
                    public void onResponse(Call<ProductsResponses> call,
                                           Response<ProductsResponses> response) {
                        Log.e("product",new Gson().toJson(response.body().getData()));

                        if (response.isSuccessful()) {
                            if (response.body().getData().getProducts().size() == 0) {
                                rv_recycle_products.setVisibility(View.GONE);
                            } else {
                                mOffersAdapter.updateAll(response.body().getData().getProducts());
                                mFoodModels = response.body().getData().getProducts();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductsResponses> call, Throwable t) {
                        Log.e("error",new Gson().toJson(t));
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();

                    }
                });
    }
    private void getShopData(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).showShop(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),mSharedPrefManager.getUserData().getShop().getShop_id()).
                enqueue(new Callback<ShowShopResponse>() {
                    @Override
                    public void onResponse(Call<ShowShopResponse> call, Response<ShowShopResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()){
                            if (response.body().getStatus()==1){
                                tvCity.setText(response.body().getData().getUser_city());
                                tvFamilyName.setText(response.body().getData().getShop_name());
                                rbRating.setRating(response.body().getData().getShop_rate());
                                Glide.with(mContext).load(response.body().getData().getUser_image()).asBitmap().placeholder(R.mipmap.logo).into(civ_image);
                                Glide.with(mContext).load(response.body().getData().getUser_image()).asBitmap().placeholder(R.mipmap.logo).into(Background);
                                mCategoryModels = response.body().getData().getUser_categories();
                                mFamilyCategoriesAdapter.updateAll(response.body().getData().getUser_categories());
                                mOffersAdapter.updateAll(response.body().getData().getProducts());
                                mFoodModels = response.body().getData().getProducts();
                            }
                            else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ShowShopResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext,t);
                        t.printStackTrace();
                        hideProgressDialog();

                    }
                });
    }
}
