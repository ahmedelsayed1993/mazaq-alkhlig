package com.aait.mazaqelkhlij.UI.activities.Client;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.Client.FamilyOrderDetailsAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.listeners.OnItemClickListener;
import com.aait.mazaqelkhlij.models.CartModel;
import com.aait.mazaqelkhlij.models.CartProductsModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class FamilyOrdersActivty extends ParentActivity implements OnItemClickListener {


    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.civ_family_image)
    CircleImageView civFamilyImage;

    @BindView(R.id.tv_family_name)
    TextView tvFamilyName;

    @BindView(R.id.lay_header)
    LinearLayout lay_header;

    LinearLayoutManager linearLayoutManager;

    FamilyOrderDetailsAdapter mFamilyOrderDetailsAdapter;

    List<CartProductsModel> mFamilyOrderModels = new ArrayList<>();
    CartModel cartModel;


    private int providerID;



    public static void startActivity(AppCompatActivity mAppCompatActivity, CartModel cartModel) {
        Intent mIntent = new Intent(mAppCompatActivity, FamilyOrdersActivty.class);
        mIntent.putExtra(Constant.BundleData.FAMILY_ID, cartModel);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    public void getBundleData() {
        cartModel = (CartModel) getIntent().getSerializableExtra(Constant.BundleData.FAMILY_ID);
        Log.e("cart",cartModel.getCart_products().toArray().toString());
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.family_orders));
        getBundleData();
        lay_header.setVisibility(View.VISIBLE);
        Glide.with(mContext).load(cartModel.getProvider_image()).asBitmap()
                .into(civFamilyImage);
        tvFamilyName.setText(cartModel.getProvider_name());
        mFamilyOrderModels = cartModel.getCart_products();
//        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
//        mFamilyOrderDetailsAdapter = new FamilyOrderDetailsAdapter(mContext, mFamilyOrderModels,
//                R.layout.recycle_family_order_details_client);
        mFamilyOrderDetailsAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(mFamilyOrderDetailsAdapter);
       // mFamilyOrderDetailsAdapter.updateAll(mFamilyOrderModels);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_family_orders_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    protected void onResume() {
        super.onResume();
//        getFamilyOrders(mLanguagePrefManager.getAppLanguage(),
//                Urls.BASE_TOKEN + mSharedPrefManager.getUserData().getJwt_token(), providerID);
        mFamilyOrderDetailsAdapter.updateAll(mFamilyOrderModels);
    }
}
