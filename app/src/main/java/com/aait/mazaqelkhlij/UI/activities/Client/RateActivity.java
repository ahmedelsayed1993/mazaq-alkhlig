package com.aait.mazaqelkhlij.UI.activities.Client;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.EditText;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.Client.RateAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.ValidationUtils;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.BaseResponse;
import com.aait.mazaqelkhlij.models.CommentsModel;
import com.aait.mazaqelkhlij.models.CommentsResponse;
import com.aait.mazaqelkhlij.models.RatesResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RateActivity extends ParentActivity {


    @BindView(R.id.rating_bar_rate)
    AppCompatRatingBar ratingBarRate;

    @BindView(R.id.ed_add_rate)
    EditText edAddRate;


    LinearLayoutManager linearLayoutManager;

    RateAdapter mRateAdapter;

    List<CommentsModel> mRateModels = new ArrayList<>();

    int familyID;

    public static void startActivity(AppCompatActivity mAppCompatActivity, int providerID) {
        Intent mIntent = new Intent(mAppCompatActivity, RateActivity.class);
        mIntent.putExtra(Constant.BundleData.FAMILY_ID, providerID);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData() {
        familyID = getIntent().getIntExtra(Constant.BundleData.FAMILY_ID, 0);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.rating));
        getBundleData();
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mRateAdapter = new RateAdapter(mContext, mRateModels, R.layout.recycle_rate_row_client);
        rvRecycle.setAdapter(mRateAdapter);

        if (mSharedPrefManager.getLoginStatus()) {
            swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                   ShowComments();

                }
            });

            ShowComments();
        }else {

        }
    }

    @Override
    protected int getLayoutResource() {
        return  R.layout.activity_rate_client;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }
    @OnClick(R.id.btn_rate)
    void onBtnRateClick() {
        if (rateValidation()) {
            DoRate();
        }
    }

    boolean rateValidation() {
        if (!ValidationUtils.emptyValidation(edAddRate, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }
    private void DoRate(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).DoRate(mSharedPrefManager.getUserData().getUser_id(),familyID,(int) ratingBarRate.getRating(),edAddRate.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                CommonUtil.PrintLogE(ratingBarRate.getRating()+"");
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        edAddRate.setText("");
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void ShowComments(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getComments(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),familyID).enqueue(new Callback<RatesResponse>() {
            @Override
            public void onResponse(Call<RatesResponse> call, Response<RatesResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().getUser_rate_before().equals("1")){
                            ratingBarRate.setRating(Float.parseFloat(response.body().getData().getUser_rate()));
                        }
                        if (response.body().getData().getComments().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.no_data);
                        }
                        else {
                            mRateAdapter.updateAll(response.body().getData().getComments());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<RatesResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });
    }
}
