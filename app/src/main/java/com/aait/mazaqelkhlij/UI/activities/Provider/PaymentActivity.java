package com.aait.mazaqelkhlij.UI.activities.Provider;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Network.RetroWeb;
import com.aait.mazaqelkhlij.Network.ServiceApi;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.adapters.Provider.AccountsAdapter;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.PermissionUtils;
import com.aait.mazaqelkhlij.Uitls.ProgressRequestBody;
import com.aait.mazaqelkhlij.base.ParentActivity;
import com.aait.mazaqelkhlij.models.BanksModel;
import com.aait.mazaqelkhlij.models.BanksResponse;
import com.aait.mazaqelkhlij.models.BaseResponse;
import com.aait.mazaqelkhlij.models.DeptResponse;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.mazaqelkhlij.App.Constant.RequestPermission.REQUEST_IMAGES;

public class PaymentActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks {

    @BindView(R.id.amount_txt)
    TextView AmountTxt;

    LinearLayoutManager linearLayoutManager;

    AccountsAdapter mAccountAdapter;

    List<BanksModel> mAccountModel = new ArrayList<>();

    @BindView(R.id.accounts_recycler)
    RecyclerView accounts_recycler;
    @BindView(R.id.ed_bank_name)
    EditText edBankName;
    @BindView(R.id.ed_Name_of_the_transferring_bank)
    EditText transferingBank;
    @BindView(R.id.ed_Account_Holder_Name)
    EditText edAccountHolderName;

    @BindView(R.id.ed_account_number)
    EditText edAccountNumber;
    @BindView(R.id.ed_iban)
    EditText edIban;

    @BindView(R.id.ed_Amount_desired_to_be_paid)
    EditText edAmount;
    @BindView(R.id.send)
    Button Send;
    @BindView(R.id.image)
    CircleImageView image;
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, PaymentActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.commissions));
        getDept();
        //AmountTxt.setText(mSharedPrefManager.getUserData().getDebt_money());
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false);
        mAccountAdapter = new AccountsAdapter(mContext,mAccountModel,R.layout.recycler_accounts);
        accounts_recycler.setLayoutManager(linearLayoutManager);
        accounts_recycler.setAdapter(mAccountAdapter);
        getBanks();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_payments;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @OnClick(R.id.send)
    void onSendClick(){
        if (edBankName.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.fill_empty));
        }else {
            if (edIban.getText().toString().equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.fill_empty));
            }
            else {
                if (edAccountHolderName.getText().toString().equals("")){
                    CommonUtil.makeToast(mContext,getString(R.string.fill_empty));
                }
                else {
                    if (edAccountNumber.getText().toString().equals("")){
                        CommonUtil.makeToast(mContext,getString(R.string.fill_empty));
                    }
                    else {
                        if (edAmount.getText().toString().equals("")){
                            CommonUtil.makeToast(mContext,getString(R.string.fill_empty));
                        }
                        else {if (ImageBasePath==null){
                            CommonUtil.makeToast(mContext,getString(R.string.choose_image));
                        }else {
                            Transfer(ImageBasePath);
                        }
//                            transferMoney(Urls.BASE_TOKEN + mSharedPrefManager.getUserData().getJwt_token(),
//                                    edBankName.getText().toString(),transferingBank.getText().toString(),
//                                    edAccountHolderName.getText().toString(),edAccountNumber.getText().toString(),
//                                    Double.parseDouble(edAmount.getText().toString()));
                        }
                    }
                }
            }
        }
    }
    @OnClick(R.id.image)
    void onImageClick(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }


    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        image.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }
    private void getBanks(){
        RetroWeb.getClient().create(ServiceApi.class).getBanks(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<BanksResponse>() {
            @Override
            public void onResponse(Call<BanksResponse> call, Response<BanksResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().size()==0){
                            accounts_recycler.setVisibility(View.GONE);
                        }else {
                            mAccountAdapter.updateAll(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }

                }
            }

            @Override
            public void onFailure(Call<BanksResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();

            }
        });
    }
    private void getDept(){
        RetroWeb.getClient().create(ServiceApi.class).getDept(mSharedPrefManager.getUserData().getUser_id()).enqueue(new Callback<DeptResponse>() {
            @Override
            public void onResponse(Call<DeptResponse> call, Response<DeptResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        AmountTxt.setText(response.body().getData());
                    }else {
                        AmountTxt.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<DeptResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();

            }
        });
    }
    private void Transfer(String PathFromImage){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(PathFromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, PaymentActivity.this);
        filePart = MultipartBody.Part.createFormData("image", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).transfer(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),2,edBankName.getText().toString(),edAccountHolderName.getText().toString(),edAccountNumber.getText().toString(),edIban.getText().toString(),edAmount.getText().toString(),filePart).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
