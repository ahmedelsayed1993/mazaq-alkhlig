package com.aait.mazaqelkhlij.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.Pereferences.LanguagePrefManager;
import com.aait.mazaqelkhlij.Pereferences.SharedPrefManager;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.activities.NotificationActivity;
import com.aait.mazaqelkhlij.Uitls.AudioPlayer;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Messaging";

    String notificationTitle;

    String notificationMessage;

    String notificationData;

    SharedPrefManager mSharedPrefManager;

    String notificationType;

   // OrderModel mOrderModel;

    LanguagePrefManager mLanguagePrefManager;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        CommonUtil.onPrintLog(remoteMessage.getNotification());
        CommonUtil.onPrintLog(remoteMessage.getData());
        mSharedPrefManager = new SharedPrefManager(getApplicationContext());
        mLanguagePrefManager = new LanguagePrefManager(getApplicationContext());

        if (!mSharedPrefManager.getNotificationStatus()){
            return;
        }

        AudioPlayer audioPlayer = new AudioPlayer();
        audioPlayer.play(getApplicationContext(), R.raw.notification);

//        notificationType = remoteMessage.getData().get("type");
        notificationTitle = remoteMessage.getData().get("title");
        notificationMessage = remoteMessage.getData().get("description");
        notificationType = remoteMessage.getData().get("notification_type");
        // if the notification contains data payload
        if (remoteMessage == null) {
            return;
        }

        // if the user not logged in never do any thing
        if (!mSharedPrefManager.getLoginStatus()) {
            return;
        } else {
            notificationData = remoteMessage.getData().get("msg");
            CommonUtil.PrintLogE("Data : " + notificationData);
            Intent intent = new Intent(this, NotificationActivity.class);
            intent.putExtra("notification", "notification");
            showNotification(remoteMessage, intent);


        }
    }
    private void showNotification(RemoteMessage message, Intent intent) {

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder myNotification = new NotificationCompat.Builder(this)
                .setContentTitle(message.getData().get("title"))
                .setContentText(message.getData().get("msg"))
                .setTicker("Notification!")
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setSmallIcon(R.mipmap.logo);
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("10001", "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.DKGRAY);
            notificationChannel.setShowBadge(true);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            myNotification.setChannelId("10001");
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notificationManager.notify(0 /* Request Code */, myNotification.build());
    }

    /**
     * Processing user specific push message
     * It will be displayed with / without image in push notification tray
     */

    private void handleNotification(String notificationTitle, String notificationMessage, Intent mIntent) {
        AudioPlayer audioPlayer = new AudioPlayer();
        audioPlayer.play(getApplicationContext(), R.raw.notification);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils
                .showNotificationMessage(notificationTitle, notificationMessage, System.currentTimeMillis(), mIntent);
    }
}
