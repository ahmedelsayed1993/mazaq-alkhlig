package com.aait.mazaqelkhlij.Network;

import com.aait.mazaqelkhlij.models.BanksResponse;
import com.aait.mazaqelkhlij.models.BaseResponse;
import com.aait.mazaqelkhlij.models.CartResponse;
import com.aait.mazaqelkhlij.models.CategoriesResponse;
import com.aait.mazaqelkhlij.models.ChangeOrderStatusResponse;
import com.aait.mazaqelkhlij.models.CommentsResponse;
import com.aait.mazaqelkhlij.models.ConfirmCodeModel;
import com.aait.mazaqelkhlij.models.ContactUsResponse;
import com.aait.mazaqelkhlij.models.DelegatesResponse;
import com.aait.mazaqelkhlij.models.DeptResponse;
import com.aait.mazaqelkhlij.models.ForgotPasswordResponse;
import com.aait.mazaqelkhlij.models.ListModel;
import com.aait.mazaqelkhlij.models.ListModelResponse;
import com.aait.mazaqelkhlij.models.LoginResponse;
import com.aait.mazaqelkhlij.models.NewPasswordResponse;
import com.aait.mazaqelkhlij.models.NotificationModel;
import com.aait.mazaqelkhlij.models.NotificationResponse;
import com.aait.mazaqelkhlij.models.OrderDetailsResponse;
import com.aait.mazaqelkhlij.models.OrderResponse;
import com.aait.mazaqelkhlij.models.ProductDetailsResponse;
import com.aait.mazaqelkhlij.models.ProductResponse;
import com.aait.mazaqelkhlij.models.ProductsResponse;
import com.aait.mazaqelkhlij.models.ProductsResponses;
import com.aait.mazaqelkhlij.models.RatesResponse;
import com.aait.mazaqelkhlij.models.RegisterResponse;
import com.aait.mazaqelkhlij.models.SearchResponse;
import com.aait.mazaqelkhlij.models.ShopRateResponse;
import com.aait.mazaqelkhlij.models.ShopsResponse;
import com.aait.mazaqelkhlij.models.ShowProductResponse;
import com.aait.mazaqelkhlij.models.ShowShopResponse;
import com.aait.mazaqelkhlij.models.TermsResponse;
import com.aait.mazaqelkhlij.models.UserModel;

import java.util.List;

import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ServiceApi {

    @GET(Urls.Areas)
    Call<ListModelResponse> getAreas(
            @Query("lang") String lang
    );
    @POST(Urls.Cities)
    Call<ListModelResponse> getCities(
            @Query("lang") String lang,
            @Query("id") String id
    );
    @GET(Urls.Nations)
    Call<ListModelResponse> getNations(
            @Query("lang") String lang
    );
    @GET(Urls.Categories)
    Call<ListModelResponse> getCategories(
            @Query("lang") String lang
    );
    @GET(Urls.Categories)
    Call<CategoriesResponse> getCategory(
            @Query("lang") String lang
    );
    @GET(Urls.Terms)
    Call<TermsResponse> getTerms(
            @Query("lang") String lang
    );
    @GET(Urls.About)
    Call<TermsResponse> getAbout(
            @Query("lang") String lang
    );
    @GET(Urls.AppInfo)
    Call<ContactUsResponse> getAppInfo(
            @Query("lang") String lang
    );
    @Multipart
    @POST(Urls.Register)
    Call<RegisterResponse>clientRegister(
            @Query("lang") String lang,
            @Query("type") String type,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Part MultipartBody.Part avatar
    );

    @Multipart
    @POST(Urls.Register)
    Call<RegisterResponse>providerRegister(
            @Query("lang") String lang,
            @Query("type") String type,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") String nationality_id,
            @Query("civil_number") String civil_number,
            @Query("shop_name") String shop_name,
            @Query("categories_ids") String categories_ids,
            @Part MultipartBody.Part avatar
    );
    @Multipart
    @POST(Urls.Register)
    Call<RegisterResponse>delegateRegister(
            @Query("lang") String lang,
            @Query("type") String type,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") String nationality_id,
            @Query("civil_number") String civil_number,
            @Part MultipartBody.Part avatar
    );
    @POST(Urls.ConfirmCode)
    Call<ConfirmCodeModel> confirm(
            @Query("user_id") String user_id,
            @Query("code") String code
    );
    @POST(Urls.Login)
    Call<LoginResponse> Login(
            @Query("lang") String lang,
            @Query("phone") String phone,
            @Query("password") String password,
            @Query("device") String device,
            @Query("device_id") String device_id
    );
    @Multipart
    @POST(Urls.Update)
    Call<LoginResponse> clientUpdate(
            @Query("lang") String lang,
            @Query("user_id") String user_id,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") String nationality_id,
            @Query("civil_number") String civil_number,
            @Part MultipartBody.Part avatar
    );
    @Multipart
    @POST(Urls.Update)
    Call<LoginResponse> clientUpdatewithoutPass(
            @Query("lang") String lang,
            @Query("user_id") String user_id,
            @Query("name") String name,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") String nationality_id,
            @Query("civil_number") String civil_number,
            @Part MultipartBody.Part avatar
    );


    @POST(Urls.Update)
    Call<LoginResponse> clientUpdatewithoutImage(
            @Query("lang") String lang,
            @Query("user_id") String user_id,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") String nationality_id,
            @Query("civil_number") String civil_number

    );

    @POST(Urls.Update)
    Call<LoginResponse> clientUpdatewithoutPasswithoutImage(
            @Query("lang") String lang,
            @Query("user_id") String user_id,
            @Query("name") String name,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") String nationality_id,
            @Query("civil_number") String civil_number

    );
    @POST(Urls.Update)
    Call<LoginResponse> update(
            @Query("lang") String lang,
            @Query("user_id") String user_id,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") String nationality_id,
            @Query("civil_number") String civil_number
    );
    @Multipart
    @POST(Urls.Update)
    Call<LoginResponse> updateWithImage(
            @Query("lang") String lang,
            @Query("user_id") String user_id,
            @Query("name") String name,
            @Query("password") String password,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("nationality_id") String nationality_id,
            @Query("civil_number") String civil_number,
            @Part MultipartBody.Part avatar
    );
    @POST(Urls.ForgetPass)
    Call<ForgotPasswordResponse> forgetPass(
            @Query("email") String email
    );
    @POST(Urls.ForgetPassword)
    Call<NewPasswordResponse> forgetPassword(
            @Query("user_id") String user_id,
            @Query("code") String code
    );
    @POST(Urls.UpdatePassword)
    Call<BaseResponse> updatePass(
            @Query("user_id") String user_id,
            @Query("password") String password
    );
    @POST(Urls.Logout)
    Call<BaseResponse> logout(
            @Query("user_id") int user_id,
            @Query("device_id") String device_id
    );
    @POST(Urls.Complain)
    Call<BaseResponse> sendComplain(
            @Query("user_id") String user_id,
            @Query("lang") String lang,
            @Query("title") String title,
            @Query("body") String body
    );
    @POST(Urls.UpdateShop)
    Call<LoginResponse> updateShop(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("shop_name") String shop_name,
            @Query("categories_ids") String categories_ids
    );
    @Multipart
    @POST(Urls.StoreProduct)
    Call<ProductResponse> addProduct(
            @Query("user_id") int user_id,
            @Query("name_ar") String name_ar,
            @Query("name_en") String name_en,
            @Query("disc") String disc,
            @Query("price") String price,
            @Query("category_id") String category_id,
            @Part List<MultipartBody.Part>images
            );

    @POST(Urls.SearchProduct)
    Call<SearchResponse> search(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("category_id") int category_id,
            @Query("city_id") int city_id,
            @Query("price_list_id") int price_list_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("shop_id") int shop_id
    );
    @GET(Urls.PriceList)
    Call<ListModelResponse> getPriceList(
            @Query("lang") String lang
    );
    @POST(Urls.SearchShop)
    Call<ShopsResponse> getShops(
            @Query("user_id") int user_id,
            @Query("keyword") String keyword
    );
    @POST(Urls.ShowProduct)
    Call<ProductDetailsResponse> showproduct(
            @Query("user_id") int user_id,
            @Query("product_id") int product_id
    );
    @POST(Urls.AddToCart)
    Call<BaseResponse> addtocart(
            @Query("user_id") int user_id,
            @Query("product_id") int product_id,
            @Query("product_count") int product_count
    );
    @POST(Urls.ShowCart)
    Call<CartResponse> showCart(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.AddOrder)
    Call<BaseResponse> doOrder(
            @Query("user_id") int user_id
    );
    @POST(Urls.ClientOrders)
    Call<OrderResponse> getClientOrders(
            @Query("user_id") int user_id,
            @Query("lang") String lang
    );
    @GET(Urls.Offers)
    Call<SearchResponse> getOffers(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.ShowShop)
    Call<ShowShopResponse> showShop(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("shop_id") int shop_id
    );
    @POST(Urls.DeleteCartProduct)
    Call<BaseResponse> deleteCart(
            @Query("user_id") int user_id,
            @Query("cart_product_id") int cart_product_id
    );
    @POST(Urls.UpdateCatr)
    Call<BaseResponse> updateCart(
            @Query("user_id") int user_id,
            @Query("cart_product_id") int cart_product_id,
            @Query("count") int count
    );
    @POST(Urls.OrderDetails)
    Call<OrderDetailsResponse> getDetails(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("order_id") int order_id
    );
    @POST(Urls.RateShop)
    Call<BaseResponse> DoRate(
            @Query("user_id") int user_id,
            @Query("shop_id") int shop_id,
            @Query("stars") int stars,
            @Query("comment") String comment
    );
    @POST(Urls.GetComments)
    Call<RatesResponse> getComments(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("shop_id") int shop_id
    );
    @POST(Urls.FinshedOrders)
    Call<OrderResponse> getFinshed(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.ShowNotification)
    Call<NotificationResponse> showNotification(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.DelegateNewOrders)
    Call<OrderResponse> getDelegateNewOrders(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.ChangeOrderStatus)
    Call<ChangeOrderStatusResponse> changeOrder(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("order_id") int order_id,
            @Query("status") int status
    );
    @POST(Urls.DelegateCurrentOrders)
    Call<OrderResponse> getDelegateCurrentOrders(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.DelegateOnTheWayOrders)
    Call<OrderResponse> getOnTheWayOrders(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.AllDelegates)
    Call<DelegatesResponse> getAllDelegates(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.SendToDelegate)
    Call<BaseResponse> sendToDelegate(
            @Query("user_id") int user_id,
            @Query("delegate_id") int delegate_id,
            @Query("lang") String lang
    );
    @POST(Urls.ProviderNewOrders)
    Call<OrderResponse> getNewOrders(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.ProviderCurrentOrders)
    Call<OrderResponse> getCurrentorders(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.AcceptOrder)
    Call<OrderDetailsResponse> changeStatus(
            @Query("user_id") int user_id,
            @Query("order_id") int order_id,
            @Query("status") int status,
            @Query("lang") String lang
    );
    @POST(Urls.AcceptOrder)
    Call<OrderDetailsResponse> rejectOrder(
            @Query("user_id") int user_id,
            @Query("order_id") int order_id,
            @Query("status") int status,
            @Query("reason") String reason,
            @Query("lang") String lang
    );
    @POST(Urls.ShopRates)
    Call<ShopRateResponse> getRates(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.ProductsByCategory)
    Call<ProductsResponses> getProducts(
            @Query("user_id") int user_id,
            @Query("shop_id") int shop_id,
            @Query("category_id") int category_id,
            @Query("lang") String lang
    );
    @POST(Urls.AddOffer)
    Call<ProductResponse> addOffer(
            @Query("user_id") int user_id,
            @Query("product_id") int product_id,
            @Query("offer_price") String offer_price,
            @Query("lang") String lang
    );
    @POST(Urls.DeleteProduct)
    Call<BaseResponse> deleteProduct(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("product_id") int product_id
    );
    @GET(Urls.Banks)
    Call<BanksResponse> getBanks(@Query("lang") String lang);
    @POST(Urls.GetProduct)
    Call<ShowProductResponse> getProduct(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("product_id") int product_id
    );
    @POST(Urls.ProviderRequests)
    Call<DelegatesResponse> getProviders(
            @Query("lang") String lang,
            @Query("user_id") int user_id
    );
    @POST(Urls.AcceptBook)
    Call<BaseResponse> book(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("provider_id") int provider_id,
            @Query("status") int status
    );
    @POST(Urls.UpdateProduct)
    Call<ProductDetailsResponse> updateProduct(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("product_id") int product_id,
            @Query("name_ar") String name_ar,
            @Query("name_en") String name_en,
            @Query("disc") String disc,
            @Query("price") String price,
            @Query("category_id") String category_id
    );
    @Multipart
    @POST(Urls.UpdateProduct)
    Call<ProductDetailsResponse> updateProductWithImage(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("product_id") int product_id,
            @Query("name_ar") String name_ar,
            @Query("name_en") String name_en,
            @Query("disc") String disc,
            @Query("price") String price,
            @Query("category_id") String category_id,
            @Part List<MultipartBody.Part>images
    );
    @POST(Urls.GetDept)
    Call<DeptResponse> getDept(
            @Query("user_id") int user_id
    );
    @Multipart
    @POST(Urls.Transfer)
    Call<BaseResponse> transfer(
            @Query("lang") String lang,
            @Query("user_id") int user_id,
            @Query("type") int type,
            @Query("bank_name") String bank_name,
            @Query("acc_name") String acc_name,
            @Query("bank_number") String bank_number,
            @Query("iban") String iban,
            @Query("money") String money,
            @Part MultipartBody.Part image
    );

    @GET("user/{id}")
    Call<List<UserModel>> getUserPlaylists(@Path(value = "id", encoded = true) String userId);


    @PATCH("user/{id}")
    Call<List<UserModel>> upUser(
            @Path(value = "id", encoded = true) String userId,
            @Query("name") String name
    );

}
