package com.aait.mazaqelkhlij.Pereferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.aait.mazaqelkhlij.App.Constant;
import com.aait.mazaqelkhlij.models.UserModel;
import com.google.gson.Gson;

public class SharedPrefManager {

    Context mContext;

    SharedPreferences mSharedPreferences;

    SharedPreferences.Editor mEditor;

    public SharedPrefManager(Context mContext) {
        this.mContext = mContext;
        mSharedPreferences = mContext.getSharedPreferences(Constant.SharedPrefKey.SHARED_PREF_NAME, mContext.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public Boolean getLoginStatus() {
        Boolean value = mSharedPreferences.getBoolean(Constant.SharedPrefKey.LOGIN_STATUS, false);
        return value;
    }

    public void setLoginStatus(Boolean status) {
        mEditor.putBoolean(Constant.SharedPrefKey.LOGIN_STATUS, status);
        mEditor.commit();
    }

    public Boolean getNotificationStatus() {
        Boolean value = mSharedPreferences.getBoolean(Constant.SharedPrefKey.NOTIFICATION, true);
        return value;
    }

    public void setNotificationStatus(Boolean status) {
        mEditor.putBoolean(Constant.SharedPrefKey.NOTIFICATION, status);
        mEditor.commit();
    }


    public void setUserData(UserModel userModel) {
        mEditor.putString(Constant.SharedPrefKey.USER, new Gson().toJson(userModel));
        mEditor.apply();
    }

    public UserModel getUserData() {
        Gson gson = new Gson();
        return gson.fromJson(mSharedPreferences.getString(Constant.SharedPrefKey.USER, null), UserModel.class);
    }


    public void Logout() {
        mEditor.clear();
        mEditor.apply();
    }
}
