package com.aait.mazaqelkhlij.Uitls;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.aait.mazaqelkhlij.R;

public class CustomAnimationUtils {

    private static CustomAnimationUtils mThis = new CustomAnimationUtils();

    public enum AnimationType {
        FadeIn, ZoomIn, Wave, shake, EnterFormRight, Scale , googleDemo , ExitoutUp
    }

    private CustomAnimationUtils() {

    }

    public Animation startAnimation(Context context, AnimationType animationType, View view) {
        Animation animation = null;
        switch (animationType) {
            case FadeIn: {
                animation = AnimationUtils.loadAnimation(context, R.anim.fade_in);
            }
            break;
            case ZoomIn: {
                animation = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
            }
            break;
            case Wave: {
                animation = AnimationUtils.loadAnimation(context, R.anim.wave);
            }
            break;
            case shake: {
                animation = AnimationUtils.loadAnimation(context, R.anim.shake);
            }
            break;
            case EnterFormRight: {
                animation = AnimationUtils.loadAnimation(context, R.anim.slide_form_right);
            }
            break;
            case Scale: {
                animation = AnimationUtils.loadAnimation(context, R.anim.scale);
            }
            break;
            case googleDemo: {
                animation = AnimationUtils.loadAnimation(context, R.anim.google_demo_scale);
            }
            break;
            case ExitoutUp: {
                animation = AnimationUtils.loadAnimation(context, R.anim.exit_out_up);
            }
            break;
        }

        view.startAnimation(animation);

        return animation;
    }

    public static CustomAnimationUtils getThis() {
        return mThis;
    }
}
