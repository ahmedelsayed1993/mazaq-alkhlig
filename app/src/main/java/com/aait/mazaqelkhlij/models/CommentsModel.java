package com.aait.mazaqelkhlij.models;

import java.io.Serializable;

public class CommentsModel implements Serializable {
    private int comment_id;
    private int comment_user_id;
    private String comment_username;
    private String comment_user_img;
    private String comment;
    private String comment_date;

    public int getComment_id() {
        return comment_id;
    }

    public void setComment_id(int comment_id) {
        this.comment_id = comment_id;
    }

    public int getComment_user_id() {
        return comment_user_id;
    }

    public void setComment_user_id(int comment_user_id) {
        this.comment_user_id = comment_user_id;
    }

    public String getComment_username() {
        return comment_username;
    }

    public void setComment_username(String comment_username) {
        this.comment_username = comment_username;
    }

    public String getComment_user_img() {
        return comment_user_img;
    }

    public void setComment_user_img(String comment_user_img) {
        this.comment_user_img = comment_user_img;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment_date() {
        return comment_date;
    }

    public void setComment_date(String comment_date) {
        this.comment_date = comment_date;
    }
}
