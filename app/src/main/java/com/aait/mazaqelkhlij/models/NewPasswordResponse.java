package com.aait.mazaqelkhlij.models;

public class NewPasswordResponse extends BaseResponse {
    private NewPassModel data;

    public NewPassModel getData() {
        return data;
    }

    public void setData(NewPassModel data) {
        this.data = data;
    }
}
