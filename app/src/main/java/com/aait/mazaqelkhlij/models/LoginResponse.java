package com.aait.mazaqelkhlij.models;

public class LoginResponse extends BaseResponse {
    private UserModel data;

    public UserModel getData() {
        return data;
    }

    public void setData(UserModel data) {
        this.data = data;
    }
}
