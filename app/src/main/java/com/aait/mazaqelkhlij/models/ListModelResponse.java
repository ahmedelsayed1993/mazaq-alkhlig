package com.aait.mazaqelkhlij.models;

import java.util.ArrayList;

public class ListModelResponse extends BaseResponse {
    private ArrayList<ListModel> data;

    public ArrayList<ListModel> getData() {
        return data;
    }

    public void setData(ArrayList<ListModel> data) {
        this.data = data;
    }
}
