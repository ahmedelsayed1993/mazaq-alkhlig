package com.aait.mazaqelkhlij.models;

import java.util.ArrayList;

public class DelegatesResponse extends BaseResponse {
    private ArrayList<UserModel> data;

    public ArrayList<UserModel> getData() {
        return data;
    }

    public void setData(ArrayList<UserModel> data) {
        this.data = data;
    }
}
