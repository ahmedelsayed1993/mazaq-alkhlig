package com.aait.mazaqelkhlij.models;

import java.util.ArrayList;

public class ShopsResponse extends BaseResponse {
    private ArrayList<ShopsModel> data;

    public ArrayList<ShopsModel> getData() {
        return data;
    }

    public void setData(ArrayList<ShopsModel> data) {
        this.data = data;
    }
}
