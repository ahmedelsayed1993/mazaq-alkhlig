package com.aait.mazaqelkhlij.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductsResponse implements Serializable {
    private ArrayList<ProductModel> products;

    public ArrayList<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<ProductModel> products) {
        this.products = products;
    }
}
