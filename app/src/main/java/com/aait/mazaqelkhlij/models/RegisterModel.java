package com.aait.mazaqelkhlij.models;

import java.io.Serializable;

public class RegisterModel implements Serializable {
    private int user_id;
    private String user_code;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_code() {
        return user_code;
    }

    public void setUser_code(String user_code) {
        this.user_code = user_code;
    }
}
