package com.aait.mazaqelkhlij.models;

import android.support.v4.widget.SwipeRefreshLayout;

import java.io.Serializable;
import java.util.ArrayList;

public class ShopsModel implements Serializable {
    private int shop_id;
    private int shop_user_id;
    private String shop_name;
    private String shop_user_name;
    private String shop_user_image;
    private String shop_city;
    private int shop_rate;
    private ArrayList<ListModel>shop_categories;

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public int getShop_user_id() {
        return shop_user_id;
    }

    public void setShop_user_id(int shop_user_id) {
        this.shop_user_id = shop_user_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_user_name() {
        return shop_user_name;
    }

    public void setShop_user_name(String shop_user_name) {
        this.shop_user_name = shop_user_name;
    }

    public String getShop_user_image() {
        return shop_user_image;
    }

    public void setShop_user_image(String shop_user_image) {
        this.shop_user_image = shop_user_image;
    }

    public String getShop_city() {
        return shop_city;
    }

    public void setShop_city(String shop_city) {
        this.shop_city = shop_city;
    }

    public int getShop_rate() {
        return shop_rate;
    }

    public void setShop_rate(int shop_rate) {
        this.shop_rate = shop_rate;
    }

    public ArrayList<ListModel> getShop_categories() {
        return shop_categories;
    }

    public void setShop_categories(ArrayList<ListModel> shop_categories) {
        this.shop_categories = shop_categories;
    }
}
