package com.aait.mazaqelkhlij.models;

import java.io.Serializable;

public class NotificationModel implements Serializable {
    private int id;
    private String msg;
    private String taggable_id;
    private String type;
    private boolean seen;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTaggable_id() {
        return taggable_id;
    }

    public void setTaggable_id(String taggable_id) {
        this.taggable_id = taggable_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }
}
