package com.aait.mazaqelkhlij.models;

import java.util.ArrayList;

public class NotificationResponse extends BaseResponse {
    private ArrayList<NotificationModel> data;

    public ArrayList<NotificationModel> getData() {
        return data;
    }

    public void setData(ArrayList<NotificationModel> data) {
        this.data = data;
    }
}
