package com.aait.mazaqelkhlij.models;

public class ChangeOrderStatusResponse extends BaseResponse {
    private OrderModel data;

    public OrderModel getData() {
        return data;
    }

    public void setData(OrderModel data) {
        this.data = data;
    }
}
