package com.aait.mazaqelkhlij.models;

import java.util.ArrayList;

public class ShopRateResponse extends BaseResponse {
    private ArrayList<ShopRateModel> data;

    public ArrayList<ShopRateModel> getData() {
        return data;
    }

    public void setData(ArrayList<ShopRateModel> data) {
        this.data = data;
    }
}
