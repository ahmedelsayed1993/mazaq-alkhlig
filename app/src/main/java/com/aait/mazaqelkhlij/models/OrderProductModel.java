package com.aait.mazaqelkhlij.models;

import java.io.Serializable;

public class OrderProductModel implements Serializable {
    private int product_id;
    private String product_name;
    private int product_count;
    private int product_price;
    private boolean product_offer;
    private int product_original_price;
    private int product_original_offer_price;
    private String product_image;

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getProduct_count() {
        return product_count;
    }

    public void setProduct_count(int product_count) {
        this.product_count = product_count;
    }

    public int getProduct_price() {
        return product_price;
    }

    public void setProduct_price(int product_price) {
        this.product_price = product_price;
    }

    public boolean isProduct_offer() {
        return product_offer;
    }

    public void setProduct_offer(boolean product_offer) {
        this.product_offer = product_offer;
    }

    public int getProduct_original_price() {
        return product_original_price;
    }

    public void setProduct_original_price(int product_original_price) {
        this.product_original_price = product_original_price;
    }

    public int getProduct_original_offer_price() {
        return product_original_offer_price;
    }

    public void setProduct_original_offer_price(int product_original_offer_price) {
        this.product_original_offer_price = product_original_offer_price;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }
}
