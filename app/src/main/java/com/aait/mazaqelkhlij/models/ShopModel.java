package com.aait.mazaqelkhlij.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ShopModel implements Serializable {
    private int shop_id;
    private String shop_name;
    private ArrayList<CategoriesModel> shop_categories;

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public ArrayList<CategoriesModel> getShop_categories() {
        return shop_categories;
    }

    public void setShop_categories(ArrayList<CategoriesModel> shop_categories) {
        this.shop_categories = shop_categories;
    }
}
