package com.aait.mazaqelkhlij.models;

public class RegisterResponse extends BaseResponse {
    private RegisterModel data;

    public RegisterModel getData() {
        return data;
    }

    public void setData(RegisterModel data) {
        this.data = data;
    }
}
