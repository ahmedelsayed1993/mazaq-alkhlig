package com.aait.mazaqelkhlij.models;

import java.io.Serializable;

public class CatModel implements Serializable {
    private int id;
    private String name;


    private boolean isChecked = false;

    private boolean checkCategory = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isCheckCategory() {
        return checkCategory;
    }

    public void setCheckCategory(boolean checkCategory) {
        this.checkCategory = checkCategory;
    }
}
