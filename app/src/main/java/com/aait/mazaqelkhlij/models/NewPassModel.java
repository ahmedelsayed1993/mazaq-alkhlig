package com.aait.mazaqelkhlij.models;

import java.io.Serializable;

public class NewPassModel implements Serializable {
    private int user_id;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
