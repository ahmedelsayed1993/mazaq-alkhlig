package com.aait.mazaqelkhlij.models;

public class ProductDetailsResponse extends BaseResponse {
    private ProductModel data;

    public ProductModel getData() {
        return data;
    }

    public void setData(ProductModel data) {
        this.data = data;
    }
}
