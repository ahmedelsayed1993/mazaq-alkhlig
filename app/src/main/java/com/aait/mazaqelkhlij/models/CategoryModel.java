package com.aait.mazaqelkhlij.models;

import java.util.ArrayList;

public class CategoryModel {
    private ArrayList<ListModel> listModels;

    public CategoryModel(ArrayList<ListModel> listModels) {

        this.listModels = listModels;
    }

    public ArrayList<ListModel> getListModels() {
        return listModels;
    }

    public void setListModels(ArrayList<ListModel> listModels) {
        this.listModels = listModels;
    }
}
