package com.aait.mazaqelkhlij.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ShowShopModel implements Serializable {
    private int shop_id;
    private String shop_name;
    private boolean shop_have_delegate;
    private String shop_delegate_phone;
    private int shop_delegate_price;
    private int shop_rate;
    private int user_id;
    private String user_name;
    private String user_image;
    private String user_city;
    private ArrayList<CategoriesModel> user_categories;
    private ArrayList<ProductModel> products;

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public boolean isShop_have_delegate() {
        return shop_have_delegate;
    }

    public void setShop_have_delegate(boolean shop_have_delegate) {
        this.shop_have_delegate = shop_have_delegate;
    }

    public String getShop_delegate_phone() {
        return shop_delegate_phone;
    }

    public void setShop_delegate_phone(String shop_delegate_phone) {
        this.shop_delegate_phone = shop_delegate_phone;
    }

    public int getShop_delegate_price() {
        return shop_delegate_price;
    }

    public void setShop_delegate_price(int shop_delegate_price) {
        this.shop_delegate_price = shop_delegate_price;
    }

    public int getShop_rate() {
        return shop_rate;
    }

    public void setShop_rate(int shop_rate) {
        this.shop_rate = shop_rate;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_city() {
        return user_city;
    }

    public void setUser_city(String user_city) {
        this.user_city = user_city;
    }

    public ArrayList<CategoriesModel> getUser_categories() {
        return user_categories;
    }

    public void setUser_categories(ArrayList<CategoriesModel> user_categories) {
        this.user_categories = user_categories;
    }

    public ArrayList<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<ProductModel> products) {
        this.products = products;
    }
}
