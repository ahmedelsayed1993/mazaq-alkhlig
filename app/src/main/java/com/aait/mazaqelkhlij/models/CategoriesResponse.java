package com.aait.mazaqelkhlij.models;

import java.util.ArrayList;

public class CategoriesResponse extends BaseResponse {
    private ArrayList<CatModel> data;

    public ArrayList<CatModel> getData() {
        return data;
    }

    public void setData(ArrayList<CatModel> data) {
        this.data = data;
    }
}
