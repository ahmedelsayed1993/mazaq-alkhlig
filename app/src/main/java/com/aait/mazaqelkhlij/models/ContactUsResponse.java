package com.aait.mazaqelkhlij.models;

public class ContactUsResponse extends BaseResponse {
    private ContactUsModel data;

    public ContactUsModel getData() {
        return data;
    }

    public void setData(ContactUsModel data) {
        this.data = data;
    }
}
