package com.aait.mazaqelkhlij.models;

import java.util.ArrayList;

public class BanksResponse extends BaseResponse {
    private ArrayList<BanksModel> data;

    public ArrayList<BanksModel> getData() {
        return data;
    }

    public void setData(ArrayList<BanksModel> data) {
        this.data = data;
    }
}
