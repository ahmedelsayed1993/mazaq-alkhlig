package com.aait.mazaqelkhlij.models;

import java.io.Serializable;

public class ImagesModel implements Serializable {
    private int image_id;
    private String image_path;

    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
