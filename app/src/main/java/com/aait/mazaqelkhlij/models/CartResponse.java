package com.aait.mazaqelkhlij.models;

import java.util.ArrayList;

public class CartResponse extends BaseResponse {
    private ArrayList<CartModel> data;

    public ArrayList<CartModel> getData() {
        return data;
    }

    public void setData(ArrayList<CartModel> data) {
        this.data = data;
    }
}
