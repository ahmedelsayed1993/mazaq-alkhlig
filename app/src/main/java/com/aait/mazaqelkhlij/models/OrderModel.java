package com.aait.mazaqelkhlij.models;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderModel implements Serializable {
    private int order_id;
    private int order_user_id;
    private String order_user_name;
    private String order_user_avatar;
    private String order_user_city;
    private String order_user_area;
    private String order_provider_name;
    private int order_provider_id;
    private String order_provider_avatar;
    private String order_provider_city;
    private String order_provider_area;
    private int order_delegate_id;
    private String order_delegate_name;
    private String order_delegate_avatar;
    private String order_delegate_city;
    private String order_delegate_area;
    private boolean order_foreign_delegate;
    private String order_created_at;
    private int order_way_type;
    private int order_status;
    private String order_refused_reason;
    private ArrayList<OrderProductModel> order_products;
    private double total_price;
    private double delegate_price;
    private double disc_price;
    private double net_price;
    private String order_user_phone;
    private String order_provider_phone;
    private String order_delegate_phone;

    public String getOrder_user_phone() {
        return order_user_phone;
    }

    public void setOrder_user_phone(String order_user_phone) {
        this.order_user_phone = order_user_phone;
    }

    public String getOrder_provider_phone() {
        return order_provider_phone;
    }

    public void setOrder_provider_phone(String order_provider_phone) {
        this.order_provider_phone = order_provider_phone;
    }

    public String getOrder_delegate_phone() {
        return order_delegate_phone;
    }

    public void setOrder_delegate_phone(String order_delegate_phone) {
        this.order_delegate_phone = order_delegate_phone;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getOrder_user_id() {
        return order_user_id;
    }

    public void setOrder_user_id(int order_user_id) {
        this.order_user_id = order_user_id;
    }

    public String getOrder_user_name() {
        return order_user_name;
    }

    public void setOrder_user_name(String order_user_name) {
        this.order_user_name = order_user_name;
    }

    public String getOrder_user_avatar() {
        return order_user_avatar;
    }

    public void setOrder_user_avatar(String order_user_avatar) {
        this.order_user_avatar = order_user_avatar;
    }

    public String getOrder_user_city() {
        return order_user_city;
    }

    public void setOrder_user_city(String order_user_city) {
        this.order_user_city = order_user_city;
    }

    public String getOrder_user_area() {
        return order_user_area;
    }

    public void setOrder_user_area(String order_user_area) {
        this.order_user_area = order_user_area;
    }

    public String getOrder_provider_name() {
        return order_provider_name;
    }

    public void setOrder_provider_name(String order_provider_name) {
        this.order_provider_name = order_provider_name;
    }

    public int getOrder_provider_id() {
        return order_provider_id;
    }

    public void setOrder_provider_id(int order_provider_id) {
        this.order_provider_id = order_provider_id;
    }

    public String getOrder_provider_avatar() {
        return order_provider_avatar;
    }

    public void setOrder_provider_avatar(String order_provider_avatar) {
        this.order_provider_avatar = order_provider_avatar;
    }

    public String getOrder_provider_city() {
        return order_provider_city;
    }

    public void setOrder_provider_city(String order_provider_city) {
        this.order_provider_city = order_provider_city;
    }

    public String getOrder_provider_area() {
        return order_provider_area;
    }

    public void setOrder_provider_area(String order_provider_area) {
        this.order_provider_area = order_provider_area;
    }

    public int getOrder_delegate_id() {
        return order_delegate_id;
    }

    public void setOrder_delegate_id(int order_delegate_id) {
        this.order_delegate_id = order_delegate_id;
    }

    public String getOrder_delegate_name() {
        return order_delegate_name;
    }

    public void setOrder_delegate_name(String order_delegate_name) {
        this.order_delegate_name = order_delegate_name;
    }

    public String getOrder_delegate_avatar() {
        return order_delegate_avatar;
    }

    public void setOrder_delegate_avatar(String order_delegate_avatar) {
        this.order_delegate_avatar = order_delegate_avatar;
    }

    public String getOrder_delegate_city() {
        return order_delegate_city;
    }

    public void setOrder_delegate_city(String order_delegate_city) {
        this.order_delegate_city = order_delegate_city;
    }

    public String getOrder_delegate_area() {
        return order_delegate_area;
    }

    public void setOrder_delegate_area(String order_delegate_area) {
        this.order_delegate_area = order_delegate_area;
    }

    public boolean isOrder_foreign_delegate() {
        return order_foreign_delegate;
    }

    public void setOrder_foreign_delegate(boolean order_foreign_delegate) {
        this.order_foreign_delegate = order_foreign_delegate;
    }

    public String getOrder_created_at() {
        return order_created_at;
    }

    public void setOrder_created_at(String order_created_at) {
        this.order_created_at = order_created_at;
    }

    public int getOrder_way_type() {
        return order_way_type;
    }

    public void setOrder_way_type(int order_way_type) {
        this.order_way_type = order_way_type;
    }

    public int getOrder_status() {
        return order_status;
    }

    public void setOrder_status(int order_status) {
        this.order_status = order_status;
    }

    public String getOrder_refused_reason() {
        return order_refused_reason;
    }

    public void setOrder_refused_reason(String order_refused_reason) {
        this.order_refused_reason = order_refused_reason;
    }

    public ArrayList<OrderProductModel> getOrder_products() {
        return order_products;
    }

    public void setOrder_products(ArrayList<OrderProductModel> order_products) {
        this.order_products = order_products;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    public double getDelegate_price() {
        return delegate_price;
    }

    public void setDelegate_price(double delegate_price) {
        this.delegate_price = delegate_price;
    }

    public double getDisc_price() {
        return disc_price;
    }

    public void setDisc_price(double disc_price) {
        this.disc_price = disc_price;
    }

    public double getNet_price() {
        return net_price;
    }

    public void setNet_price(double net_price) {
        this.net_price = net_price;
    }
}
