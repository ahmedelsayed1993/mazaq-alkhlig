package com.aait.mazaqelkhlij.models;

import java.io.Serializable;

public class BaseResponse implements Serializable {
    private int status;
    private String msg;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean issucessfull(){
        if (status == 1){
            return true;
        }else {
            return false;
        }
    }
}
