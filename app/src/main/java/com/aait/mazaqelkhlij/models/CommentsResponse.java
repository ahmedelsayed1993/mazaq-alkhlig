package com.aait.mazaqelkhlij.models;

import java.io.Serializable;
import java.util.ArrayList;

public class CommentsResponse implements Serializable {
    private String user_rate_before;
    private String user_rate;
    private ArrayList<CommentsModel> comments;

    public String getUser_rate_before() {
        return user_rate_before;
    }

    public void setUser_rate_before(String user_rate_before) {
        this.user_rate_before = user_rate_before;
    }

    public String getUser_rate() {
        return user_rate;
    }

    public void setUser_rate(String user_rate) {
        this.user_rate = user_rate;
    }

    public ArrayList<CommentsModel> getComments() {
        return comments;
    }

    public void setComments(ArrayList<CommentsModel> comments) {
        this.comments = comments;
    }
}
