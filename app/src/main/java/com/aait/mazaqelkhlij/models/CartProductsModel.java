package com.aait.mazaqelkhlij.models;

import java.io.Serializable;

public class CartProductsModel implements Serializable {

    private int cart_product_id;
    private int product_id;
    private String product_name;
    private String product_image;
    private int product_count;
    private int total_product_price;

    public int getCart_product_id() {
        return cart_product_id;
    }

    public void setCart_product_id(int cart_product_id) {
        this.cart_product_id = cart_product_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public int getProduct_count() {
        return product_count;
    }

    public void setProduct_count(int product_count) {
        this.product_count = product_count;
    }

    public int getTotal_product_price() {
        return total_product_price;
    }

    public void setTotal_product_price(int total_product_price) {
        this.total_product_price = total_product_price;
    }
}
