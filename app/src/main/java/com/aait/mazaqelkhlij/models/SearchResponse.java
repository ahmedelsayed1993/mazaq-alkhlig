package com.aait.mazaqelkhlij.models;

import java.util.ArrayList;

public class SearchResponse extends BaseResponse {
    private ArrayList<ProductModel> data;

    public ArrayList<ProductModel> getData() {
        return data;
    }

    public void setData(ArrayList<ProductModel> data) {
        this.data = data;
    }
}
