package com.aait.mazaqelkhlij.models;

public class ForgotPasswordResponse extends BaseResponse {
    private ForgetPasswordModel data;

    public ForgetPasswordModel getData() {
        return data;
    }

    public void setData(ForgetPasswordModel data) {
        this.data = data;
    }
}
