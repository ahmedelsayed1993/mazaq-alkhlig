package com.aait.mazaqelkhlij.models;

public class ProductsResponses extends BaseResponse {
    private ProductsResponse data;

    public ProductsResponse getData() {
        return data;
    }

    public void setData(ProductsResponse data) {
        this.data = data;
    }
}
