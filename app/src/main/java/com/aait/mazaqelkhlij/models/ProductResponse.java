package com.aait.mazaqelkhlij.models;

public class ProductResponse extends BaseResponse {
    private ProviderModel data;

    public ProviderModel getData() {
        return data;
    }

    public void setData(ProviderModel data) {
        this.data = data;
    }
}
