package com.aait.mazaqelkhlij.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Pereferences.LanguagePrefManager;
import com.aait.mazaqelkhlij.Pereferences.SharedPrefManager;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.UI.views.Toaster;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.DialogUtil;
import com.pnikosis.materialishprogress.ProgressWheel;

import butterknife.ButterKnife;

public abstract class ParentActivity extends AppCompatActivity {

    protected AppCompatActivity mActivity;

    protected SharedPrefManager mSharedPrefManager;

    protected LanguagePrefManager mLanguagePrefManager;

    Toolbar toolbar;

    protected Context mContext;

    private int menuId;

    protected Toaster mToaster;

    protected Bundle mSavedInstanceState;

    private ProgressDialog mProgressDialog;

    protected SwipeRefreshLayout swipeRefresh;

    protected RelativeLayout layProgress;

    protected ProgressWheel progressWheel;

    protected RelativeLayout layNoInternet;

    protected ImageView ivNoInternet;

    protected RelativeLayout layNoItem;

    protected ImageView ivNoItem;

    protected TextView tvNoContent;

    protected RecyclerView rvRecycle;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mActivity = this;
        mSharedPrefManager = new SharedPrefManager(mContext);
        mLanguagePrefManager = new LanguagePrefManager(mContext);
        CommonUtil.setConfig(mLanguagePrefManager.getAppLanguage(), this);
        mToaster = new Toaster(mContext);

        if (isFullScreen()) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        if (hideInputType()) {
            hideInputTyping();
        }
        // set layout resources
        setContentView(getLayoutResource());
        this.mSavedInstanceState = savedInstanceState;

        ButterKnife.bind(this);
        if (isEnableToolbar()) {
            configureToolbar();
        }
        if (isRecycle()) {
            ConfigRecycle();
        }
        initializeComponents();

    }


    public void setToolbarTitle(String titleId) {
        if (toolbar != null) {
            toolbar.setTitle(titleId);
        }
    }

    protected abstract void initializeComponents();

    /**
     * this method is responsible for configure toolbar
     * it is called when I enable toolbar in my activity
     */
    private void configureToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitleTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        // check if enable back
        if (isEnableBack()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//             toolbar.setNavigationIcon(R.mipmap.back);
        }
    }

    private void ConfigRecycle() {
        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        layProgress = (RelativeLayout) findViewById(R.id.lay_progress);
        progressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        layNoInternet = (RelativeLayout) findViewById(R.id.lay_no_internet);
        ivNoInternet = (ImageView) findViewById(R.id.iv_no_internet);
        layNoItem = (RelativeLayout) findViewById(R.id.lay_no_item);
        ivNoItem = (ImageView) findViewById(R.id.iv_no_item);
        tvNoContent = (TextView) findViewById(R.id.tv_no_content);
        rvRecycle = (RecyclerView) findViewById(R.id.rv_recycle);
    }

    /**
     * @return the layout resource id
     */
    protected abstract int getLayoutResource();

    /**
     * it is a boolean method which tell my if toolbar
     * is enabled or not
     */
    protected abstract boolean isEnableToolbar();

    /**
     * it is a boolean method which tell if full screen mode
     * is enabled or not
     */
    protected abstract boolean isFullScreen();

    /**
     * it is a boolean method which tell if back button
     * is enabled or not
     */
    protected abstract boolean isEnableBack();

    /**
     * it is a boolean method which tell if input is
     * is appeared  or not
     */
    protected abstract boolean hideInputType();

    /**
     * it the current activity is a recycle
     */
    protected abstract boolean isRecycle();

    /**
     * this method allowed me to create option menu
     */
    public void createOptionsMenu(int menuId) {
        Log.e("test", "test");
        this.menuId = menuId;
        invalidateOptionsMenu();
    }

    /**
     * this method allowed me to remove option menu
     */
    public void removeOptionsMenu() {
        menuId = 0;
        invalidateOptionsMenu();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (menuId != 0) {
            getMenuInflater().inflate(menuId, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_left);
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_left);
    }

    public void hideInputTyping() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    protected void showProgressDialog(String message) {
        mProgressDialog = DialogUtil.showProgressDialog(this, message, false);
    }

    protected void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

}
