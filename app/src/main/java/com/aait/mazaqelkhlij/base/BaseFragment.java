package com.aait.mazaqelkhlij.base;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.mazaqelkhlij.Pereferences.LanguagePrefManager;
import com.aait.mazaqelkhlij.Pereferences.SharedPrefManager;
import com.aait.mazaqelkhlij.R;
import com.aait.mazaqelkhlij.Uitls.CommonUtil;
import com.aait.mazaqelkhlij.Uitls.DialogUtil;
import com.pnikosis.materialishprogress.ProgressWheel;

import butterknife.ButterKnife;

public abstract class BaseFragment extends android.support.v4.app.Fragment {

    public SharedPrefManager mSharedPrefManager;

    protected LanguagePrefManager mLanguagePrefManager;

    protected Context mContext;

    public Bundle mSavedInstanceState;

    private ProgressDialog mProgressDialog;

    protected SwipeRefreshLayout swipeRefresh;

    protected RelativeLayout layProgress;

    protected ProgressWheel progressWheel;

    protected RelativeLayout layNoInternet;

    protected ImageView ivNoInternet;

    protected RelativeLayout layNoItem;

    protected ImageView ivNoItem;

    protected TextView tvNoContent;

    protected RecyclerView rvRecycle;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResource(), container, false);
        mContext = getActivity();
        mSharedPrefManager = new SharedPrefManager(mContext);
        mLanguagePrefManager = new LanguagePrefManager(mContext);
        this.mSavedInstanceState = savedInstanceState;
        ButterKnife.bind(this, view);
        if (isRecycle()) {
            ConfigRecycle(view);
        }
        initializeComponents(view);
        return view;
    }


    protected abstract int getLayoutResource();

    protected abstract void initializeComponents(View view);

    /**
     * it the current activity is a recycle
     */
    protected abstract boolean isRecycle();

    protected void showProgressDialog(String message) {
        mProgressDialog = DialogUtil.showProgressDialog(getActivity(), message, false);
    }

    protected void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    private void ConfigRecycle(View view) {
        CommonUtil.PrintLogE("Initialize Views");
        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        layProgress = (RelativeLayout) view.findViewById(R.id.lay_progress);
        progressWheel = (ProgressWheel) view.findViewById(R.id.progress_wheel);
        layNoInternet = (RelativeLayout) view.findViewById(R.id.lay_no_internet);
        ivNoInternet = (ImageView) view.findViewById(R.id.iv_no_internet);
        layNoItem = (RelativeLayout) view.findViewById(R.id.lay_no_item);
        ivNoItem = (ImageView) view.findViewById(R.id.iv_no_item);
        tvNoContent = (TextView) view.findViewById(R.id.tv_no_content);
        rvRecycle = (RecyclerView) view.findViewById(R.id.rv_recycle);
    }

}
